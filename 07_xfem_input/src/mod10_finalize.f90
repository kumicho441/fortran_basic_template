module finalize
    use dbg
    use variables
    use initialize
    use makeMesh
    use makeK
    use insert_BC
    use CGsolver
    use addXtoG
    use evaluate
    use paraview

    implicit none
    contains

    subroutine deallocating()
        implicit none
        if (allocated(meshdata%xid)) deallocate(meshdata%xid)
        if (allocated(meshdata%gid)) deallocate(meshdata%gid)
        if (allocated(meshdata%Xnid)) deallocate(meshdata%Xnid)
        if (allocated(meshdata%inside)) deallocate(meshdata%inside)
        if (allocated(meshdata%subdivision)) deallocate(meshdata%subdivision)
        if (allocated(meshdata%enrich_node)) deallocate(meshdata%enrich_node)

        ! 追加されたメンバ
        if (allocated(matrix%co_ip_subd)) deallocate(matrix%co_ip_subd)
        if (allocated(matrix%KG)) deallocate(matrix%KG)
        if (allocated(matrix%KGX)) deallocate(matrix%KGX)
        if (allocated(matrix%KX)) deallocate(matrix%KX)

        if (allocated(Garea%elem_node_id)) deallocate(Garea%elem_node_id)
        if (allocated(Garea%co_node)) deallocate(Garea%co_node)
        if (allocated(Garea%Kmat)) deallocate(Garea%Kmat)

        if (allocated(Xarea%elem_node_id)) deallocate(Xarea%elem_node_id)
        if (allocated(Xarea%co_node)) deallocate(Xarea%co_node)
        if (allocated(Xarea%Kmat)) deallocate(Xarea%Kmat)

        if (allocated(Earea%elem_node_id)) deallocate(Earea%elem_node_id)
        if (allocated(Earea%co_node)) deallocate(Earea%co_node)
        if (allocated(Earea%Kmat)) deallocate(Earea%Kmat)

        if (allocated(solver%Kmat)) deallocate(solver%Kmat)
        if (allocated(solver%Fvec)) deallocate(solver%Fvec)
        if (allocated(solver%uvec)) deallocate(solver%uvec)
        if (allocated(solver%Pmat)) deallocate(solver%Pmat)
        if (allocated(solver%PTmat)) deallocate(solver%PTmat)
        if (allocated(solver%Pinv)) deallocate(solver%Pinv)
        if (allocated(solver%rvec)) deallocate(solver%rvec)
        if (allocated(solver%r0)) deallocate(solver%r0)
        if (allocated(solver%rvec_save)) deallocate(solver%rvec_save)
        if (allocated(solver%pvec)) deallocate(solver%pvec)
        if (allocated(solver%alp)) deallocate(solver%alp)
        if (allocated(solver%bet)) deallocate(solver%bet)
        if (allocated(solver%pro)) deallocate(solver%pro)
        if (allocated(solver%uth)) deallocate(solver%uth)
        if (allocated(eval%elel2)) deallocate(eval%elel2)
        if (allocated(eval%ucal)) deallocate(eval%ucal)
        if (allocated(eval%uvec_th)) deallocate(eval%uvec_th)
    end subroutine deallocating

    subroutine main_program()
        implicit none

        call mod03_makeMesh()
        call mod04_makeK()
        call mod05_BC()
        call mod06_CGsolver()
        call mod07_uG_plus_uX()
        call mod08_L2norm()
        call mod09_vis_outcome()
    end subroutine main_program

    subroutine mod10_repeat_program()
        implicit none
        call mod00_debug()

        dbg_disign_mesh = .true.
        do global_iter = 4, 32
            diviL_disign = global_iter*2 !(10)
            
            call deallocating()
            call main_program()

            write(*,*)Earea%Nelem,',',Earea%Nnode,',',solver%iter,',',eval%L2,',',global_iter*2
        enddo

    end subroutine mod10_repeat_program



end module finalize

    