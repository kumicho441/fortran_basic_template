module makeMesh
    use dbg
    use initialize
    use variables
    implicit none

    contains

    subroutine Gmesher()!Gmesh定義
        implicit none
        integer(4) :: i,j
        dbg_name = 'Gmesher'
        call dbg_start_subroutineM()
     
        meshdata%leng_mesh = (meshdata%lengL/meshdata%diviL)

        !G領域座標,id設定
        do i = 1, meshdata%diviL + 1
            do j = 1, meshdata%diviL + 1
                Garea%co_node(1,(meshdata%diviL+1)*(i-1)+ j ) = (j-1)*meshdata%leng_mesh
                Garea%co_node(2,(meshdata%diviL+1)*(i-1)+ j ) = (i-1)*meshdata%leng_mesh
            enddo
        enddo

        do j = 1, meshdata%diviL
            do i = 1, meshdata%diviL
                Garea%elem_node_id(1,i+ meshdata%diviL * (j-1)) = i+(meshdata%diviL+1)*(j-1)
                Garea%elem_node_id(2,i+ meshdata%diviL * (j-1)) = Garea%elem_node_id(1,i+meshdata%diviL * (j-1)) + 1
                Garea%elem_node_id(3,i+ meshdata%diviL * (j-1)) = i+(meshdata%diviL+1)*(j)  + 1
                Garea%elem_node_id(4,i+ meshdata%diviL * (j-1)) = Garea%elem_node_id(3,i+ meshdata%diviL * (j-1)) - 1
            enddo
        enddo
    end subroutine Gmesher

    subroutine Xmesher()!Xmesh定義
        implicit none
        integer(4) :: c1,i,j
        real(8) :: xco1,yco1
        dbg_name = 'Xmesher'
        call dbg_start_subroutineM()
        
        !エンリッチメッシュ選定
        j = 0
        do i = 1, Garea%Nelem
            xco1 = Garea%co_node(1,Garea%elem_node_id(1,i))
            yco1 = Garea%co_node(2,Garea%elem_node_id(1,i))
            if(xco1 < meshdata%lengX .and. yco1 < meshdata%lengX)then!エンリッチ付与判定
                j = j+1
                meshdata%inside(i) = .true.
            endif
        enddo

        Xarea%Nelem = j
        c1 = nint(sqrt(dble(j)))!Xmesh一辺分割数
        Xarea%Nelem = c1**2
        Xarea%Nnode = (c1+1)**2

        !Xarea%の初期化を実行
        allocate(Xarea%elem_node_id(4, Xarea%Nelem), & 
        Xarea%co_node(2, Xarea%Nnode), & 
        Xarea%Kmat(Xarea%Nnode*2, Xarea%Nnode*2))
        Xarea%elem_node_id = 0
        Xarea%co_node = 0.0d0
        Xarea%Kmat = 0.0d0   

        
        ! !デバッグ
        ! write(*,*)c1,Xarea%Nelem,Xarea%Nnode
        
        !X領域座標,id設定
        do i = 1, c1 + 1
            do j = 1, c1 + 1
                Xarea%co_node(1,(c1+1)*(i-1)+ j ) = (j-1)*meshdata%leng_mesh
                Xarea%co_node(2,(c1+1)*(i-1)+ j ) = (i-1)*meshdata%leng_mesh
            enddo
        enddo

        do j = 1, c1
            do i = 1, c1
                Xarea%elem_node_id(1,i+ c1 * (j-1)) = i+(c1+1)*(j-1)
                Xarea%elem_node_id(2,i+ c1 * (j-1)) = Xarea%elem_node_id(1,i+c1 * (j-1)) + 1
                Xarea%elem_node_id(3,i+ c1 * (j-1)) = i+(c1+1)*(j)  + 1
                Xarea%elem_node_id(4,i+ c1 * (j-1)) = Xarea%elem_node_id(3,i+ c1 * (j-1)) - 1
            enddo
        enddo
    end subroutine Xmesher

    subroutine Emesher()!L2ノルム誤差評価用のメッシュ
        implicit none
        integer(4) ::i,j,n,c1,c2,c3
        real(8) :: th,pi,dr
        real(8),allocatable :: r(:)
        dbg_name = 'Emesher'
        call dbg_start_subroutineM()

        call initialize_Earea()


        pi = atan(1.0d0)*4
        n = meshdata%diviL
        allocate(r(n+1))

        do j = 1, n+1
            th = pi*(j-1)/2/n

            !デバッグ
            ! write(*,*)th/pi * 180

            if(th <= pi/4 .or. abs(th - pi/4)<= 1.0E-08)then!分割数により稀にバグるための措置
                dr = (meshdata%lengL * sqrt(1 + tan(th)**2) - meshdata%lengR) / n
                r(1) = meshdata%lengR

                do i = 2,n+1
                    r(i) = r(i-1) + dr 
                enddo

                do i = 1, n + 1
                    Earea%co_node(1,i + (j-1)*(n + 1)) = r(i) * cos(th)
                    Earea%co_node(2,i + (j-1)*(n + 1)) = r(i) * sin(th)
                enddo

            else

                do i = 1,n + 1
                    Earea%co_node(1,i + (j-1)*(n+1)) = Earea%co_node(2,i + n*(n+1) - (j-1)*(n+1)) !>対称性を利用して座標を定義
                    Earea%co_node(2,i + (j-1)*(n+1)) = Earea%co_node(1,i + n*(n+1) - (j-1)*(n+1)) !>対称性を利用して座標を定義
                enddo

            endif
        enddo

        do j = 1,n
            do i = 1,n
                Earea%elem_node_id(1,i+n*(j-1)) = i + (n+1) * (j-1)
                Earea%elem_node_id(2,i+n*(j-1)) = Earea%elem_node_id(1,i+n*(j-1)) + 1
                Earea%elem_node_id(3,i+n*(j-1)) = i + (n+1) * j + 1
                Earea%elem_node_id(4,i+n*(j-1)) = Earea%elem_node_id(3,i+n*(j-1)) - 1
            enddo
        enddo

    end subroutine Emesher

    ! subroutine 

    subroutine select_subdivision()!細分化メッシュ決定
        implicit none
        integer(4) :: i
        real(8) :: xco1,yco1,xco2,yco2,r1,r2
        dbg_name = 'select_subdivision'
        call dbg_start_subroutineM()

        !細分化するメッシュ選定
        do i = 1, Garea%Nelem
            xco1 = Garea%co_node(1,Garea%elem_node_id(1,i))
            yco1 = Garea%co_node(2,Garea%elem_node_id(1,i))
            r1 = sqrt(xco1**2+yco1**2)
            xco2 = Garea%co_node(1,Garea%elem_node_id(3,i))
            yco2 = Garea%co_node(2,Garea%elem_node_id(3,i))      
            r2 = sqrt(xco2**2+yco2**2)
            
            if(r1 <= meshdata%lengR .and. meshdata%lengR <= r2)then
                meshdata%subdivision(i) = .true.
            endif
        enddo

        !(デバッグ:細分化なし)
    end subroutine select_subdivision

    subroutine connectGX()!GmeshとXmeshを対応付ける
        implicit none
        integer(4) :: i,j,k,elemid,nodeid
        real(8) :: xco1,yco1
        dbg_name = 'connectGX'
        call dbg_start_subroutineM()
        
        !xid(i)=X領域i番目要素のG要素番号,gid(i)=G領域i番目要素のX要素番号
        allocate(meshdata%xid(Xarea%Nelem),meshdata%gid(Garea%Nelem),meshdata%enrich_node(Xarea%Nnode))
        meshdata%xid = 0
        meshdata%gid = -1
        meshdata%enrich_node = .false.

        j = 0

        do i = 1,Garea%Nelem
            if(meshdata%inside(i))then
                j = j+1
                meshdata%xid(j) = i
                meshdata%gid(i) = j
            endif
        enddo

        !物理量を追加する節点の選定
        do elemid = 1,Xarea%Nelem
            if(meshdata%subdivision(meshdata%xid(elemid)))then
                do i = 1,4
                    meshdata%enrich_node(Xarea%elem_node_id(i,elemid)) = .true.
                enddo 
            endif
        enddo

        !物理量を追加する節点数の記録
        do nodeid = 1,Xarea%Nnode
            if(meshdata%enrich_node(nodeid))then
                meshdata%Nenrich = meshdata%Nenrich + 1
            endif
        enddo

        meshdata%Nconstrain = Xarea%Nnode - meshdata%Nenrich

        !各領域の節点番号を対応付ける
        do i = 1, Xarea%Nelem
            do j = 1,4
                meshdata%Xnid(Xarea%elem_node_id(j,i)) = Garea%elem_node_id(j,meshdata%xid(i))
            enddo
        enddo

    end subroutine connectGX

    subroutine mesher_visualize()!メッシャー部分の可視化ファイル作成
        implicit none
        integer(4) :: i
        character(256) :: filename = '07_xfem.vtk'
        dbg_name = 'mesehr_visualize'
        call dbg_start_subroutineM()

        open(unit = numVTK, file = filename, status = 'replace')
            write(numVTK, '(a)') '# vtk DataFile Version 4.1'
            write(numVTK, '(a)') 'This is a comment line'
            write(numVTK, '(a)') 'ASCII'
            write(numVTK, '(a)') 'DATASET UNSTRUCTURED_GRID'
            write(numVTK, '(a,i0,a)') 'POINTS ',Garea%Nnode,' float'

            do i = 1,Garea%Nnode!>節点の座標を行ごとに代入する
                write(numVTK, '(G0,a,G0,a,G0)') dble(Garea%co_node(1,i)),&
                ' ',  dble(Garea%co_node(2,i)),' 0.0'
            enddo

             write(numVTK, '(a,i0,a,i0)') 'CELLS ', Garea%Nelem ,' ', 5*(Garea%Nelem)

            do i =1,Garea%Nelem!節点id入力
                write(numVTK,'(a,I0,a,I0,a,I0,a,I0)') '4 ', Garea%elem_node_id(1,i)-1,' ',&
                Garea%elem_node_id(2,i)-1,' ',&
                Garea%elem_node_id(3,i)-1,' ',&
                Garea%elem_node_id(4,i)-1
            enddo

            write(numVTK, '(a,I0)') 'CELL_TYPES ' , Garea%Nelem
            do i = 1,Garea%Nelem
                write(numVTK,'(I0)') 9
            enddo
       close(numVTK)

       !Xmeshのvtkファイルを作成
       if(dbg_vis_Xmesh)then

            filename = 'Xmesh.vtk'
            open(unit = numX, file = filename, status = 'replace')
            write(numX, '(a)') '# vtk DataFile Version 4.1'
            write(numX, '(a)') 'This is a comment line'
            write(numX, '(a)') 'ASCII'
            write(numX, '(a)') 'DATASET UNSTRUCTURED_GRID'
            write(numX, '(a,i0,a)') 'POINTS ', Xarea%Nnode, ' float'
    
            do i = 1, Xarea%Nnode  !>節点の座標を行ごとに代入する
                write(numX, '(G0,a,G0,a,G0)') dble(Xarea%co_node(1,i)), &
                ' ', dble(Xarea%co_node(2,i)), ' 0.0'
            enddo
            
            write(numX, '(a,i0,a,i0)') 'CELLS ', Xarea%Nelem, ' ', 5*(Xarea%Nelem)
            
            do i = 1, Xarea%Nelem  ! 節点id入力
                write(numX, '(a,I0,a,I0,a,I0,a,I0)') '4 ', Xarea%elem_node_id(1,i)-1, ' ', &
                Xarea%elem_node_id(2,i)-1, ' ', &
                Xarea%elem_node_id(3,i)-1, ' ', &
                Xarea%elem_node_id(4,i)-1
            enddo
            
            write(numX, '(a,I0)') 'CELL_TYPES ', Xarea%Nelem
            do i = 1, Xarea%Nelem
                write(numX, '(I0)') 9
            enddo
            close(numX)
        endif

        !L2ノルム誤差評価用のメッシュを作成
        open(unit = numL2, file = '07_xfem_eval.vtk', status = 'replace')
            write(numL2, '(a)') '# vtk DataFile Version 4.1'
            write(numL2, '(a)') 'This is a comment line'
            write(numL2, '(a)') 'ASCII'
            write(numL2, '(a)') 'DATASET UNSTRUCTURED_GRID'
            write(numL2, '(a,i0,a)') 'POINTS ',Earea%Nnode,' float'

            do i = 1,Earea%Nnode!>節点の座標を行ごとに代入する
                write(numL2, '(G0,a,G0,a,G0)') dble(Earea%co_node(1,i)),&
                ' ',  dble(Earea%co_node(2,i)),' 0.0'
            enddo

             write(numL2, '(a,i0,a,i0)') 'CELLS ', Earea%Nelem ,' ', 5*(Earea%Nelem)

            do i =1,Earea%Nelem!節点id入力
                write(numL2,'(a,I0,a,I0,a,I0,a,I0)') '4 ', Earea%elem_node_id(1,i)-1,' ',&
                Earea%elem_node_id(2,i)-1,' ',&
                Earea%elem_node_id(3,i)-1,' ',&
                Earea%elem_node_id(4,i)-1
            enddo

            write(numL2, '(a,I0)') 'CELL_TYPES ' , Earea%Nelem
            do i = 1,Earea%Nelem
                write(numL2,'(I0)') 9
            enddo
       close(numL2)



    end subroutine mesher_visualize

    subroutine mod03_makeMesh()
        dbg_name = 'mod03_makeMesh'
        call dbg_start_subroutineL()
        
        call initialize_meshdata()
        call initialize_Garea()
        call Gmesher()
        call Xmesher()
        call select_subdivision()
        call connectGX()
        call Emesher()
        call mesher_visualize()
    end subroutine mod03_makeMesh





end module makeMesh