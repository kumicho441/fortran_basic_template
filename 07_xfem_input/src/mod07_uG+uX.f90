module addXtoG
    use dbg
    use variables
    use initialize
    implicit none

    contains

    subroutine uG_plus_uX()
        implicit none

        integer(4) :: i
        if(.not.dbg_no_addition)then

            do i = 1, Xarea%Nnode
                solver%uvec(meshdata%Xnid(i)*2 - 1) = solver%uvec(meshdata%Xnid(i)*2 - 1) + solver%uvec(solver%SoM_G + i*2-1)
                solver%uvec(meshdata%Xnid(i)*2    ) = solver%uvec(meshdata%Xnid(i)*2    ) + solver%uvec(solver%SoM_G + i*2  )
            enddo
        endif
    end subroutine uG_plus_uX

    subroutine mod07_uG_plus_uX()
        implicit none

        dbg_name = 'mod07_uG_plus_uX'
        call dbg_start_subroutineL()

        call uG_plus_uX()

    end subroutine mod07_uG_plus_uX


end module addXtoG