module initialize
    use dbg
    use variables
    implicit none
    contains

    subroutine initialize_meshdata()
        implicit none
        ! dbg_name = 'initialize_meshdata'
        ! call dbg_start_subroutine()

    ! meshdata_strucの変数を初期化
    meshdata%lengL = 10.0d0
    meshdata%diviL = 10
    meshdata%Nenrich = 0
    ratioX = ratioR + meshdata%lengL*1.01/meshdata%diviL!

    !デバッグ:メッシュ指定
    if(dbg_disign_mesh)then
        meshdata%lengL = lengL_disign
        meshdata%diviL = diviL_disign
        ratioR = ratioR_disign
        ratioX = ratioX_disign
    endif


    meshdata%lengX = meshdata%lengL*ratioX
    meshdata%lengR = meshdata%lengL*ratioR


    end subroutine initialize_meshdata


    subroutine initialize_Garea()
        ! dbg_name = 'initialize_Garea'
        ! call dbg_start_subroutine()

        Garea%Nelem = meshdata%diviL**2
        Garea%Nnode = (meshdata%diviL + 1)**2
        
        allocate(Garea%elem_node_id(4, Garea%Nelem), &
                 Garea%co_node(2, Garea%Nnode), &
                 Garea%Kmat(Garea%Nnode*2, Garea%Nnode*2), &
                 meshdata%inside(Garea%Nelem),meshdata%subdivision(Garea%Nelem),meshdata%Xnid(Garea%Nnode))
        Garea%elem_node_id = 0
        Garea%co_node = 0.0d0
        Garea%Kmat = 0.0d0
        meshdata%inside = .false.
        meshdata%subdivision = .false.
        meshdata%Xnid = 0
    end subroutine initialize_Garea

    subroutine initialize_Earea()
        Earea%Nelem = meshdata%diviL**2
        Earea%Nnode = (meshdata%diviL + 1)**2

        allocate(Earea%elem_node_id(4, Earea%Nelem), &
                 Earea%co_node(2, Earea%Nnode), &
                 Earea%Kmat(1,1), &!EareaではKmatを使わないため1でallocate
                 )
        Earea%elem_node_id = 0
        Earea%co_node = 0.0d0
        Earea%Kmat = 0.0d0

    end subroutine initialize_Earea

    subroutine initialize_matrix()!matrix%の初期化
        implicit none
        integer(4) :: i,j,k,in
        real(8) :: len1,len2,len3,r(2)

        ! dbg_name = 'initialize_matrix'
        ! call dbg_start_subroutine()

        matrix%Dmat = 0.0d0
        matrix%Jmat = 0.0d0
        matrix%Jinv = 0.0d0
        matrix%detJ = 0.0d0
        matrix%Bmat = 0.0d0
        matrix%BTmat = 0.0d0
        matrix%Kemat = 0.0d0
        matrix%KeGX = 0.0d0
        matrix%mat38 = 0.0d0
        matrix%mat24 = 0.0d0
        matrix%mat42_m = 0.0d0
        matrix%mat42_u = 0.0d0
        matrix%mat88 = 0.0d0
        matrix%BonGT = 0.0d0
        matrix%mat24_m = 0.0d0
        matrix%mat24_u = 0.0d0

        allocate(matrix%KGX(Garea%Nnode*2,Xarea%Nnode*2),matrix%KG(Garea%Nnode*2,Garea%Nnode*2),&
        matrix%KX(Xarea%Nnode*2,Xarea%Nnode*2))
        matrix%KGX = 0.0d0
        matrix%KG = 0.0d0
        matrix%KX = 0.0d0

    
        matrix%Nip = 4
        matrix%n_rec = 4

        !デバッグ:n_rec指定
        if(dbg_disign_nrec)then
            matrix%n_rec = n_rec_disign
        endif

        !デバッグ:細分化なし
        if(dbg_no_subdivision)then
            matrix%n_rec = 1
        endif

        matrix%Nip_subd = (matrix%n_rec**2) * 4
        matrix%co_ip = 0.0d0
        matrix%co_ip(1,1) = -0.57735027
        matrix%co_ip(2,1) = -0.57735027
        matrix%co_ip(1,2) =  0.57735027
        matrix%co_ip(2,2) = -0.57735027
        matrix%co_ip(1,3) =  0.57735027
        matrix%co_ip(2,3) =  0.57735027
        matrix%co_ip(1,4) = -0.57735027
        matrix%co_ip(2,4) =  0.57735027

        matrix%weight = 1.0d0
        matrix%weight_subd = 1.0d0/(matrix%n_rec**2)
        allocate(matrix%co_ip_subd(2,matrix%Nip_subd))
        matrix%co_ip_subd = 0.0d0

        in = 0!inの初期化
        r(1) = -0.57735027
        r(2) =  0.57735027
        len1 = 1.0d0/dble(matrix%n_rec)
        len2 = r(1)/dble(matrix%n_rec)
        len3 = r(2)/dble(matrix%n_rec)

        do i = 1, matrix%n_rec!何列目の要素に注目しているか表す
            do j = 1, matrix%n_rec!何行目の要素に注目しているか表す
                do k = 1, 4!細分化された要素内で何番目の積分点に注目しているか表す
                    in = in + 1!要素内での積分点の番号を表す

                    ! matrix%co_ip_subd(1,in) = (dble(i) - 1)*2.0d0*len1 + len2 - 1.0d0 + len1
                    ! matrix%co_ip_subd(2,in) = (dble(j) - 1)*2.0d0*len1 + len3 - 1.0d0 + len1

                    if(k == 1)then
                        matrix%co_ip_subd(1,in) = (dble(i) - 1)*2.0d0*len1 + len1 + len2 - 1.0d0
                        matrix%co_ip_subd(2,in) = (dble(j) - 1)*2.0d0*len1 + len1 + len2 - 1.0d0
                    endif
                    if(k == 2)then
                        matrix%co_ip_subd(1,in) = (dble(i) - 1)*2.0d0*len1 + len1 + len3 - 1.0d0
                        matrix%co_ip_subd(2,in) = (dble(j) - 1)*2.0d0*len1 + len1 + len2 - 1.0d0
                        ! matrix%co_ip_subd(1,in) = (dble(i) - 1)*2.0d0*len1 + len1 + len2 - 1.0d0
                        ! matrix%co_ip_subd(2,in) = (dble(j) - 1)*2.0d0*len1 + len1 + len3 - 1.0d0
                    endif
                    if(k == 3)then
                        matrix%co_ip_subd(1,in) = (dble(i) - 1)*2.0d0*len1 + len1 + len3 - 1.0d0
                        matrix%co_ip_subd(2,in) = (dble(j) - 1)*2.0d0*len1 + len1 + len3 - 1.0d0
                    endif
                    if(k == 4)then
                        matrix%co_ip_subd(1,in) = (dble(i) - 1)*2.0d0*len1 + len1 + len2 - 1.0d0
                        matrix%co_ip_subd(2,in) = (dble(j) - 1)*2.0d0*len1 + len1 + len3 - 1.0d0
                        ! matrix%co_ip_subd(1,in) = (dble(i) - 1)*2.0d0*len1 + len1 + len3 - 1.0d0
                        ! matrix%co_ip_subd(2,in) = (dble(j) - 1)*2.0d0*len1 + len1 + len2 - 1.0d0
                    endif

                enddo
            enddo
        enddo

        matrix%weight_subd = 1.0d0/(matrix%n_rec*matrix%n_rec)

        ! !デバッグ
        ! do i = 1, 4
        !     write(*,*) matrix%co_ip(1,i),matrix%co_ip(2,i)
        !     write(*,*) matrix%co_ip_subd(1,i),matrix%co_ip_subd(2,i)
        ! enddo
    end subroutine initialize_matrix

    subroutine initialize_solver()!solver%の初期化
        implicit none
        integer(4) :: n

        solver%SoM_G = Garea%Nnode*2
        solver%SoM_X = Xarea%Nnode*2
        solver%SoM = solver%SoM_G + solver%SoM_X

        n = solver%SoM

        allocate(solver%Kmat(n, n), solver%Fvec(n), solver%uvec(n), solver%Pmat(n, n), &
        solver%PTmat(n, n), solver%Pinv(n, n), solver%rvec(n), solver%r0(n), solver%rvec_save(n), &
        solver%pvec(n), solver%alp(n), solver%bet(n), solver%pro(n),solver%uth(n))


        solver%Kmat = 0.0d0
        solver%Fvec = 0.0d0
        solver%uvec = 0.0d0
        solver%Pmat = 0.0d0
        solver%PTmat = 0.0d0
        solver%Pinv = 0.0d0
        solver%rvec = 0.0d0
        solver%r0 = 0.0d0
        solver%rvec_save = 0.0d0
        solver%pvec = 0.0d0
        solver%alp = 0.0d0
        solver%bet = 0.0d0
        solver%pro = 0.0d0
        solver%uth = 0.0d0
        solver%iter = 0
    end subroutine initialize_solver

    subroutine initialize_eval()
        implicit none
        eval%L2 = 0.0d0
        eval%L2_nu = 0.0d0
        eval%L2_de = 0.0d0
        eval%dif_nu = 0.0d0
        eval%dif_de = 0.0d0
        allocate(eval%elel2(Earea%Nnode),eval%ucal(Earea%Nnode*2),eval%uvec_th(Earea%Nnode*2))

        eval%elel2 = 0.0d0
        eval%ucal = 0.0d0
        eval%uvec_th = 0.0d0

    end subroutine initialize_eval



end module initialize