module variables
    use dbg
    implicit none

    integer(4) :: numL2 = 997,numX = 998,numVTK = 999,numbc = 11, numload = 12
    ! real(8) :: young=200000,poisson=0.3,ratioX=0.4d0,ratioR =0.25d0
    real(8) :: young=200000,poisson=0.3,ratioX,ratioR =0.25d0
    real(8) :: sig_inf = 100
    logical :: Xmode = .false.

    type :: matrix_struc
    !全体剛性方程式生成に関する変数
    real(8) :: Dmat(3,3),Jmat(2,2),Jinv(2,2),detJ,Bmat(3,8),BTmat(8,3),Kemat(8,8),KeGX(8,8)
    real(8) :: mat38(3,8),mat24(2,4),mat42_m(4,2),mat42_u(4,2),mat88(8,8),BonGT(8,3),mat24_m(2,4),mat24_u(2,4)
    real(8),allocatable :: KG(:,:),KGX(:,:),KX(:,:)

    !ガウス積分に関する変数
    integer(4) :: Nip=4,n_rec=4, Nip_subd
    real(8) :: co_ip(2,4),weight,weight_subd
    real(8),allocatable ::co_ip_subd(:,:)
    end type matrix_struc

    type :: meshdata_struc
    !メッシュ生成に用いる変数
    real(8) :: lengL,lengX,lengR,leng_mesh
    integer(4) :: diviL
    integer(4) :: Nenrich,Nconstrain!追加の物理量を付与する"節点"数,追加の物理量を0で拘束する"節点"数
    integer(4),allocatable :: xid(:),gid(:),Xnid(:)
    logical,allocatable :: inside(:)!エンリッチ付与の対象判定
    logical,allocatable :: subdivision(:)!またぐ判定(細分化)判定
    ! logical,allocatable :: node_inside(:)!節点の内外判定を行う
    logical,allocatable :: enrich_node(:)!節点がエンリッチ要素のものか判定

    end type meshdata_struc

    type :: area_struc
    integer(4) :: Nelem,Nnode
    integer(4),allocatable :: elem_node_id(:,:)!elem_node_id(節点番号,要素番号)
    real(8),allocatable :: co_node(:,:),Kmat(:,:)!co_node(方向,要素番号)
    end type area_struc

    type :: solver_struc
    integer(4) :: SoM,SoM_G,SoM_X,iter,max_iter = 1000000!反復回数
    real(8),allocatable :: Kmat(:,:),Fvec(:),uvec(:),Pmat(:,:),PTmat(:,:),Pinv(:,:)
    real(8),allocatable :: rvec(:),r0(:),rvec_save(:),pvec(:),alp(:),bet(:),pro(:),uth(:)
    real(8) :: load,Force!引張応力
    end type solver_struc

    type eval_struc
    real(8) :: L2,L2_nu,L2_de,dif_nu,dif_de!nuは分子,deは分母を意味する
    real(8),allocatable :: elel2(:),ucal(:),uvec_th(:)
    end type eval_struc

    type(matrix_struc) :: matrix
    type(meshdata_struc) :: meshdata
    type(area_struc) :: Garea,Xarea,Earea
    type(solver_struc) :: solver
    type(eval_struc) :: eval


end module variables
    