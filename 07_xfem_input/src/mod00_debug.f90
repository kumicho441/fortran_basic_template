module dbg
    implicit none

    logical :: dbg_show_processL,dbg_show_processM,dbg_disign_mesh,dbg_disign_nrec,dbg_no_subdivision,&
    dbg_disign_sig_inf,dbg_no_diag_scaling,dbg_vis_Xmesh,dbg_no_addition,dbg_not_showL2,dbg_outcome,&
    dbg_not_exchange_Emesh
    character(256) :: dbg_name
    real(8) :: lengL_disign,ratioR_disign,ratioX_disign
    integer(4) :: diviL_disign,n_rec_disign,sig_inf_disign
    integer(4) :: global_iter


    contains
    subroutine dbg_start_subroutineL()
        if(dbg_show_processL)then
            write(*,*)'Finish'
            write(*,*)'start subroutine ',dbg_name
        endif
    end subroutine dbg_start_subroutineL
    subroutine dbg_start_subroutineM()
        if(dbg_show_processM)then
            write(*,*)' Finish'
            write(*,*)' start subroutine ',dbg_name
        endif
    end subroutine dbg_start_subroutineM

    subroutine mod00_debug()
        implicit none

        !実行中のサブルーチンを表示
        dbg_show_processL = .false. !モジュール単位の大型サブルーチン
        dbg_show_processM = .false. !モジュール内の主要なサブルーチン

        !メッシュ分割を指定
        dbg_disign_mesh = .true.!(初期値)
            lengL_disign = 10 !(10.0d0)
            diviL_disign = 20 !(10)
            ratioR_disign = 0.25d0 !(0.25d0)
            ratioX_disign = ratioR_disign + lengL_disign*1.01/diviL_disign !(0.4d0)

        !積分領域細分化時の一辺あたり分割数指定
        dbg_disign_nrec = .false.!(4)
            n_rec_disign = 4

        !細分化なし
        dbg_no_subdivision = .false.

        !Xmesh可視化
        dbg_vis_Xmesh = .false.

        !σ_∞指定
        dbg_disign_sig_inf = .false.!(100)
            sig_inf_disign = 100

        !対角スケーリングなし
        dbg_no_diag_scaling = .false.

        !uG+uXの足しこみなし
        dbg_no_addition = .false.

        !誤差評価用メッシュに変位を付与しない
        dbg_not_exchange_Emesh = .false.

        !L2ノルム誤差の表示なし
        dbg_not_showL2 = .false.

        !解析結果の表示
        dbg_outcome = .false.

        




        
        !実行するデバッグの可視化
        if(dbg_show_processL)then
            write(*,*)'CAUTION "dbg_show_processL" is True!'
        endif
        if(dbg_show_processM)then
            write(*,*)'CAUTION "dbg_show_processM" is True!'
        endif
        if(dbg_disign_mesh)then
            write(*,*)'CAUTION "dbg_disign_mesh" is True!'
        endif
        if(dbg_disign_nrec)then
            write(*,*)'CAUTION "dbg_disign_nrec" is True!'
        endif
        if(dbg_no_subdivision)then
            write(*,*)'CAUTION "dbg_no_subdivision" is True!'
        endif
        if(dbg_vis_Xmesh)then
            write(*,*)'CAUTION "dbg_vis_Xmesh" is True!'
        endif
        if(dbg_disign_sig_inf)then
            write(*,*)'CAUTION "dbg_disign_sig_inf" is True!'
        endif
        if(dbg_no_diag_scaling)then
            write(*,*)'CAUTION "dbg_no_diag_scaling" is True!'
        endif
        if(dbg_no_addition)then
            write(*,*)'CAUTION "dbg_no_addition" is True!'
        endif
        if(dbg_not_exchange_Emesh)then
            write(*,*)'CAUTION "dbg_not_exchange_Emesh" is True!'
        endif
        if(dbg_not_showL2)then
            write(*,*)'CAUTION "dbg_not_showL2" is True!'
        endif
        if(dbg_outcome)then
            write(*,*)'CAUTION "dbg_outcome" is True!'
        endif


        if(dbg_show_processL.or.dbg_show_processM)then
            dbg_name= 'mod00_debug'
            write(*,*)'start subroutine ',dbg_name
        endif
        
    end subroutine mod00_debug

end module dbg