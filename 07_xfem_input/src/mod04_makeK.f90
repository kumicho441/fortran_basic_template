module makeK
    use dbg
    use initialize
    use variables
    implicit none

    contains

    subroutine get_co(area,elemid,et,gz,xco,yco)
        implicit none
        type(area_struc) :: area
        integer(4),intent(in) :: elemid
        real(8),intent(in) :: et,gz
        real(8),intent(out) :: xco,yco

        integer(4) :: i,nodeid(4)
        real(8) :: N(4)

        !要素に対応する節点番号
        do i = 1,4
            nodeid(i) = area%elem_node_id(i,elemid)
        enddo

        !要素の形状関数
        N(1) = (1-gz)*(1-et)/4
        N(2) = (1+gz)*(1-et)/4
        N(3) = (1+gz)*(1+et)/4
        N(4) = (1-gz)*(1+et)/4

        !求めるx,y座標の算出
        xco = 0.0d0
        yco = 0.0d0
        do i = 1,4
            xco = xco + N(i) * area%co_node(1,nodeid(i))
            yco = yco + N(i) * area%co_node(2,nodeid(i))
        enddo
    end subroutine get_co

    subroutine makeB(area,et,gz,elemid)
        implicit none
        type(area_struc) :: area
        real(8),intent(in) :: et,gz
        integer(4) :: elemid,i,j
        real(8) :: xco,yco
        
        matrix%Dmat(1,1) = 1/(1-poisson)
        matrix%Dmat(2,2) = 1/(1-poisson)
        matrix%Dmat(1,2) = poisson/(1-poisson)
        matrix%Dmat(2,1) = poisson/(1-poisson)
        matrix%Dmat(3,3) = 0.5
        matrix%Dmat = matrix%Dmat * young/(1+poisson)

        !Jmat
        matrix%mat24(1,1) = -(1-et)/4
        matrix%mat24(1,2) =  (1-et)/4
        matrix%mat24(1,3) =  (1+et)/4
        matrix%mat24(1,4) = -(1+et)/4
        matrix%mat24(2,1) = -(1-gz)/4
        matrix%mat24(2,2) = -(1+gz)/4
        matrix%mat24(2,3) =  (1+gz)/4
        matrix%mat24(2,4) =  (1-gz)/4
        
        do i = 1,4
            matrix%mat42_m(i,1) = area%co_node(1,area%elem_node_id(i,elemid))
            matrix%mat42_m(i,2) = area%co_node(2,area%elem_node_id(i,elemid))
        enddo
        
        matrix%Jmat = matmul(matrix%mat24, matrix%mat42_m)
        
        !detJ
        matrix%detJ = matrix%Jmat(1,1)*matrix%Jmat(2,2) - matrix%Jmat(1,2)*matrix%Jmat(2,1)
        
        !Jinv
        matrix%Jinv(1,1) =  matrix%Jmat(2,2)/matrix%detJ
        matrix%Jinv(1,2) = -matrix%Jmat(1,2)/matrix%detJ
        matrix%Jinv(2,1) = -matrix%Jmat(2,1)/matrix%detJ
        matrix%Jinv(2,2) =  matrix%Jmat(1,1)/matrix%detJ
        
        !Bmat
        matrix%mat24_m(1,1) = -(1-et)/4
        matrix%mat24_m(2,1) = -(1-gz)/4
        matrix%mat24_m(1,2) = (1-et)/4
        matrix%mat24_m(2,2) = -(1+gz)/4
        matrix%mat24_m(1,3) = (1+et)/4
        matrix%mat24_m(2,3) = (1+gz)/4
        matrix%mat24_m(1,4) = -(1+et)/4
        matrix%mat24_m(2,4) = (1-gz)/4
        
        matrix%mat24_u = 0.0d0
        matrix%mat24_u = matmul(matrix%Jinv, matrix%mat24_m)
        
        matrix%Bmat = 0.0d0

        call get_co(area,elemid,et,gz,xco,yco)

        if(Xmode .and. sqrt(xco**2 + yco**2) <= meshdata%lengR)then
            do i = 1,4
                matrix%Bmat(1,(2*i-1)) = -matrix%mat24_u(1,i)
                matrix%Bmat(3,2*i)     = -matrix%mat24_u(1,i)
                matrix%Bmat(2,2*i)     = -matrix%mat24_u(2,i)
                matrix%Bmat(3,(2*i-1)) = -matrix%mat24_u(2,i)
            enddo
        else
            do i = 1,4
                matrix%Bmat(1,(2*i-1)) = matrix%mat24_u(1,i)
                matrix%Bmat(3,2*i)     = matrix%mat24_u(1,i)
                matrix%Bmat(2,2*i)     = matrix%mat24_u(2,i)
                matrix%Bmat(3,(2*i-1)) = matrix%mat24_u(2,i)
            enddo
        endif

        ! !デバッグ
        ! do i = 1,4
        !     matrix%Bmat(1,(2*i-1)) = matrix%mat24_u(1,i)
        !     matrix%Bmat(3,2*i)     = matrix%mat24_u(1,i)
        !     matrix%Bmat(2,2*i)     = matrix%mat24_u(2,i)
        !     matrix%Bmat(3,(2*i-1)) = matrix%mat24_u(2,i)
        ! enddo
        
        !TraB
        do i = 1,3
            do j = 1,8
                matrix%BTmat(j,i) = matrix%Bmat(i,j)
            enddo
        enddo

    end subroutine makeB

    subroutine makeKe_single(area,elemid)!G領域、L領域の要素剛性行列生成
        implicit none
        type(area_struc) :: area
        integer(4),intent(in) :: elemid
        integer(4) :: ip
        real(8) :: et,gz,xco,yco

        do ip = 1, matrix%Nip

            et = matrix%co_ip(1,ip)
            gz = matrix%co_ip(2,ip)

            call makeB(area,et,gz,elemid)

            matrix%mat88 = 0.0d0!Keを作るための情報はmat88に保存している
            matrix%mat38 = 0.0d0

            matrix%mat38 = matmul(matrix%Dmat,matrix%Bmat)
            matrix%mat88 = matmul(matrix%BTmat,matrix%mat38)* matrix%detJ

            matrix%mat88 = matrix%mat88 * matrix%weight

            !デバッグ
            ! call get_co(area,elemid,et,gz,xco,yco)
            ! if(Xmode .and. sqrt(xco**2 + yco**2) <= meshdata%lengR)then
            !     matrix%Kemat = matrix%Kemat - matrix%mat88
            ! else
                matrix%Kemat = matrix%Kemat + matrix%mat88
            ! endif
    
        enddo
    end subroutine makeKe_single

    subroutine makeKe_single_subd(area,elemid)!G領域、L領域の要素剛性行列生成
        implicit none
        type(area_struc) :: area
        integer(4),intent(in) :: elemid
        integer(4) :: ip
        real(8) :: et,gz,xco,yco

        do ip = 1, matrix%Nip_subd

            et = matrix%co_ip_subd(1,ip)
            gz = matrix%co_ip_subd(2,ip)

            call makeB(area,et,gz,elemid)
            matrix%mat88 = 0.0d0
            matrix%mat38 = 0.0d0

            matrix%mat38 = matmul(matrix%Dmat,matrix%Bmat)
            matrix%mat88 = matmul(matrix%BTmat,matrix%mat38)* matrix%detJ!Keを作るための情報はmat88に保存している

            matrix%mat88 = matrix%mat88 * matrix%weight_subd

            !デバッグ
            ! call get_co(area,elemid,et,gz,xco,yco)
            ! if(Xmode .and. sqrt(xco**2 + yco**2) <= meshdata%lengR)then
            !     matrix%Kemat = matrix%Kemat - matrix%mat88
            ! else
                matrix%Kemat = matrix%Kemat + matrix%mat88
            ! endif

        enddo
    end subroutine makeKe_single_subd

    subroutine makeK_single(area)!G領域、X領域の剛性行列生成
        implicit none
        type(area_struc) :: area
        integer(4) :: elemid
        integer(4) :: i,j,n,m
        dbg_name ='makeK_single'
        call dbg_start_subroutineM()

        do elemid = 1, area%Nelem
            matrix%Kemat = 0.0d0
        
            !細分化処理
            if(meshdata%subdivision(elemid))then
                call makeKe_single_subd(area,elemid) !要素が境界をまたぐ場合のガウス積分
            else !またがない場合のガウス積分
                call makeKe_single(area,elemid)
            endif
        
            !足しこみ
            do i = 1, 4
                n = area%elem_node_id(i, elemid)
                do j = 1, 4
                    m = area%elem_node_id(j, elemid)
        
                    area%Kmat(2*n-1, 2*m-1) = area%Kmat(2*n-1, 2*m-1) + matrix%Kemat(2*i-1, 2*j-1)
                    area%Kmat(2*n-1, 2*m  ) = area%Kmat(2*n-1, 2*m  ) + matrix%Kemat(2*i-1, 2*j  )
                    area%Kmat(2*n  , 2*m-1) = area%Kmat(2*n  , 2*m-1) + matrix%Kemat(2*i  , 2*j-1)
                    area%Kmat(2*n  , 2*m  ) = area%Kmat(2*n  , 2*m  ) + matrix%Kemat(2*i  , 2*j  )
                enddo
            enddo
        enddo

    !     do i = 1, 8
    !         write(*,*) Sdata%Kemat(i,1), Sdata%Kemat(i,2), Sdata%Kemat(i,3), Sdata%Kemat(i,4), &
    !         Sdata%Kemat(i,5), Sdata%Kemat(i,6), Sdata%Kemat(i,7), Sdata%Kemat(i,8)
    ! enddo

    end subroutine makeK_single

    subroutine makeKGXe(Xelemid)!elemidはXmeshのものであることに注意
        integer(4),intent(in) :: Xelemid
        real(8) :: et,gz,mat38(3,8),mat83(8,3)
        integer(4) :: Gelemid,ip

        Gelemid = meshdata%xid(Xelemid)

        do ip = 1, matrix%Nip

            et = matrix%co_ip(1,ip)
            gz = matrix%co_ip(2,ip)

            Xmode = .false.
            call makeB(Garea,et,gz,Gelemid)
            mat83 = matrix%BTmat
            Xmode = .true.
            call makeB(Xarea,et,gz,Xelemid)
            mat38 = matmul(matrix%Dmat,matrix%Bmat)
            matrix%mat88 = matmul(mat83,mat38)*matrix%detJ*matrix%weight
            matrix%Kemat = matrix%Kemat + matrix%mat88

        enddo

    end subroutine makeKGXe

    subroutine makeKGXe_subd(Xelemid)!elemidはXmeshのものであることに注意
        integer(4),intent(in) :: Xelemid
        real(8) :: et,gz,mat38(3,8),mat83(8,3)
        integer(4) :: Gelemid,ip

        Gelemid = meshdata%xid(Xelemid)

        do ip = 1, matrix%Nip_subd

            et = matrix%co_ip_subd(1,ip)
            gz = matrix%co_ip_subd(2,ip)

            Xmode = .false.
            call makeB(Garea,et,gz,Gelemid)
            mat83 = matrix%BTmat
            Xmode = .true.
            call makeB(Xarea,et,gz,Xelemid)
            mat38 = matmul(matrix%Dmat,matrix%Bmat)
            matrix%mat88 = matmul(mat83,mat38)*matrix%detJ*matrix%weight_subd
            matrix%Kemat = matrix%Kemat + matrix%mat88

        enddo

    end subroutine makeKGXe_subd

    subroutine makeKGX()!連成項の作成
        implicit none
        integer(4) :: Xelemid,Gelemid
        integer(4) :: i,j,n,m
        dbg_name = 'makeKGX'
        call dbg_start_subroutineM()

        do Xelemid = 1, Xarea%Nelem

            Gelemid = meshdata%xid(Xelemid)
            matrix%Kemat = 0.0d0

            !細分化処理
            if(meshdata%subdivision(Xelemid))then 
                    call makeKGXe_subd(Xelemid)!要素が境界をまたぐ場合のガウス積分
            else
                    call makeKGXe(Xelemid) !またがない場合のガウス積分
            endif

            !足しこみ
            do i = 1,4
                n = Garea%elem_node_id(i,Gelemid)
                do j = 1,4
                    m = Xarea%elem_node_id(j,Xelemid)

                    matrix%KGX(2*n-1, 2*m-1) = matrix%KGX(2*n-1, 2*m-1) + matrix%Kemat(2*i-1, 2*j-1)
                    matrix%KGX(2*n-1, 2*m  ) = matrix%KGX(2*n-1, 2*m  ) + matrix%Kemat(2*i-1, 2*j  )
                    matrix%KGX(2*n  , 2*m-1) = matrix%KGX(2*n  , 2*m-1) + matrix%Kemat(2*i  , 2*j-1)
                    matrix%KGX(2*n  , 2*m  ) = matrix%KGX(2*n  , 2*m  ) + matrix%Kemat(2*i  , 2*j  )
                enddo
            enddo
            matrix%mat88 = 0.0d0
        enddo


    end subroutine makeKGX

    subroutine makeKmat()!各領域のKmatを合体
        implicit none
        integer(4) :: i,j,c1
        dbg_name = 'makeKmat'
        call dbg_start_subroutineM()

        !KG部分
        do i = 1, solver%SoM_G
            do j = 1, solver%SoM_G
                solver%Kmat(i,j) = Garea%Kmat(i,j)
            enddo
        enddo

        !KX部分
        C1 = solver%SoM_G
        do i = 1, solver%SoM_X
            do j = 1, solver%SoM_X
                solver%Kmat(c1 + i,c1 + j) = Xarea%Kmat(i,j)
            enddo
        enddo

        !KGX,KXG部分
        do i = 1, solver%SoM_G
            do j = 1, solver%SoM_X
                solver%Kmat(i,c1 + j) = matrix%KGX(i,j)
                solver%Kmat(c1 + j,i) = matrix%KGX(i,j)
            enddo
        enddo
                
    end subroutine makeKmat

    subroutine makeJ(area,et,gz,elemid)!mod08で座標変換に使用
        implicit none
        type(area_struc) :: area
        real(8),intent(in) :: et,gz
        integer(4) :: elemid,i,j
        real(8) :: xco,yco
        
        ! matrix%Dmat(1,1) = 1/(1-poisson)
        ! matrix%Dmat(2,2) = 1/(1-poisson)
        ! matrix%Dmat(1,2) = poisson/(1-poisson)
        ! matrix%Dmat(2,1) = poisson/(1-poisson)
        ! matrix%Dmat(3,3) = 0.5
        ! matrix%Dmat = matrix%Dmat * young/(1+poisson)

        !Jmat
        matrix%mat24(1,1) = -(1-et)/4
        matrix%mat24(1,2) =  (1-et)/4
        matrix%mat24(1,3) =  (1+et)/4
        matrix%mat24(1,4) = -(1+et)/4
        matrix%mat24(2,1) = -(1-gz)/4
        matrix%mat24(2,2) = -(1+gz)/4
        matrix%mat24(2,3) =  (1+gz)/4
        matrix%mat24(2,4) =  (1-gz)/4
        
        do i = 1,4
            matrix%mat42_m(i,1) = area%co_node(1,area%elem_node_id(i,elemid))
            matrix%mat42_m(i,2) = area%co_node(2,area%elem_node_id(i,elemid))
        enddo
        
        matrix%Jmat = matmul(matrix%mat24, matrix%mat42_m)
        
        !detJ
        matrix%detJ = matrix%Jmat(1,1)*matrix%Jmat(2,2) - matrix%Jmat(1,2)*matrix%Jmat(2,1)

    end subroutine makeJ


    subroutine mod04_makeK()
        implicit none
        integer(4) :: i,j
        dbg_name = 'mode04_makeK'
        call dbg_start_subroutineL()

        call initialize_matrix()
        call initialize_solver()
        Xmode = .false.
        call makeK_single(Garea)
        Xmode = .true.
        call makeK_single(Xarea)
        call makeKGX()
        call makeKmat()

        !デバッグ
        ! do i = 1,3
        !         write(*,*) matrix%Bmat(i,1), matrix%Bmat(i,2), matrix%Bmat(i,3), matrix%Bmat(i,4), &
        !                 matrix%Bmat(i,5), matrix%Bmat(i,6), matrix%Bmat(i,7), matrix%Bmat(i,8)
        ! enddo
        ! do i = 1,8
        !     write(*,*)Garea%Kmat(i,1),Garea%Kmat(i,2),Garea%Kmat(i,3),Garea%Kmat(i,4),&
        !     Garea%Kmat(i,5),Garea%Kmat(i,6),Garea%Kmat(i,7),Garea%Kmat(i,8)
        ! enddo
        ! do i = 1, 8
        !     write(*,*) matrix%Kemat(i,1), matrix%Kemat(i,2), matrix%Kemat(i,3), matrix%Kemat(i,4), &
        !                matrix%Kemat(i,5), matrix%Kemat(i,6), matrix%Kemat(i,7), matrix%Kemat(i,8)
        ! enddo
    end subroutine mod04_makeK







end module makeK