program main
    !use
    use dbg
    use variables
    use initialize
    use makeMesh
    use makeK
    use insert_BC
    use CGsolver
    use addXtoG
    use evaluate
    use paraview
    use finalize

    implicit none

    !call
    !通常のプログラム

    call mod00_debug()
    call mod03_makeMesh()
    call mod04_makeK()
    call mod05_BC()
    call mod06_CGsolver()
    call mod07_uG_plus_uX()
    call mod08_L2norm()
    call mod09_vis_outcome()

        if(.not. dbg_not_showL2)then
        write(*,*)'L2ノルム誤差 =',eval%L2
        endif

    !自動反復プログラム
    ! write(*,*)'要素数,節点数,反復回数,L2ノルム誤差'
    ! call mod10_repeat_program()



    if(dbg_show_processL.or.dbg_show_processM)then
        write(*,*)'Finish'
        write(*,*)'program Finish'
    endif

    if(dbg_outcome)then
        write(*,*)'要素数,節点数,反復回数,L2ノルム誤差 ='
        write(*,*)Earea%Nelem,',',Earea%Nnode,',',solver%iter,',',eval%L2
    endif


end program main