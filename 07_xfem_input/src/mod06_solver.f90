module CGsolver
    use dbg
    use variables
    use initialize
    implicit none

    contains

    subroutine diag_scaling()
        implicit none
        integer(4) :: i

        dbg_name = 'diag_scaling'
        call dbg_start_subroutineM()

        if(.not. dbg_no_diag_scaling)then
            do i = 1, solver%SoM
                solver%Pmat(i,i) = sqrt(abs(solver%Kmat(i,i)))
                solver%PTmat(i,i) = solver%Pmat(i,i)
                solver%Pinv(i,i) = 1/sqrt(abs(solver%Kmat(i,i)))
            enddo
            solver%Kmat = matmul(solver%Kmat, solver%Pinv)
            solver%Kmat = matmul(solver%Pinv, solver%Kmat)
            solver%uvec = matmul(solver%PTmat, solver%uvec)
            solver%Fvec = matmul(solver%Pinv, solver%Fvec)
        endif
    end subroutine diag_scaling

    subroutine CGsolver_main()
        implicit none

        integer(4) :: i
        real(8) :: x,y,z

        dbg_name = 'CGsolver_main'
        call dbg_start_subroutineM()

        call diag_scaling()
        solver%alp = 0.0d0
        solver%pro = 0.0d0
        solver%rvec = 0.0d0

        solver%pro = matmul(solver%Kmat, solver%uvec)
        solver%rvec = solver%Fvec - solver%pro ! r0の定義
        solver%pvec = solver%rvec ! p0の定義
        solver%r0 = solver%rvec ! r0の値は保存しておく

        do i = 1, solver%max_iter
            solver%pro = matmul(solver%Kmat, solver%pvec) ! Kmatとp_(i-1)の積を求める

            solver%alp = dot_product(solver%rvec, solver%rvec) / dot_product(solver%pvec, solver%pro) ! α(i)の計算
            solver%uvec = solver%uvec + solver%alp * solver%pvec ! u_(i)の計算
            solver%rvec_save = solver%rvec ! r_(i-1)を保存

            solver%rvec = solver%rvec - solver%alp * solver%pro!r(i)の更新

            x = sqrt(dot_product(solver%rvec, solver%rvec))
            y = sqrt(dot_product(solver%r0, solver%r0))
            z = dot_product(solver%rvec_save, solver%rvec_save)

            if ((x/y) <= 1.0E-8) then ! 繰り返し判定
                if (.not. dbg_no_diag_scaling) then
                    solver%uvec = matmul(solver%Pinv, solver%uvec) ! 対角スケーリングを利用する場合の処理
                endif
                solver%iter = i!反復回数を記録
                exit
            endif

            solver%bet = x*x / z
            solver%pvec = solver%rvec + solver%bet * solver%pvec
        enddo

        ! write(*,*)i

        if (i == solver%max_iter) then
            write(*, *) "CG method error"
            stop
        endif

    end subroutine CGsolver_main

    subroutine mod06_CGsolver()
        implicit none

        dbg_name = 'mod06_CGsolver'
        call dbg_start_subroutineL()

        call CGsolver_main()
    end subroutine mod06_CGsolver



end module CGsolver