module insert_BC
    use dbg
    use variables
    use initialize
    use makeK
    implicit none

    contains

    subroutine insert_Neumann()!外力の境界条件
        implicit none


        dbg_name = 'insert_Neumann'
        call dbg_start_subroutineM()
        


    end subroutine insert_Neumann

    subroutine insert_Dirichlet()!変位の境界条件
        implicit none
        integer(4) :: i,j,k,n,c1,c2,Nbc,Ndim,Nid,dim
        real(8) :: x,ka,mu,ra,th,xco,yco,pi,c3

        dbg_name = 'insert_Dirichlet'
        call dbg_start_subroutineM()


        c1 = meshdata%diviL!Gmesh一辺分割数
        c2 = nint(sqrt(dble(Xarea%Nelem)))!Xmesh一辺分割数

        !外周理論解の作成(ついでに領域全体の理論解を求めておく)
        if(dbg_disign_sig_inf)then
            sig_inf = sig_inf_disign
        endif

        pi = 4*atan(1.0d0)
        ka = (3-poisson)/(1+poisson)
        mu = young/(2*(1+poisson))
        c3 = sig_inf*meshdata%lengR/(8*mu)
        
        solver%uth(1) = 0.0d0
        solver%uth(2) = 0.0d0
        do i = 2,Garea%Nnode
            
            xco = Garea%co_node(1,i)
            yco = Garea%co_node(2,i)
            ra = sqrt(xco**2 + yco**2)

            !デバッグ

            if(xco <= 1.0E-08)then!atan=∞となる場合の処理
                th = pi/2
            else
                th = atan(yco/xco)
            endif

            solver%uth(i*2-1) = c3*(ra*(ka+1)*cos(th)/meshdata%lengR + &
                                2*meshdata%lengR*((1+ka)*cos(th) + cos(3*th))/ra - &
                                2*(meshdata%lengR**3)*cos(3*th)/(ra**3))

            solver%uth(i*2  ) = c3*(ra*(ka-3)*sin(th)/meshdata%lengR + &
                                2*meshdata%lengR*((1-ka)*sin(th) + sin(3*th))/ra - &
                                2*(meshdata%lengR**3)*sin(3*th)/(ra**3))

            if(ra < meshdata%lengR)then
                solver%uth(i*2-1) = 0.0d0
                solver%uth(i*2) = 0.0d0
            endif

            ! !デバッグ
            ! write(*,*)solver%uth(i*2-1),solver%uth(i*2)
        enddo


        !bc.datファイルの作成
        open(unit = numbc, file = 'bc.dat', status = 'replace')
            write(numbc,'(i0,a)')8*c1 + meshdata%Nconstrain*2 ,' 2'!{境界条件数,次元}

        !G領域固定
        do i = 1, c1 + 1
            write(numbc, '(i0,a,i0,a,G0)')i,' ',1,' ',solver%uth(i*2-1)
            write(numbc, '(i0,a,i0,a,G0)')i,' ',2,' ',0.0d0
        enddo

        do i = 2, c1 
            j = (c1+1)*(i-1) +1
            k = (c1+1)*i
            write(numbc, '(i0,a,i0,a,G0)')j,' ',1,' ',0.0d0
            write(numbc, '(i0,a,i0,a,G0)')j,' ',2,' ',solver%uth(j*2  )
            write(numbc, '(i0,a,i0,a,G0)')k,' ',1,' ',solver%uth(k*2-1)
            write(numbc, '(i0,a,i0,a,G0)')k,' ',2,' ',solver%uth(k*2  )
        enddo

        do i = 1, c1+1
            j =  (c1+1)*c1 + i
            write(numbc, '(i0,a,i0,a,G0)')j,' ',1,' ',solver%uth(j*2-1)
            write(numbc, '(i0,a,i0,a,G0)')j,' ',2,' ',solver%uth(j*2  )
        enddo


        !X領域固定
        ! do i = 1, c2 + 1
        !     j = Garea%Nnode + i
        !     write(numbc, '(i0,a,i0,a,G0)')j,' ',2,' ',0.0d0
        ! enddo

        ! do i = 2, c2 
        !     j = Garea%Nnode + (c2+1)*(i-1) + 1
        !     k = Garea%Nnode + (c2+1)*i
        !     write(numbc, '(i0,a,i0,a,G0)')j,' ',1,' ',0.0d0
        !     write(numbc, '(i0,a,i0,a,G0)')k,' ',1,' ',0.0d0
        !     write(numbc, '(i0,a,i0,a,G0)')k,' ',2,' ',0.0d0
        ! enddo

        ! do i = 1, c2+1
        !     j = Garea%Nnode + (c2+1)*c2 + i
        !     write(numbc, '(i0,a,i0,a,G0)')j,' ',1,' ',0.0d0
        !     write(numbc, '(i0,a,i0,a,G0)')j,' ',2,' ',0.0d0
        ! enddo
        do i = 1, Xarea%Nnode
            if(.not. meshdata%enrich_node(i))then
                j = Garea%Nnode + i

                write(numbc, '(i0,a,i0,a,G0)')j,' ',1,' ',0.0d0
                write(numbc, '(i0,a,i0,a,G0)')j,' ',2,' ',0.0d0

            endif
        enddo
                

        close(numbc)
        !bc.datファイルの読み取り
        open(numbc,file = 'bc.dat', status = 'old')
        read(numbc,*)Nbc,Ndim

        do i = 1,Nbc
            read(numbc,*)Nid,dim,x
            n = (Nid-1)*Ndim + dim

            do j = 1,solver%SoM
                solver%Fvec(j) = solver%Fvec(j) - solver%Kmat(j,n)*x
            enddo
            do j = 1,solver%SoM
                solver%Kmat(n,j) = 0.0d0
                solver%Kmat(j,n) = 0.0d0
            enddo

            solver%Kmat(n,n) = 1.0d0
            solver%uvec(n) = x
            solver%Fvec(n) = x
        enddo

        close(numbc)
    end subroutine insert_Dirichlet

    subroutine mod05_BC()
        dbg_name = 'mod05_BC'
        call dbg_start_subroutineL()

        call insert_Neumann()
        call insert_Dirichlet()

    end subroutine mod05_BC
end module insert_BC