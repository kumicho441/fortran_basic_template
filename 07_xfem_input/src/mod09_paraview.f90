module paraview
    use dbg
    use variables
    use initialize
    implicit none

    contains

    subroutine visG()
        implicit none
        integer(4) :: i

        open(unit = numVTK, file = '07_xfem.vtk', status = 'old', position = 'append')

        !数値解の入力
        write(numVTK, '(a,I0)') 'POINT_DATA ',Garea%Nnode
        write(numVTK, '(a)')'VECTORS d_cal_Vector_Data float'      
        do i = 1, Garea%Nnode
            write(numVTK,'(G0,a,G0,a)') solver%uvec(2*i-1),' ',solver%uvec(2*i),' 0.0'
        enddo    
        
        !理論解の入力
        write(numVTK, '(a)')'VECTORS d_th_Vector_Data float'
        do i = 1, Garea%Nnode
            write(numVTK,'(G0,a,G0,a)')solver%uth(2*i-1),' ',solver%uth(2*i),' 0.0'
        enddo

        close(numVTK)
    end subroutine visG

    subroutine visX()
        implicit none
        integer(4) :: i,j
        if(dbg_vis_Xmesh)then
            dbg_name = 'visX'
            call dbg_start_subroutineM()

            open(unit = numX, file = 'Xmesh.vtk', status = 'old', position = 'append')

            !数値解の入力
            write(numX, '(a,I0)') 'POINT_DATA ',Xarea%Nnode
            write(numX, '(a)')'VECTORS d_cal_Vector_Data float' 

            j = Garea%Nnode*2
            do i = 1, Xarea%Nnode
                write(numX,'(G0,a,G0,a)') solver%uvec(j+2*i-1),' ',solver%uvec(j+2*i),' 0.0'
            enddo    
            close(numX)
        endif

    end subroutine visX

    subroutine visE()
        implicit none
        integer(4) :: i

        open(unit = numL2, file = '07_xfem_eval.vtk', status = 'old', position = 'append')

        !数値解の入力
        write(numL2, '(a,I0)') 'POINT_DATA ',Earea%Nnode
        write(numL2, '(a)')'VECTORS d_cal_Vector_Data float'      
        do i = 1, Earea%Nnode
            write(numL2,'(G0,a,G0,a)') eval%ucal(2*i-1),' ',eval%ucal(2*i),' 0.0'
        enddo    
        
        !理論解の入力
        write(numL2, '(a)')'VECTORS d_th_Vector_Data float'
        do i = 1, Earea%Nnode
            write(numL2,'(G0,a,G0,a)')eval%uvec_th(2*i-1),' ',eval%uvec_th(2*i),' 0.0'
        enddo

        !各点におけるL2誤差
        write(numL2, '(a,I0)') 'CELL_DATA ',Earea%Nelem
        write(numL2, '(a)')'SCALARS L2_Data float'
        write(numL2, '(a)')'LOOKUP_TABLE default'
        do i = 1, Earea%Nelem !>各要素におけるL2誤差
            write(numL2,'(G0)') eval%elel2(i)
        enddo

        close(numL2)
    end subroutine visE

    subroutine mod09_vis_outcome()
        implicit none

        dbg_name = 'mod09_vis_outcome'
        call dbg_start_subroutineL()

        call visG()
        call visX()
        call visE()

    end subroutine mod09_vis_outcome






end module paraview