module evaluate
    use dbg
    use variables
    use initialize
    use makeK
    implicit none

    contains
    subroutine get_Gmeshid(xco,yco,Gelemid)!座標から対応するG要素を得る
        implicit none
        real(8),intent(in) :: xco,yco
        integer(4),intent(out) :: Gelemid

        integer(4) :: xid,yid

        xid = int(xco/meshdata%leng_mesh)
        yid = int(yco/meshdata%leng_mesh)



        !Gelemidエラーに対する措置
        if(xid >= meshdata%diviL)then
            xid = meshdata%diviL-1
        endif
        if(yid >= meshdata%diviL)then
            yid = meshdata%diviL-1
        endif

        Gelemid = yid*meshdata%diviL + xid + 1!Gelemid導出


        ! if(Gelemid > Garea%Nelem)then
        !     Gelemid = Garea%Nelem
        ! endif

        ! !デバッグ
        ! write(*,*)'xid,yid=',xid,yid
    end subroutine get_Gmeshid

    subroutine get_etgz(xco,yco,Gelemid,et_g,gz_g)!座標とG要素番号からG要素におけるet,gzの値を得る
        implicit none
        real(8),intent(in) :: xco,yco
        integer(4),intent(in) :: Gelemid
        real(8),intent(out) :: et_g,gz_g

        real(8) :: x0,y0

        x0 = Garea%co_node(1,Garea%elem_node_id(1,Gelemid))
        y0 = Garea%co_node(2,Garea%elem_node_id(2,Gelemid))

        gz_g = 2*(xco - x0)/meshdata%leng_mesh - 1
        et_g = 2*(yco - y0)/meshdata%leng_mesh - 1
    end subroutine get_etgz

    subroutine trans_mesh()!格子状メッシュの変位を誤差評価用メッシュに移す
        implicit none
        real(8) :: xco,yco,N(4),et_g,gz_g
        integer(4) :: i,c1,nodeid,Gelemid

        if(.not. dbg_not_exchange_Emesh)then


            dbg_name = 'trans_mesh'
            call dbg_start_subroutineM()

            !E領域全節点で繰り返す
            do nodeid = 1, Earea%Nnode

                !節点座標入手
                xco = Earea%co_node(1,nodeid)
                yco = Earea%co_node(2,nodeid)

                !対応するG要素のetgz入手
                call get_Gmeshid(xco,yco,Gelemid)

                ! !デバッグ
                ! write(*,*)'nodeid,Gelemid=',nodeid,Gelemid

                ! !デバッグ
                ! write(*,*)nodeid

                call get_etgz(xco,yco,Gelemid,et_g,gz_g)

                !デバッグ
                ! write(*,*)'nodeid,et_g,gz_g=',nodeid,et_g,gz_g

                !形状関数の値を算出する
                N(1) = (1 - gz_g) * (1 - et_g) / 4
                N(2) = (1 + gz_g) * (1 - et_g) / 4
                N(3) = (1 + gz_g) * (1 + et_g) / 4
                N(4) = (1 - gz_g) * (1 + et_g) / 4

                do i = 1,4
                    eval%ucal(nodeid*2 - 1) = eval%ucal(nodeid*2 - 1) + &
                    N(i)*solver%uvec(Garea%elem_node_id(i,Gelemid)*2-1)

                    eval%ucal(nodeid*2    ) = eval%ucal(nodeid*2    ) + &
                    N(i)*solver%uvec(Garea%elem_node_id(i,Gelemid)*2  )
                enddo
            enddo

        endif
    end subroutine trans_mesh

    subroutine transmesh_th()!誤差評価用メッシュでの理論解を求める
        implicit none
        integer(4) :: i,j,k,n,c1,Nbc,Ndim,Nid,dim
        real(8) :: x,ka,mu,ra,th,xco,yco,pi,c3

        dbg_name = 'transmesh_th'
        call dbg_start_subroutineM()


        c1 = meshdata%diviL!Gmesh一辺分割数

        pi = 4*atan(1.0d0)
        ka = (3-poisson)/(1+poisson)
        mu = young/(2*(1+poisson))
        c3 = sig_inf*meshdata%lengR/(8*mu)
        
        ! eval%uvec_th(1) = 0.0d0
        ! eval%uvec_th(2) = 0.0d0
        do i = 1,Earea%Nnode
            !デバッグ
            ! write(*,*)'Enodeid = ',i


            xco = Earea%co_node(1,i)
            yco = Earea%co_node(2,i)
            ra = sqrt(xco**2 + yco**2)

            if(xco <= 1.0E-08)then!atan=∞となる場合の処理
                th = pi/2
            else
                th = atan(yco/xco)
            endif

            eval%uvec_th(i*2-1) = c3*(ra*(ka+1)*cos(th)/meshdata%lengR + &
                                2*meshdata%lengR*((1+ka)*cos(th) + cos(3*th))/ra - &
                                2*(meshdata%lengR**3)*cos(3*th)/(ra**3))

            eval%uvec_th(i*2  ) = c3*(ra*(ka-3)*sin(th)/meshdata%lengR + &
                                2*meshdata%lengR*((1-ka)*sin(th) + sin(3*th))/ra - &
                                2*(meshdata%lengR**3)*sin(3*th)/(ra**3))

            if(ra < meshdata%lengR)then
                eval%uvec_th(i*2-1) = 0.0d0
                eval%uvec_th(i*2) = 0.0d0
            endif

            ! !デバッグ
            ! write(*,*)solver%uth(i*2-1),solver%uth(i*2)
        enddo        

    end subroutine transmesh_th

    subroutine makeL2_sub(ip,elemid)!誤差評価用メッシュでのL2ノルム誤差評価(1/2)
        implicit none
        integer(4),intent(in) :: ip,elemid

        real(8) :: et,gz,pi,ka,mu,et_g,gz_g
        real(8) :: N(4),ipxco,ipyco!ipは積分点を表す
        real(8) :: ux_th,uy_th,ux_cal,uy_cal,x,y
        real(8) :: ra,ur,th,c1,c2,c3
        integer(4) :: i,Gelemid

        ! dbg_name = 'makeL2_sub'
        ! call dbg_start_subroutineM()

        et = matrix%co_ip(1,ip)
        gz = matrix%co_ip(2,ip)
        pi = 4*atan(1.0d0)
        ka = (3-poisson)/(1+poisson)
        mu = young/(2*(1+poisson))
        c3 = sig_inf*meshdata%lengR/(8*mu)

        ipxco = 0.0d0
        ipyco = 0.0d0
        ux_cal = 0.0d0
        uy_cal = 0.0d0

        N(1) = (1 - gz) * (1 - et) / 4
        N(2) = (1 + gz) * (1 - et) / 4
        N(3) = (1 + gz) * (1 + et) / 4
        N(4) = (1 - gz) * (1 + et) / 4  

        do i = 1,4
            ipxco = ipxco + N(i)*Earea%co_node(1,Earea%elem_node_id(i,elemid))!積分点のx座標
            ipyco = ipyco + N(i)*Earea%co_node(2,Earea%elem_node_id(i,elemid))!積分点のy座標
        enddo

        ra = sqrt(ipxco**2 + ipyco**2)
        if(ipxco <= 1.0E-08)then!atan=∞となる場合の処理
            th = pi/2
        else
            th = atan(ipyco/ipxco)
        endif

        ux_th = c3*(ra*(ka+1)*cos(th)/meshdata%lengR + &
                                2*meshdata%lengR*((1+ka)*cos(th) + cos(3*th))/ra - &
                                2*(meshdata%lengR**3)*cos(3*th)/(ra**3))

        uy_th = c3*(ra*(ka-3)*sin(th)/meshdata%lengR + &
                                2*meshdata%lengR*((1-ka)*sin(th) + sin(3*th))/ra - &
                                2*(meshdata%lengR**3)*sin(3*th)/(ra**3))


        ! do i = 1,4
        !     ux_cal = ux_cal + N(i) * eval%ucal(Earea%elem_node_id(i,elemid)*2 - 1)
        !     uy_cal = uy_cal + N(i) * eval%ucal(Earea%elem_node_id(i,elemid)*2    )
        ! enddo


        call get_Gmeshid(ipxco,ipyco,Gelemid)
        call get_etgz(ipxco,ipyco,Gelemid,et_g,gz_g)

        N(1) = (1 - gz_g) * (1 - et_g) / 4
        N(2) = (1 + gz_g) * (1 - et_g) / 4
        N(3) = (1 + gz_g) * (1 + et_g) / 4
        N(4) = (1 - gz_g) * (1 + et_g) / 4  

        ! do i = 1,4
        !     ux_cal = ux_cal + N(i) * eval%ucal(Earea%elem_node_id(i,elemid)*2 - 1)
        !     uy_cal = uy_cal + N(i) * eval%ucal(Earea%elem_node_id(i,elemid)*2    )
        ! enddo

        do i = 1,4
            ux_cal = ux_cal + N(i) * solver%uvec(Garea%elem_node_id(i,Gelemid)*2 - 1)
            uy_cal = uy_cal + N(i) * solver%uvec(Garea%elem_node_id(i,Gelemid)*2    )
        enddo
        
        call makeJ(Earea,et,gz,elemid)
        x = ((ux_cal - ux_th) ** 2 + (uy_cal - uy_th) ** 2) * matrix%detJ
        y = (ux_th ** 2 + uy_th ** 2) * matrix%detJ

        eval%dif_nu = eval%dif_nu + x !要素のL2誤差(分子)
        eval%dif_de = eval%dif_de + y !要素のL2誤差(分母)

        eval%L2_nu = eval%L2_nu + x
        eval%L2_de = eval%L2_de + y
    end subroutine makeL2_sub

    subroutine makeL2_main()!誤差評価用メッシュでのL2ノルム誤差評価(2/2)
        implicit none

        real(8) :: x,ra,th,ur,c1,c2
        integer(4) :: elemid,i

        dbg_name ='makeL2_main'
        call dbg_start_subroutineM

        do elemid = 1, Earea%Nelem
            eval%dif_nu = 0.0d0
            eval%dif_de = 0.0d0
            
            do i = 1,matrix%Nip
                call makeL2_sub(i,elemid)
            enddo

            eval%elel2(elemid) = sqrt(eval%dif_nu/eval%dif_de)
        enddo

        eval%L2 = sqrt(eval%L2_nu)/sqrt(eval%L2_de)

        ! if(.not. dbg_not_showL2)then
        ! write(*,*)'L2ノルム誤差 =',eval%L2
        ! endif

    end subroutine makeL2_main

    subroutine mod08_L2norm()
        implicit none 
        dbg_name = 'mod08_L2norm'
        call dbg_start_subroutineL()

        call initialize_eval()

        call trans_mesh()
        call transmesh_th()
        call makeL2_main()


    end subroutine mod08_L2norm

end module evaluate