subroutine help_makeKe(poisson,young,et,gz,time,numbers,SoM,Noelem,Nonode,co_node,Kemat) 
    !>help_makeK(ポアソン比,ヤング率,η,ξ,何個目の要素か,変数numbers,SoM,要素数,節点数,節点座標,(出力)Kmat)

    ! implicit none
    ! integer,intent(in) :: SoM,time,Noelem,numbers(Noelem,4)
    ! real(8),intent(in) :: poisson,young,et,gz!> [修正済み]行列の大きさが固定されていることに注意
    ! real(8) :: Kemat(8,8),Dmat(3,3),Jmat(2,2),detJ,Jinv(2,2),Bmat(3,8),TraB(8,3),mat38(3,8)
    ! integer :: i,j,k,l
    ! real(8) :: x

    implicit none
    ! Integer variables with intent(in)
    integer, intent(in) :: SoM, time, Noelem
    integer, intent(in) :: numbers(Noelem, 4)

    ! Real(8) variables with intent(in)
    real(8), intent(in) :: poisson, young, et, gz,co_node(Nonode,3)

    ! Real(8) variables for matrices and other computations
    real(8) :: Kemat(8, 8), Dmat(3, 3), Jmat(2, 2), detJ, Jinv(2, 2)
    real(8) :: Bmat(3, 8), TraB(8, 3), mat38(3, 8)
    real(8) :: x

    ! Allocatable real(8) array for node coordinates
    ! real(8), allocatable :: co_node1(:,:)

    real(8) :: mat24_1(2, 4), co_node2(4, 2)
    real(8) :: mat42_1(4,2),mat42_2(4,2) !>それぞれNをx,yやη,ξで微分したものを表す

    ! Integer variables for loop indices and node counts
    integer :: i, j, k, l, Nonode, numnode


    Kemat = 0.0
    Dmat = 0; Jmat = 0; Jinv = 0; Bmat = 0; TraB = 0; mat38 = 0;detJ = 0 !>初期化
 !> Dマトリックス導出
    do i =1,2
        Dmat(i,i) = 1/(1-poisson)
        Dmat(i,3) = 0
        Dmat(3,i) = 0
    enddo

    Dmat(1,2) = poisson/(1-poisson)
    Dmat(2,1) = poisson/(1-poisson)
    Dmat(3,3) = 0.5

    x = young/(1+poisson)
    do i = 1,3
        do j = 1,3
            Dmat(i,j) = x*Dmat(i,j)
        enddo
    enddo

 !> Jマトリックス導出
    Jmat = 0.0d0
    numnode = 11
    mat24_1 = 0
    co_node2 = 0


    !> ここから行列に値を代入する
    mat24_1(1,1) = -(1-et)/4
    mat24_1(1,2) = (1-et)/4
    mat24_1(1,3) = (1+et)/4
    mat24_1(1,4) = -(1+et)/4
    mat24_1(2,1) = -(1-gz)/4
    mat24_1(2,2) = -(1+gz)/4
    mat24_1(2,3) = (1+gz)/4
    mat24_1(2,4) = (1-gz)/4

    do i = 1,4 !> 注目している要素に対応する節点座標を代入 この時点で値が対応していない
        j = numbers(time,i)
        ! write(*,*)j
        co_node2(i,1) = co_node(j,1)
        co_node2(i,2) = co_node(j,2) 
    enddo 
    ! write(*,*)"co="

    ! do i = 1,4
    !     write(*,*)co_node2(i,1),co_node2(i,2)
    ! enddo
    do k = 1,2
        do j = 1, 2
             do i = 1, 4
                Jmat(k,j) = Jmat(k,j) + mat24_1(k,i)*co_node2(i,j)
            end do
        end do
    enddo
    ! write(*,*)"Jmat =",Jmat

 !detJの計算
    detJ = Jmat(1,1)*Jmat(2,2) - Jmat(1,2)*Jmat(2,1)

 !Jマトリックスの逆行列を求める


    Jinv(1,1) = Jmat(2,2)/detJ
    Jinv(1,2) = -Jmat(1,2)/detJ
    Jinv(2,1) = -Jmat(2,1)/detJ
    Jinv(2,2) = Jmat(1,1)/detJ


 !Bマトリックス導出

    mat42_2(1,1) = -(1-et)/4
    mat42_2(1,2) = -(1-gz)/4
    mat42_2(2,1) = (1-et)/4
    mat42_2(2,2) = -(1+gz)/4
    mat42_2(3,1) = (1+et)/4
    mat42_2(3,2) = (1+gz)/4
    mat42_2(4,1) = -(1+et)/4
    mat42_2(4,2) = (1-gz)/4

    i = 0
    !> ここからNのx,y微分を求める
    do i = 1, 4 !>繰り返し回数は節点数と関係ないことに注意
        mat42_1(i,1) =  Jinv(1,1)*mat42_2(i,1) + Jinv(1,2)*mat42_2(i,2)
        mat42_1(i,2) =  Jinv(2,1)*mat42_2(i,1) + Jinv(2,2)*mat42_2(i,2)
    enddo


    !> ここから具体的な値を計算・代入する
    Bmat = 0
    do i = 1,4!>繰り返し数が固定されていることに注意
        !>write(*,*)i
        Bmat(1,(2*i-1)) = mat42_1(i,1)
        Bmat(3,2*i) = mat42_1(i,1)
        Bmat(2,2*i) = mat42_1(i,2)
        Bmat(3,(2*i-1)) = mat42_1(i,2)
    enddo
 !Bマトリックスの逆行列を求める
    do i=1,3
        do j=1,8
            traB(j,i) = Bmat(i,j)
        enddo
    enddo


 !Kマトリックスの具体的な値を求める
    do j = 1,8

    do i = 1,3 !>繰り返しの回数が固定されていることに注意
            mat38(1,j) = mat38(1,j) + Dmat(1,i)*Bmat(i,j)
            mat38(2,j) = mat38(2,j) + Dmat(2,i)*Bmat(i,j)
            mat38(3,j) = mat38(3,j) + Dmat(3,i)*Bmat(i,j)
    enddo
    enddo

    do k = 1,8
    do j = 1,8
        do i = 1,3 !>繰り返しの回数が固定されていることに注意
            Kemat(k,j) = Kemat(k,j) + TraB(k,i)*mat38(i,j)         
        enddo
    enddo
    enddo

    do i =1,8
    do j = 1,8
        Kemat(i,j) = Kemat(i,j)*detJ
    enddo
    enddo

    ! write(*,*)Kemat
    ! write(*,*)"detJ=",detJ

    !>求めた要素剛性マトリックスを剛性マトリックスに対応付ける。3次元未対応
    ! do i = 1,4
    !     do j =1,4
    !         k = numbers(time,i); l = numbers(time,j)
    !         Kmat(2*k-1,2*l-1) = Kmat(2*k-1,2*l-1) + Kemat(2*i-1,2*j-1)
    !         Kmat(2*k-1,2*l) = Kmat(2*k-1,2*l) + Kemat(2*i-1,2*j)
    !         Kmat(2*k,2*l-1) = Kmat(2*k,2*l-1) + Kemat(2*i,2*j-1)
    !         Kmat(2*k,2*l) = Kmat(2*k,2*l) + Kemat(2*i,2*j) 
    !     enddo
    ! enddo

end subroutine help_makeKe


subroutine makeK(poisson,young,time,numbers,SoM,Noelem,Nonode,co_node,Kmat) 
        !>makeK(ポアソン比,ヤング率,何個目の要素か,変数numbers,SoM,要素数,節点数,節点座標,Kマトリックス(出力))
    implicit none
    real(8),intent(in) :: poisson,young,co_node(Nonode,3)
    integer,intent(in) :: SoM,time,Noelem,numbers(Noelem,4),Nonode
    integer :: i,j,k,l
    real(8) :: Kemat(8,8),Kmat(SoM,SoM),mat1(8,8),x !>[修正済み]行列の大きさが固定されていることに注意


    x = 0.57735

    mat1 = 0.0
    Kemat = 0.0
    call help_makeKe(poisson,young,-x,-x,time,numbers,SoM,Noelem,Nonode,co_node,mat1) !>ガウス積分を計算するために値を代入していく

    do i = 1,8
        do j = 1,8
        Kemat(i,j) = Kemat(i,j) + mat1(i,j)
        enddo
    enddo

    mat1 = 0.0
    call help_makeKe(poisson,young,x,-x,time,numbers,SoM,Noelem,Nonode,co_node,mat1)
    do i = 1,8
        do j = 1,8
            Kemat(i,j) = Kemat(i,j) + mat1(i,j)
        enddo
    enddo

    mat1 = 0.0
    call help_makeKe(poisson,young,x,x,time,numbers,SoM,Noelem,Nonode,co_node,mat1)
        do i = 1,8
            do j = 1,8
                Kemat(i,j) = Kemat(i,j) + mat1(i,j)
            enddo
    enddo

    mat1 = 0.0
    call help_makeKe(poisson,young,-x,x,time,numbers,SoM,Noelem,Nonode,co_node,mat1)
    do i = 1,8
        do j = 1,8
            Kemat(i,j) = Kemat(i,j) + mat1(i,j)
        enddo
    enddo

    ! do i = 1,8
    ! write(*,*)"mat1(",i",8)=",mat1(i,8)
    ! enddo
    ! write(*,*)"Kemat="
    ! write(*,*)Kemat

    do i = 1,4
        do j =1,4
            k = numbers(time,i); l = numbers(time,j) !>k,lは注目している要素の節点番号を表す
            ! write(*,*)"k,l=",k,l
            Kmat(2*k-1,2*l-1) = Kmat(2*k-1,2*l-1) + Kemat(2*i-1,2*j-1)
            Kmat(2*k-1,2*l) = Kmat(2*k-1,2*l) + Kemat(2*i-1,2*j)
            Kmat(2*k,2*l-1) = Kmat(2*k,2*l-1) + Kemat(2*i,2*j-1)
            Kmat(2*k,2*l) = Kmat(2*k,2*l) + Kemat(2*i,2*j) 

        enddo
    enddo
    ! write(*,*)"Kmat=",Kmat

end subroutine makeK

subroutine product(mat,vec,SoM,outcome) !>product(mat,vec,SoM,outcome)
    implicit none
    
        integer :: i,j
    
        integer,intent(in) :: SoM
        real(8),intent(in) :: mat(SoM,SoM),vec(SoM)
    
        real(8) :: outcome(SoM)
    
        do i = 1, SoM
            outcome(i) = 0.0 ! ベクトルyの要素を初期化
            do j = 1, SoM
                outcome(i) = outcome(i) + mat(i,j) * vec(j)
            end do
        end do
end subroutine product



program main
    implicit none
 !変数の定義
    ! 使い捨て変数
    integer :: a, b, i, j, k, n, time ! 繰り返しなどに使う整数
    real(8) :: x, y, z

    ! メッシュ定義に必要な変数
    integer :: divix, diviy, Npelem
    real(8) :: lengx, lengy

    ! 支配方程式導出に必要な変数
    integer :: SoM
    real(8), allocatable :: co_x(:), co_y(:), displacement(:), Force(:)

    ! 線形ソルバに使う変数
    real(8), allocatable :: bvec(:), Amat(:,:), xvec(:), rvec(:), r0(:), rsavevec(:), pvec(:), alpha(:), beta(:), pro(:) 

    ! ファイル出力に必要な変数
    integer :: numVTKfile, Nonode, Noelem
    integer, allocatable :: numbers(:,:)
    real(8), allocatable :: co_node(:,:)

    character(256) :: filename

    ! 定数の定義に使う変数
    real(8) :: young, poisson, eps(3), Kemat(8,8) ! ヤング率、ポアソン比、行列、ベクトル
    real(8), allocatable :: Kmat(:,:)

    ! その他の変数
    integer :: numinput, numnode, numelem, numbc, numload, Nodim, Nobc, Noload
    
    !> ファイル番号
    numinput = 10; numnode = 11; numelem = 12; numbc = 13; numload = 14; numVTKfile = 999

 !メッシュ生成
    !>正方形メッシュのx,y方向長さとx,y方向分割数を入力する
    write(*,*)"x方向長さ y方向長さ x方向分割数 y方向分割数 を入力"
    read *,lengx,lengy,divix,diviy

    Nonode = (divix+1)*(diviy+1)
    Noelem =     divix*diviy


    allocate(co_x(divix+1),co_y(diviy+1))
    
    !>ここから各節点の座標を求める(もっと簡単に書き直せるかも)
    do i = 1,divix+1
        co_x(i) = 0 + (lengx/divix) * (i-1)!>左からi列目の節点のx座標を記録
    enddo

    do i = 1, diviy+1
        co_y(i) = 0 + (lengy/diviy) * (i-1)!>下からi行目の節点のy座標を記録
    enddo
    allocate(co_node(Nonode,3))

    do j = 1, diviy+1
        do i = 1,divix+1
                co_node(i+(divix+1)*(j-1),2) = co_y(j)
                co_node(i+(divix+1)*(j-1),1) = co_x(i)
        enddo
    enddo

    !>各要素の節点座標を記録する
    allocate(numbers(Noelem,4))

    do j = 1, diviy
        do i = 1,divix        

            ! write(*,*)i+(divix)*(j-1)
    numbers(i+(divix)*(j-1),1) = i+(divix+1)*(j-1)
    numbers(i+(divix)*(j-1),2) = numbers(i+(divix)*(j-1),1) + 1
    numbers(i+(divix)*(j-1),3) = i+(divix+1)*(j)  + 1
    numbers(i+(divix)*(j-1),4) = numbers(i+(divix)*(j-1),3) - 1
        enddo
    enddo

        !numbersが適切かチェックするのに使う
        ! do i = 1,Noelem
        ! write(*,*)i,numbers(i,1), numbers(i,2), numbers(i,3), numbers(i,4)
        ! enddo

    Npelem = 4 !>各要素は正方形のため要素ごとの節点は4つ

 !ファイル読み取り

    open(numinput,file="input.dat",status="old") 
        read(numinput,*)young
        read(numinput,*)poisson
    close(numinput)

    open(numbc,file="bc.dat",status="old")
        read(numbc,*) Nobc,Nodim
        SoM = Nonode*Nodim
        ! write(*,*)"SoM=",SoM
        allocate(displacement(SoM)) !> 節点数×最大自由度の分だけ解の要素数を作る
        allocate(Force(SoM)) !>節点数×最大自由度の分だけ外力ベクトルの要素数を作る
        displacement = 0.0 !> 解の初期化
        Force = 0.0 !> 外力ベクトルの初期化

        do i = 1,Nobc
            read(numbc,*) a,b,x
            displacement((a-1)*Nodim + b) = x !>境界条件を変位ベクトルに代入
        enddo
    close(numbc)

    open(numload,file="load.dat",status="old")
        read(numload,*)Noload,Nodim

        do i=1,Noload
            read(numload,*) a,b,x
            Force((a-1)*Nodim+b) = x
        enddo
    close(numload)

 !マトリックス導出
    allocate(Kmat(SoM,SoM))
    Kmat = 0.0


    do time = 1,Noelem
    ! write(*,*)poisson,young,time,numbers,SoM,Noelem,Nonode,co_node,Kmat

    call makeK(poisson,young,time,numbers,SoM,Noelem,Nonode,co_node,Kmat)
    enddo
    ! write(*,*)"Kmat=",Kmat

 !>CG法を用いた求解
    allocate(bvec(SoM),Amat(SoM,SoM),xvec(SoM),rvec(SoM),r0(SoM))!>長すぎるため改行
    allocate(rsavevec(SoM),pvec(SoM),alpha(SoM),beta(SoM),pro(SoM))

    xvec = 1.0; rvec = 0.0; rsavevec = 0.0; pvec = 0.0 !>各ベクトルの初期化
    Amat = Kmat!> A,bをソルバに入力(bはノイマン境界条件)
    bvec = Force

    open(numbc,file="bc.dat",status="old")!>境界条件の読み取り(右辺が0でない場合にも対応+ファイル読み取りと結合の必要あり)
        read(numbc,*) Nobc,Nodim

        do i = 1,Nobc !>ディリクレ境界条件代入
            read(numbc,*) a,b,x

            n = (a-1)*Nodim + b
            do j = 1,SoM
                Amat(n,j) = 0
                Amat(j,n) = 0
            enddo
            Amat(n,n) = 1
            
            xvec(n) = x !>境界条件を解ベクトルに代入している
            ! write(*,*)Nobc
        enddo

    close(numbc)

    call product(Amat,xvec,SoM,pro)
    rvec = bvec - pro !>r0の定義
    pvec = rvec !> p0の定義
    r0 = rvec !> r0の値は繰り返し判定に使うため保存しておく

    do i = 1, 1000
        call product(Amat,pvec,SoM,pro) !>Amatとp(i-1)の積を求める

        x = dot_product(rvec,rvec) !> alphaの分子
        y = dot_product(pvec,pro) !> alphaの分母
        alpha = x/y !>alpha(i)の計算
        
        xvec = xvec + alpha * pvec !>x(i)の計算
        rsavevec = rvec !>r(i-1)を保存
        rvec = rvec - alpha*pro !>r(i)を計算
        
        x = sqrt(dot_product(rvec,rvec))
        y = sqrt(dot_product(r0,r0))
        z = (dot_product(rsavevec,rsavevec))
        if((x/y) <= 1.0E-8)then !>繰り返し判定
            exit
        endif

        beta = x*x/z
        pvec = rvec + beta*pvec
        if(i == 1000)then
            write(*,*)"error"
            stop
        endif
    enddo
    displacement = xvec

    ! write(*,*)Force
    do i =1,Nonode
    write(*,*)displacement(Nodim*i-1),displacement(Nodim*i) !>３次元未対応に注意
    enddo
    ! do i = 1,SoM
    ! write(*,*)Amat(i,1),Amat(i,2),Amat(i,3),Amat(i,4),Amat(i,5),Amat(i,6),Amat(i,7),Amat(i,8)
    ! enddo

 !>指定された条件を満たす形状を表すvtkファイルを出力する

    numVTKfile = 999
    filename = 'mesher_outcome.vtk' !>ここでファイルの名前と拡張子を決めている

    open(unit = numVTKfile, file = filename, status = 'replace')
    write(numVTKfile, '(a)') '# vtk DataFile Version 4.1'
    write(numVTKfile, '(a)') 'Thisis a comment line'
    write(numVTKfile, '(a)') 'ASCII'
    write(numVTKfile, '(a)') 'DATASET UNSTRUCTURED_GRID'

  
    write(numVTKfile, '(a,i0,a)') 'POINTS ',Nonode,' float'
    do i = 1,Nonode
        !> 各節点の座標を行ごとに代入する
        write(numVTKfile, '(G0,a,G0,a,G0)') dble(co_node(i,1)),' ',  dble(co_node(i,2)),' ',  dble(co_node(i,3))
    enddo

    write(numVTKfile, '(a,i0,a,i0)') 'CELLS ', Noelem,' ', (1 + Npelem)*Noelem 
    do i = 1,Noelem
        !> 各要素の構成番号を要素ごとに入力する
        write(numVTKfile,'(a,I0,a,I0,a,I0,a,I0)') '4 ',numbers(i,1)-1,' ', numbers(i,2)-1,' ', numbers(i,3)-1,' ', numbers(i,4)-1
    enddo

    write(numVTKfile, '(a,I0)') 'CELL_TYPES ' , Noelem
    do i = 1, Noelem
        write(numVTKfile,'(I0)') 9
    enddo

    write(numVTKfile, '(a,I0)') 'POINT_DATA ',Nonode
    write(numVTKfile, '(a)')'VECTORS Displacement_Vector_Data float'
    do i = 1, Nonode
        !>線形ソルバで求めた変位を入力する,z方向は0で入力
        write(numVTKfile,'(G0,a,G0,a)') displacement(2*i-1),' ',displacement(2*i),' 0.0'
    enddo

    close(numVTKfile)



end program