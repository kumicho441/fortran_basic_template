subroutine product(mat,vec,MatSize,outcome) !>product(mat,vec,MatSize,outcome)
implicit none

    integer :: i,j

    integer,intent(in) :: MatSize
    real(8),intent(in) :: mat(MatSize,MatSize),vec(MatSize)

    real(8) :: outcome(MatSize)

    do i = 1, MatSize
        outcome(i) = 0.0 ! ベクトルyの要素を初期化
        do j = 1, MatSize
            outcome(i) = outcome(i) + mat(i,j) * vec(j)
        end do
    end do
end subroutine product


program main
    implicit none
    integer :: i,j
    real(8) :: x,y,z

    integer :: numelem,numbc,Nonode,Nodim,MatSize
    real(8),allocatable :: bvec(:),Amat(:,:),xvec(:),rvec(:),r0(:),rsavevec(:),pvec(:),alpha(:),beta(:),pro(:) !>rvec(添え字)


    !>ファイル読み取り テストの値定義などメインプログラムで省略OK
    integer :: values1(32),values2(32)

    numelem = 11
    numbc = 12
    open(numelem,file="elem.dat",status="old")
    Nonode = 0
        read(numelem,*) i,Nonode 

    close(numelem)

    open(numbc,file="bc.dat",status="old")
    Nodim = 0
     read(numbc,*) i,Nodim
     MatSize = Nonode*Nodim
    close(numbc)

    !>行列の定義を行う　以降メインプログラムで省略できない

    allocate(bvec(MatSize),Amat(MatSize,MatSize),xvec(MatSize),rvec(MatSize),r0(MatSize))!>長すぎるため改行
    allocate(rsavevec(MatSize),pvec(MatSize),alpha(MatSize),beta(MatSize),pro(MatSize))

    !> 行列にテストで値を代入する。テストの解は[1,9,8,5,2,0,2,3]である
    values1 = [10, 3, 4, 6, 2, 5, 7, 1,3, 8, 6, 1, 5, 2, 4, 9,4, 6, 12, 5, 3, 7, 8, 2,6, 1, 5, 15, 4, 3, 9, 6]
    values2 = [2, 5, 3, 4, 11, 6, 2, 5,5, 2, 7, 3, 6, 13, 5, 3,7, 4, 8, 9, 2, 5, 17, 4,1, 9, 2, 6, 5, 3, 4, 14]
    do i = 1, MatSize
        do j = 1, MatSize
            if(i <= MatSize/2)then
                Amat(i, j) = values1((i-1)*MatSize + j)
            else
                Amat(i, j) = values2((i-5)*MatSize + j)
            endif
        end do
    end do



    bvec = [120,173,207,174,132,125,202,188]

    !> Kmat,Forceを代入している
    ! Amat = Kmat
    ! bvec = Force

    xvec = 0.0 !>各ベクトルの初期化
    rvec = 0.0
    rsavevec = 0.0
    pvec = 0.0
    

    ! read(*, *) xvec !>初期の解ベクトルx0を入力する

    ! do i = 1, MatSize
    !     do j = 1, MatSize
    !         write(*,*)Amat(i,j)
    !     end do
    ! end do



    call product(Amat,xvec,MatSize,pro)
    rvec = bvec - pro !>r0の定義
    pvec = rvec !> p0の定義
    r0 = rvec !> r0の値は繰り返し判定に使うため保存しておく

    do i = 1, MatSize
        call product(Amat,pvec,MatSize,pro) !>Amatとp(i-1)の積を求める

        x = dot_product(rvec,rvec) !> alphaの分子
        y = dot_product(pvec,pro) !> alphaの分母
        alpha = x/y !>alpha(i)の計算
        
        xvec = xvec + alpha * pvec !>x(i)の計算
        rsavevec = rvec !>r(i-1)を保存
        rvec = rvec - alpha*pro !>r(i)を計算
        
        x = dot_product(rvec,rvec)
        y = dot_product(r0,r0)
        z = dot_product(rsavevec,rsavevec)
        if(((x*x)/(y*y)) <= 1.0E-016)then !>繰り返し判定
            exit
        endif

        beta = x/z
        pvec = rvec + beta*pvec

    enddo

    do i = 1, MatSize
    write(*,*)xvec(i)
    enddo

end program main