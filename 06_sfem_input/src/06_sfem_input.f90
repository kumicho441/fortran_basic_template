program main
    use dbg_controller
    use variables
    use initialize
    use mesh_and_vis
    use makeK
    use connectLG
    use bc_and_solve
    use addinguLuG
    use eval_L2
    use makeVTK
    ! use makeK_multi

    implicit none
    ! write(*, *) young

    real(8) :: x,y ,x_co,y_co
    integer(4) :: Gelemid
    call dbg()
    call initialize_data()!初期化
    call meshvis()!メッシュのみの生成・可視化
    call mod05_completeK(smd,Gdata,Ldata,divi,CGd)!Kmatの生成を行う
    call integrate_BC(divi,CGd,Gdata)!境界条件の挿入を行う
    call solver(CGd)!CG法で問題を解く
    call makeu_cal(Gdata,Ldata,divi,CGd)!u_Gをu_Lに足しこみ数値解を求める
    call makeL2_full(smd,Ldata,Gdata,CGd,evd)
    call vis_outcome(Gdata,Ldata,CGd,evd)!変位の可視化を行う

    

    write(*,*)'G要素数, ','L要素数, ','L2誤差, ','積分点数, ','反復回数'
    ! write(*,*)Gdata%Nelem,',',Ldata%Nelem,',',evd%L2,',',smd%Nip_subd**2,',',iii
    write(*,*)Gdata%Nelem,',',Ldata%Nelem,',',evd%L2,',',smd%n_rec**2 * 4,',',iii



    ! do iii = 1, Ldata%Nnode
    !     x_co = Ldata%co_node(1,iii)
    !     y_co = Ldata%co_node(2,iii)

    !     call mod04_get_Gmeshid(x_co,y_co,divi,Gelemid)
    !     write(*,*)iii,Gelemid
    ! enddo

    ! call get_co(400,0.0d0,-0.0d0,x,y)
    ! write(*,*)x,y

    ! do int = 1,Ldata%Nelem
    !     call get_BonG(Kdata,Gdata,divi,int,-1.0d0,-1.0d0)
    ! enddo

    ! call makeKe_multi(Kdata,Gdata,divi,0.0d0,0.0d0,25)

    ! write(*,*)Ldata%Nelem
    ! call makeK_multi_full(Kdata,Gdata,Ldata,divi)

    ! do int = 1,10
    ! write(*,*)Ldata%co_node(1,Ldata%Nnode+1 -int),Ldata%co_node(2,Ldata%Nnode+1 -int)
    ! write(*,*)Ldata%co_node(1,121+11-int),Ldata%co_node(2, 121+11-int)
    ! enddo

    ! do int = 1,10
    !     x_co = Ldata%co_node(1,Ldata%Nnode+1 -int)
    !     y_co = Ldata%co_node(2,Ldata%Nnode+1 -int)
    !     call get_Gmeshid(x_co,y_co,divi,Gelemid)
    !     write(*,*)Gelemid

    !     x_co = Ldata%co_node(1,121+11-int)
    !     y_co = Ldata%co_node(2, 121+11-int)
    !     call get_Gmeshid(x_co,y_co,divi,Gelemid)
    !     write(*,*)Gelemid
    !     write(*,*)' '
    ! enddo
    
end program main