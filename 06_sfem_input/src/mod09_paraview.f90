module makeVTK
    use dbg_controller
    use variables
    use mesh_and_vis
    implicit none
    contains

    subroutine vis_outcome(Mdata_G,Mdata_L,CGdata,L2data)
        implicit none
        type(meshdata) :: Mdata_G,Mdata_L
        type(CGmethoddata) :: CGdata
        type(evaluationdata) :: L2data

        integer(4) :: i

        if(debug_option)then
            write(*,*)"subroutine vis_outcome start"
        endif

        open(unit = numVTK, file = '06_sfem_outcome.vtk', status = 'old', position = 'append')

            write(numVTK, '(a,I0)') 'POINT_DATA ',Mdata_G%Nnode + Mdata_L%Nnode
            write(numVTK, '(a)')'VECTORS d_cal_Vector_Data float'

            !G領域変位
            do i = 1, Mdata_G%Nnode
                write(numVTK,'(G0,a,G0,a)') CGdata%uvec(2*i-1),' ',CGdata%uvec(2*i),' 0.0'
            enddo

            !L領域変位
            do i = Mdata_G%Nnode + 1, Mdata_G%Nnode + Mdata_L%Nnode
                write(numVTK,'(G0,a,G0,a)') CGdata%uvec(2*i-1),' ',CGdata%uvec(2*i),' 0.0'
            enddo

            !理論解入力
            write(numVTK, '(a)')'VECTORS d_th_Vector_Data float'
            do i = 1, Mdata_L%Nnode + Mdata_G%Nnode
                !>理論解から求めた変位を入力する
                write(numVTK,'(G0,a,G0,a)')L2data%uvec_th(2*i-1),' ',L2data%uvec_th(2*i),' 0.0'
            enddo

            !各点におけるL2誤差
            write(numVTK, '(a,I0)') 'CELL_DATA ',Mdata_G%Nelem + Mdata_L%Nelem
            write(numVTK, '(a)')'SCALARS L2_Data float'
            write(numVTK, '(a)')'LOOKUP_TABLE default'
            do i = 1, Mdata_G%Nelem
                write(numVTK,'(G0)') 0.0d0
            enddo

            do i = 1, Mdata_L%Nelem
                !>各点におけるL2誤差
                write(numVTK,'(G0)') L2data%elel2(i)
            enddo

            ! do i = 1, Mdata_G%Nelem
            !     write(numVTK,'(G0)') L2data%elel2(i)
            ! enddo

            ! do i = 1, Mdata_L%Nelem
            !     !>各点におけるL2誤差
            !     write(numVTK,'(G0)') 0.0d0
            ! enddo

        close(numVTK)

        if(debug_option)then
            write(*,*)"Finish"
        endif
    end subroutine vis_outcome

end module makeVTK