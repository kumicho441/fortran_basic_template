module mesh_and_vis
    use dbg_controller
    use variables
    ! use initialize
    implicit none

    contains

    subroutine mesher()!InputFile内のファイルをもとにメッシュ分割を行う
        implicit none
        integer(4) :: a,  d,i,j
        integer(4) :: Gid(4)!L要素の細分化判定に用いる
        real(8) :: x,z,r,s,dx,dy
        real(8) :: radius,x_co1,y_co1,x_co2,y_co2,x_co3,y_co3,x_co4,y_co4,r1,r2

        if(debug_option)then
            write(*,*)"Subroutine mesher start"
        endif
  
        open(numG, file="Gmesh.dat", status="old")
            ! read(numG, *) x, a  !{グローバル領域の長さ, 各辺の分割数}
            read(numG, *) divi%leng_Garea, divi%diviG  !{グローバル領域の長さ, 各辺の分割数}
        close(numG)

        divi%leng_Gmesh = (divi%leng_Garea/divi%diviG)
        ! write(*,*)divi%leng_Gmesh

        !G領域節点座標
        do i = 1, divi%diviG + 1
            do j = 1, divi%diviG + 1
                Gdata%co_node(1,(divi%diviG+1)*(i-1)+ j ) = (j-1)*divi%leng_Gmesh
                Gdata%co_node(2,(divi%diviG+1)*(i-1)+ j ) = (i-1)*divi%leng_Gmesh
            enddo
        enddo

        !G領域節点id
        do j = 1, divi%diviG
            do i = 1,divi%diviG
                Gdata%elem_node_id(1,i+ divi%diviG * (j-1)) = i+(divi%diviG+1)*(j-1)
                Gdata%elem_node_id(2,i+ divi%diviG * (j-1)) = Gdata%elem_node_id(1,i+divi%diviG * (j-1)) + 1
                Gdata%elem_node_id(3,i+ divi%diviG * (j-1)) = i+(divi%diviG+1)*(j)  + 1
                Gdata%elem_node_id(4,i+ divi%diviG * (j-1)) = Gdata%elem_node_id(3,i+ divi%diviG * (j-1)) - 1
            enddo
        enddo

        ! do j = 1, a!G領域要素と節点の対応デバッグ
        !     do i = 1,a
        !         write(*,*)Gdata%elem_node_id(1,i+ a * (j-1)),&
        !         Gdata%elem_node_id(2,i+ a * (j-1)) ,&
        !         Gdata%elem_node_id(3,i+ a * (j-1)) ,&
        !         Gdata%elem_node_id(4,i+ a * (j-1)) 
        !     enddo
        ! enddo

        ! do i = 1,Gdata%Nnode!Gメッシュの座標確認
        ! write(*,*)Gdata%co_node(1,i),Gdata%co_node(2,i)
        ! enddo


        !Lメッシュ定義
        r = divi%leng_Garea*area_ratio !{ローカル領域の半径}
        s = r/2 !{ローカル領域の正方形部分の長さ}

        open(numL, file="Lmesh.dat", status="old")
            read(numL, *) divi%diviL_q,divi%diviL_c! {正方形各辺の分割数,r方向分割数}
        close(numL)


        !L正方形部分節点座標
        z = (s/divi%diviL_q)
        do i = 1,divi%diviL_q + 1
            do j = 1, divi%diviL_q + 1
                Ldata%co_node(1,(divi%diviL_q+1)*(i-1)+ j ) = (j-1)*z
                Ldata%co_node(2,(divi%diviL_q+1)*(i-1)+ j ) = (i-1)*z
            enddo
        enddo

        !円孔部節点座標定義
        a = (divi%diviL_q+1)*(divi%diviL_q+1)
        do i = 1, divi%diviL_q+1

            x = (pi/2)/(2*divi%diviL_q)*(i-1)

            dx = (r*cos(x))
            dy = (r*sin(x))
            
            dx = (dx - Ldata%co_node(1,i*(divi%diviL_q+1)))/divi%diviL_c
            dy = (dy - Ldata%co_node(2,i*(divi%diviL_q+1)))/divi%diviL_c

            do j = 1, divi%diviL_c
                Ldata%co_node(1,a + (i-1)*divi%diviL_c + j) = Ldata%co_node(1,i*(divi%diviL_q+1)) + dx*j
                Ldata%co_node(2,a + (i-1)*divi%diviL_c + j) = Ldata%co_node(2,i*(divi%diviL_q+1)) + dy*j
                ! if(Ldata%co_node(1,a + (i-1)*divi%diviL_c + j) < 0.0d0)then
                !     Ldata%co_node(1,a + (i-1)*divi%diviL_c + j) = 0.0d0
                ! endif

                if(i /= divi%diviL_q+1)then
                Ldata%co_node(1,Ldata%Nnode - (i*divi%diviL_c)+j )= Ldata%co_node(2,a + (i-1)*divi%diviL_c + j)
                Ldata%co_node(2,Ldata%Nnode - (i*divi%diviL_c)+j )= Ldata%co_node(1,a + (i-1)*divi%diviL_c + j)
                endif
            enddo
        enddo

        do i = 1,Ldata%Nnode!メッシュ内包関係バグ予防
            if(Ldata%co_node(1,i) < 0.0d0)then
                Ldata%co_node(1,i) = 0.0d0
            endif

            if(Ldata%co_node(2,i) < 0.0d0)then
                Ldata%co_node(2,i) = 0.0d0
            endif
        enddo



        !Lメッシュid定義
        do j = 1, divi%diviL_q
            do i = 1,divi%diviL_q
                Ldata%elem_node_id(1,i+ divi%diviL_q * (j-1)) = i+(divi%diviL_q+1)*(j-1)
                Ldata%elem_node_id(2,i+ divi%diviL_q * (j-1)) = Ldata%elem_node_id(1,i + divi%diviL_q * (j-1)) + 1
                Ldata%elem_node_id(3,i+ divi%diviL_q * (j-1)) = i+(divi%diviL_q+1)*(j)  + 1
                Ldata%elem_node_id(4,i+ divi%diviL_q * (j-1)) = Ldata%elem_node_id(3,i + divi%diviL_q * (j-1)) - 1
            enddo
        enddo

        d = divi%diviL_q*divi%diviL_q
        do i = 1, divi%diviL_q
            do j = 1,divi%diviL_c

                if(j == 1)then
                Ldata%elem_node_id(1,d + (i-1)*divi%diviL_c + j) = (divi%diviL_q+1)*i 
                Ldata%elem_node_id(2,d + (i-1)*divi%diviL_c + j) = a + divi%diviL_c*(i-1) + j
                Ldata%elem_node_id(3,d + (i-1)*divi%diviL_c + j) = a + divi%diviL_c*i + j
                Ldata%elem_node_id(4,d + (i-1)*divi%diviL_c + j) = (divi%diviL_q+1)*(i+1)
                else

                Ldata%elem_node_id(1,d + (i-1)*divi%diviL_c + j) = a + divi%diviL_c*(i-1) + j -1
                Ldata%elem_node_id(2,d + (i-1)*divi%diviL_c + j) = a + divi%diviL_c*(i-1) + j + 1 -1
                Ldata%elem_node_id(3,d + (i-1)*divi%diviL_c + j) = a + divi%diviL_c*i + j + 1 -1
                Ldata%elem_node_id(4,d + (i-1)*divi%diviL_c + j) = a + divi%diviL_c*i + j -1
                endif

            enddo
        enddo

        do i = divi%diviL_q+1,2*divi%diviL_q
            do j = 1,divi%diviL_c

                if(j == 1)then
                ! Ldata%elem_node_id(1,d + (i-1)*divi%diviL_c + j) = a - (i - (divi%diviL_q+1))
                Ldata%elem_node_id(1,d + (i-1)*divi%diviL_c + j) = a - (i - (divi%diviL_q+1))
                Ldata%elem_node_id(2,d + (i-1)*divi%diviL_c + j) = a + divi%diviL_c*(i-1) + j
                Ldata%elem_node_id(3,d + (i-1)*divi%diviL_c + j) = a + divi%diviL_c*i + j
                Ldata%elem_node_id(4,d + (i-1)*divi%diviL_c + j) = a - (i - (divi%diviL_q+1)) - 1
                else

                Ldata%elem_node_id(1,d + (i-1)*divi%diviL_c + j) = a + divi%diviL_c*(i-1) + j -1
                Ldata%elem_node_id(2,d + (i-1)*divi%diviL_c + j) = a + divi%diviL_c*(i-1) + j + 1 -1
                Ldata%elem_node_id(3,d + (i-1)*divi%diviL_c + j) = a + divi%diviL_c*i + j + 1 -1
                Ldata%elem_node_id(4,d + (i-1)*divi%diviL_c + j) = a + divi%diviL_c*i + j -1
                endif

            enddo
        enddo

        ! !デバッグ用Lメッシュ定義
        ! open(numG, file="Gmesh.dat", status="old")
        !     read(numG, *) divi%leng_Larea, divi%diviL  !{ローカル領域の長さ, 各辺の分割数}
        ! close(numG)

        ! divi%leng_Lmesh = (divi%leng_Larea/divi%diviL)
        ! ! write(*,*)divi%leng_Lmesh

        ! do i = 1, divi%diviL + 1
        !     do j = 1, divi%diviL + 1
        !         Ldata%co_node(1,(divi%diviL+1)*(i-1)+ j ) = (j-1)*divi%leng_Lmesh
        !         Ldata%co_node(2,(divi%diviL+1)*(i-1)+ j ) = (i-1)*divi%leng_Lmesh
        !     enddo
        ! enddo


        ! do j = 1, divi%diviL
        !     do i = 1,divi%diviL
        !         Ldata%elem_node_id(1,i+ divi%diviL * (j-1)) = i+(divi%diviL+1)*(j-1)
        !         Ldata%elem_node_id(2,i+ divi%diviL * (j-1)) = Ldata%elem_node_id(1,i+divi%diviL * (j-1)) + 1
        !         Ldata%elem_node_id(3,i+ divi%diviL * (j-1)) = i+(divi%diviL+1)*(j)  + 1
        !         Ldata%elem_node_id(4,i+ divi%diviL * (j-1)) = Ldata%elem_node_id(3,i+ divi%diviL * (j-1)) - 1
        !     enddo
        ! enddo

        if(debug_option)then
            write(*,*)"Finish"
        endif
    end subroutine mesher

    subroutine visualize()!メッシャー部分の可視化ファイル作成
        implicit none
        integer(4) :: a, i
        character(256) :: filename


        ! write(*,*)Gdata%Nnode, Ldata%Nnode
        if(debug_option)then
            write(*,*)"Subroutine visualize start"
        endif

            filename = '06_sfem_outcome.vtk'
            open(unit = numVTK, file = filename, status = 'replace')
                write(numVTK, '(a)') '# vtk DataFile Version 4.1'
                write(numVTK, '(a)') 'This is a comment line'
                write(numVTK, '(a)') 'ASCII'
                write(numVTK, '(a)') 'DATASET UNSTRUCTURED_GRID'
                write(numVTK, '(a,i0,a)') 'POINTS ',Gdata%Nnode + Ldata%Nnode,' float'

                do i = 1,Gdata%Nnode
                    !> G節点の座標を行ごとに代入する
                    write(numVTK, '(G0,a,G0,a,G0)') dble(Gdata%co_node(1,i)),&
                    ' ',  dble(Gdata%co_node(2,i)),' 0.0'
                enddo

                do i = 1,Ldata%Nnode
                    !> L節点の座標を行ごとに代入する
                    write(numVTK, '(G0,a,G0,a,G0)') dble(Ldata%co_node(1,i)),&
                    ' ',  dble(Ldata%co_node(2,i)),' 0.001'
                enddo

                write(numVTK, '(a,i0,a,i0)') 'CELLS ', Gdata%Nelem + Ldata%Nelem,' ', 5*(Gdata%Nelem + Ldata%Nelem)

                do i =1,Gdata%Nelem
                    write(numVTK,'(a,I0,a,I0,a,I0,a,I0)') '4 ', Gdata%elem_node_id(1,i)-1,' ',&
                     Gdata%elem_node_id(2,i)-1,' ',&
                     Gdata%elem_node_id(3,i)-1,' ',&
                     Gdata%elem_node_id(4,i)-1
                enddo

                a = Gdata%Nnode

                do i =1,Ldata%Nelem
                    write(numVTK,'(a,I0,a,I0,a,I0,a,I0)') '4 ', a + Ldata%elem_node_id(1,i)-1,' ',&
                     a + Ldata%elem_node_id(2,i)-1,' ',&
                     a + Ldata%elem_node_id(3,i)-1,' ',&
                     a + Ldata%elem_node_id(4,i)-1
                enddo

                write(numVTK, '(a,I0)') 'CELL_TYPES ' , Gdata%Nelem + Ldata%Nelem
                do i = 1, Gdata%Nelem + Ldata%Nelem
                    write(numVTK,'(I0)') 9
                enddo

            close(numVTK)

        if(debug_option)then
            write(*,*)"Finish"
        endif
    end subroutine visualize

    subroutine meshvis()
        implicit none
        call mesher()!メッシュ作成
        call visualize()!可視化ファイル作成
    end subroutine meshvis

end module mesh_and_vis