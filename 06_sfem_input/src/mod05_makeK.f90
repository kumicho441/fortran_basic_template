module makeK
    use dbg_controller
    use variables
    use connectLG
    implicit none


    contains

    !Bマトリックス生成(共通)
    subroutine makeB(Sdata,Mdata,et,gz,target)!et,gz,targetを入れると、対応する要素のBマトリックスを生成する
        implicit none
        type(Smatdata) :: Sdata
        type(meshdata) :: Mdata
        ! type(divisiondata) :: Ddata
        real(8),intent(in) :: et,gz
        integer(4),intent(in) :: target

        real(8) :: x,x_co,y_co
        integer(4) :: i,j

        !Dmat導出
        call mod04_get_co(Mdata,target,et,gz,x_co,y_co)
        ! call mod04_get_co(Mdata,target,-1.0d0,-1.0d0,x_co,y_co)

        if(sqrt(x_co**2 + y_co**2) <= divi%leng_Garea*area_ratio)then!積分点が繊維領域内にあるとき
            ! write(*,*)'inside',target

            ! Sdata%Dmat =0.0d0
            Sdata%Dmat(1,1) = 1/(1-poisson_1)
            Sdata%Dmat(2,2) = 1/(1-poisson_1)
            Sdata%Dmat(1,2) = poisson_1/(1-poisson_1)
            Sdata%Dmat(2,1) = poisson_1/(1-poisson_1)
            Sdata%Dmat(3,3) = 0.5
            x = young_1/(1+poisson_1)
            Sdata%Dmat = x * Sdata%Dmat

        else!積分点が繊維領域外にあるとき

            ! Sdata%Dmat =0.0d0
            Sdata%Dmat(1,1) = 1/(1-poisson_2)
            Sdata%Dmat(2,2) = 1/(1-poisson_2)
            Sdata%Dmat(1,2) = poisson_2/(1-poisson_2)
            Sdata%Dmat(2,1) = poisson_2/(1-poisson_2)
            Sdata%Dmat(3,3) = 0.5
            x = young_2/(1+poisson_2)
            Sdata%Dmat = x * Sdata%Dmat
        endif

        !Jmat導出
        Sdata%mat24(1,1) = -(1-et)/4
        Sdata%mat24(1,2) =  (1-et)/4
        Sdata%mat24(1,3) =  (1+et)/4
        Sdata%mat24(1,4) = -(1+et)/4
        Sdata%mat24(2,1) = -(1-gz)/4
        Sdata%mat24(2,2) = -(1+gz)/4
        Sdata%mat24(2,3) =  (1+gz)/4
        Sdata%mat24(2,4) =  (1-gz)/4
        
        do i = 1,4!対象とする要素に対応する節点座標を記録
            Sdata%mat42_m(i,1) = Mdata%co_node(1,Mdata%elem_node_id(i,target))
            Sdata%mat42_m(i,2) = Mdata%co_node(2,Mdata%elem_node_id(i,target))
        enddo

        Sdata%Jmat = matmul(Sdata%mat24,Sdata%mat42_m)


        !detJ
        ! Sdata%detJ = 0.0d0
        Sdata%detJ = Sdata%Jmat(1,1)*Sdata%Jmat(2,2) - Sdata%Jmat(1,2)*Sdata%Jmat(2,1)

        !Jinv
        ! Sdata%Jinv = 0.0d0
        Sdata%Jinv(1,1) =  Sdata%Jmat(2,2)/Sdata%detJ
        Sdata%Jinv(1,2) = -Sdata%Jmat(1,2)/Sdata%detJ
        Sdata%Jinv(2,1) = -Sdata%Jmat(2,1)/Sdata%detJ
        Sdata%Jinv(2,2) =  Sdata%Jmat(1,1)/Sdata%detJ

        !Bmat修正版
        ! Sdata%mat24_m = 0.0d0

        Sdata%mat24_m(1,1) = -(1-et)/4
        Sdata%mat24_m(2,1) = -(1-gz)/4
        Sdata%mat24_m(1,2) = (1-et)/4
        Sdata%mat24_m(2,2) = -(1+gz)/4
        Sdata%mat24_m(1,3) = (1+et)/4
        Sdata%mat24_m(2,3) = (1+gz)/4
        Sdata%mat24_m(1,4) = -(1+et)/4
        Sdata%mat24_m(2,4) = (1-gz)/4

        Sdata%mat24_u = 0.0d0
        Sdata%mat24_u = matmul(Sdata%Jinv,Sdata%mat24_m)

        Sdata%Bmat = 0.0d0
        do i = 1,4
            Sdata%Bmat(1,(2*i-1)) = Sdata%mat24_u(1,i)
            Sdata%Bmat(3,2*i)     = Sdata%mat24_u(1,i)
            Sdata%Bmat(2,2*i)     = Sdata%mat24_u(2,i)
            Sdata%Bmat(3,(2*i-1)) = Sdata%mat24_u(2,i)
        enddo

        !TraB
        do i = 1,3
            do j = 1,8
                Sdata%BTmat(j,i) = Sdata%Bmat(i,j)
            enddo
        enddo

        ! if(debug_option)then
        !     write(*,*)"finish"
        ! endif
    end subroutine makeB


    ! subroutine makeKe_mono(Sdata,Mdata,et,gz,target)!G領域、L領域の要素剛性行列生成
    !     implicit none
    !     type(Smatdata) :: Sdata
    !     type(meshdata) :: Mdata
    !     real(8),intent(in) :: et,gz
    !     integer(4),intent(in) :: target

    !     call makeB(Sdata,Mdata,et,gz,target)

    !     Sdata%mat88 = 0.0d0!Keを作るための情報はmat88に保存している
    !     Sdata%mat38 = 0.0d0

    !     Sdata%mat38 = matmul(Sdata%Dmat,Sdata%Bmat)
    !     Sdata%mat88 = matmul(Sdata%BTmat,Sdata%mat38)* Sdata%detJ

    ! end subroutine makeKe_mono



    ! subroutine makeKe_mono_subd(Sdata,Mdata,int1,int2,target)!G領域、L領域の要素剛性行列生成
    !     implicit none
    !     type(Smatdata) :: Sdata
    !     type(meshdata) :: Mdata
        

    !     integer(4),intent(in) :: target
    !     real(8) :: et,gz
    !     integer(4) :: int1,int2
    !     et = Sdata%co_ip_subd(int1)
    !     gz = Sdata%co_ip_subd(int2)

    !     call makeB(Sdata,Mdata,et,gz,target)

    !     Sdata%mat88 = 0.0d0!Keを作るための情報はmat88に保存している
    !     Sdata%mat38 = 0.0d0

    !     Sdata%mat38 = matmul(Sdata%Dmat,Sdata%Bmat)
    !     Sdata%mat88 = matmul(Sdata%BTmat,Sdata%mat38)* Sdata%detJ

    !     Sdata%mat88 = Sdata%mat88 * Sdata%wip_subd(int1)*Sdata%wip_subd(int2)
    ! end subroutine makeKe_mono_subd

    subroutine makeKe_mono_subd(Sdata,Mdata,int1,target)!G領域、L領域の要素剛性行列生成
        implicit none
        type(Smatdata) :: Sdata
        type(meshdata) :: Mdata

        integer(4),intent(in) :: target
        real(8) :: et,gz
        integer(4) :: int1
        et = Sdata%co_ip_subd(1,int1)
        gz = Sdata%co_ip_subd(2,int1)

        call makeB(Sdata,Mdata,et,gz,target)

        Sdata%mat88 = 0.0d0!Keを作るための情報はmat88に保存している
        Sdata%mat38 = 0.0d0

        Sdata%mat38 = matmul(Sdata%Dmat,Sdata%Bmat)
        Sdata%mat88 = matmul(Sdata%BTmat,Sdata%mat38)* Sdata%detJ

        Sdata%mat88 = Sdata%mat88 * Sdata%wip_subd
    end subroutine makeKe_mono_subd

    ! subroutine makeKe_mono(Sdata,Mdata,int1,int2,target)!G領域、L領域の要素剛性行列生成
    !     implicit none
    !     type(Smatdata) :: Sdata
    !     type(meshdata) :: Mdata
        

    !     integer(4),intent(in) :: target
    !     real(8) :: et,gz
    !     integer(4) :: int1,int2
    !     et = Sdata%co_ip(int1)
    !     gz = Sdata%co_ip(int2)

    !     call makeB(Sdata,Mdata,et,gz,target)

    !     Sdata%mat88 = 0.0d0!Keを作るための情報はmat88に保存している
    !     Sdata%mat38 = 0.0d0

    !     Sdata%mat38 = matmul(Sdata%Dmat,Sdata%Bmat)
    !     Sdata%mat88 = matmul(Sdata%BTmat,Sdata%mat38)* Sdata%detJ

    !     Sdata%mat88 = Sdata%mat88 * Sdata%wip(int1)*Sdata%wip(int2)
    ! end subroutine makeKe_mono

    subroutine makeKe_mono(Sdata,Mdata,i,target)!G領域、L領域の要素剛性行列生成
        implicit none
        type(Smatdata) :: Sdata
        type(meshdata) :: Mdata

        integer(4),intent(in) :: target
        real(8) :: et,gz
        integer(4) :: i
        et = Sdata%co_ip(1,i)
        gz = Sdata%co_ip(2,i)

        call makeB(Sdata,Mdata,et,gz,target)

        Sdata%mat88 = 0.0d0!Keを作るための情報はmat88に保存している
        Sdata%mat38 = 0.0d0

        Sdata%mat38 = matmul(Sdata%Dmat,Sdata%Bmat)
        Sdata%mat88 = matmul(Sdata%BTmat,Sdata%mat38)* Sdata%detJ

        Sdata%mat88 = Sdata%mat88 * Sdata%wip
    end subroutine makeKe_mono

    subroutine makeK_mono(Sdata,Mdata)!G領域、L領域の剛性行列生成
        implicit none
        type(Smatdata) :: Sdata
        type(meshdata) :: Mdata
        integer(4) :: target

        ! real(8) :: x
        integer(4) :: i,j,n,m

        do target = 1, Mdata%Nelem
            Sdata%Kemat = 0.0d0
            
            !積分領域の細分化
            if(Mdata%complex(target))then!要素が境界をまたぐ場合のガウス積分
                do i = 1, Sdata%Nip_subd
                        call makeKe_mono_subd(Sdata,Mdata,i,target)
                        Sdata%Kemat = Sdata%Kemat  + Sdata%mat88
                enddo
            else!またがない場合のガウス積分
                do i = 1, Sdata%Nip
                        call makeKe_mono(Sdata,Mdata,i,target)
                        Sdata%Kemat = Sdata%Kemat  + Sdata%mat88
                enddo
            endif


            ! do int= 1,8
            !     write(*,*) Sdata%Kemat(int,1), Sdata%Kemat(int,2), Sdata%Kemat(int,3), Sdata%Kemat(int,4), &
            !     Sdata%Kemat(int,5), Sdata%Kemat(int,6), Sdata%Kemat(int,7), Sdata%Kemat(int,8)
            ! enddo

            do i = 1,4
                n = Mdata%elem_node_id(i,target)
                do j = 1,4
                    m = Mdata%elem_node_id(j,target)

                    Mdata%Kmat(2*n-1,2*m-1) = Mdata%Kmat(2*n-1,2*m-1) + Sdata%Kemat(2*i-1,2*j-1)
                    Mdata%Kmat(2*n-1,2*m  ) = Mdata%Kmat(2*n-1,2*m  ) + Sdata%Kemat(2*i-1,2*j  )
                    Mdata%Kmat(2*n  ,2*m-1) = Mdata%Kmat(2*n  ,2*m-1) + Sdata%Kemat(2*i  ,2*j-1)
                    Mdata%Kmat(2*n  ,2*m  ) = Mdata%Kmat(2*n  ,2*m  ) + Sdata%Kemat(2*i  ,2*j  )
                enddo
            enddo
        enddo

    !     do i = 1, 8
    !         write(*,*) Sdata%Kemat(i,1), Sdata%Kemat(i,2), Sdata%Kemat(i,3), Sdata%Kemat(i,4), &
    !         Sdata%Kemat(i,5), Sdata%Kemat(i,6), Sdata%Kemat(i,7), Sdata%Kemat(i,8)
    ! enddo

    end subroutine makeK_mono

    !ここまで共通

    !KGLの算出
    subroutine mod05_get_BonG(Sdata,Mdata_G,Mdata_L,Ddata,et,gz,elemid)!L要素の要素番号,et,gzから対応する座標におけるG要素のBmatがtool%BonGTに保存される
        implicit none
        type(Smatdata) :: Sdata
        type(meshdata) :: Mdata_G,Mdata_L
        type(divisiondata) :: Ddata

        real(8),intent(in) :: et,gz
        integer(4),intent(in) :: elemid

        real(8) :: x_co,y_co,et_g,gz_g
        integer(4) :: Gelemid

        call mod04_get_co(Mdata_L,elemid,et,gz,x_co,y_co)!L要素番号,et,gzから対応する点の座標算出
        call mod04_get_Gmeshid(x_co,y_co,Ddata,Gelemid)!座標から所属するG要素番号算出
        call mod04_get_etgz(Mdata_G,Ddata,x_co,y_co,Gelemid,et_g,gz_g)!座標,G要素番号からet_g,gz_gを出す。
        call makeB(Sdata,Mdata_G,et_g,gz_g,Gelemid)!G要素上のBmatの値算出
        Sdata%BonGT = Sdata%BTmat!値はSdata%BonGTに保存
        
    end subroutine mod05_get_BonG

    subroutine mod05_addKetoK(Sdata,Mdata_L,Mdata_G,Ddata,et,gz,target)
        implicit none
        type(Smatdata) :: Sdata
        type(meshdata) :: Mdata_L,Mdata_G
        type(divisiondata) :: Ddata

        real(8),intent(in) :: et,gz
        integer(4),intent(in) :: target

        real(8) :: x_co,y_co
        integer(4) :: i,j,Gelemid,Lnodeid(4),Gnodeid(4),GonL(4)

        call mod04_get_co(Mdata_L,target,et,gz,x_co,y_co)!積分点の座標算出
        call mod04_get_Gmeshid(x_co,y_co,Ddata,Gelemid)

        do i = 1,4
        Lnodeid(i) = Mdata_L%elem_node_id(i,target)
        Gnodeid(i) = Mdata_G%elem_node_id(i,Gelemid)
        enddo

        do i = 1,4
            do j = 1,4
                Sdata%KGL(Gnodeid(j)*2-1,Lnodeid(i)*2-1) = Sdata%KGL(Gnodeid(j)*2-1,Lnodeid(i)*2-1) + Sdata%mat88(j*2-1,i*2-1)
                Sdata%KGL(Gnodeid(j)*2  ,Lnodeid(i)*2-1) = Sdata%KGL(Gnodeid(j)*2  ,Lnodeid(i)*2-1) + Sdata%mat88(j*2  ,i*2-1)
                Sdata%KGL(Gnodeid(j)*2-1,Lnodeid(i)*2  ) = Sdata%KGL(Gnodeid(j)*2-1,Lnodeid(i)*2  ) + Sdata%mat88(j*2-1,i*2  ) 
                Sdata%KGL(Gnodeid(j)*2  ,Lnodeid(i)*2  ) = Sdata%KGL(Gnodeid(j)*2  ,Lnodeid(i)*2  ) + Sdata%mat88(j*2  ,i*2  )         
            enddo
        enddo
        Sdata%mat88 = 0.0d0

    end subroutine mod05_addKetoK

    ! subroutine mod05_makeKe_multi(Sdata,Mdata_G,Mdata_L,Ddata,et,gz,target)!et,gz,targetを入れると、対応する積分点のBmatonG,Dmat,Bmat,detJの積を出力する
    !     implicit none
    !     type(Smatdata) :: Sdata
    !     type(meshdata) :: Mdata_G,Mdata_L
    !     type(divisiondata) :: Ddata
    !     real(8),intent(in) :: et,gz
    !     real(8) :: et_g,gz_g,x,mat38(3,8),mat83(8,3),mat88(8,8)
    !     integer(4),intent(in) :: target

    !     call mod05_get_BonG(Sdata,Mdata_G,Mdata_L,Ddata,et,gz,target)
    !     call makeB(Sdata,Mdata_L,et,gz,target)
    !     mat38 = matmul(Sdata%Dmat,Sdata%Bmat)
    !     Sdata%mat88 = matmul(Sdata%BonGT,mat38)*Sdata%detJ

    !     if(Mdata_L%complex(target))then!要素が境界をまたぐ場合のガウス積分
    !         ! write(*,*)target
    !         Sdata%mat88 = Sdata%mat88*Sdata%wip_subd(i_ip)*Sdata%wip_subd(j_ip)
    !     else!またがない場合のガウス積分
    !         Sdata%mat88 = Sdata%mat88*Sdata%wip(i_ip)*Sdata%wip(j_ip)
    !     endif

    !     call mod05_addKetoK(Sdata,Mdata_L,Mdata_G,Ddata,et,gz,target)
    ! end subroutine mod05_makeKe_multi

    ! subroutine mod05_makeKe_multi_subd(Sdata,Mdata_G,Mdata_L,Ddata,int1,int2,target)!et,gz,targetを入れると、対応する積分点のBmatonG,Dmat,Bmat,detJの積を出力する
    !     implicit none
    !     type(Smatdata) :: Sdata
    !     type(meshdata) :: Mdata_G,Mdata_L
    !     type(divisiondata) :: Ddata
    !     integer(4),intent(in) :: target,int1,int2

    !     real(8) :: et,gz
    !     real(8) :: et_g,gz_g,x,mat38(3,8),mat83(8,3),mat88(8,8)

    !     et = Sdata%co_ip_subd(int1)
    !     gz = Sdata%co_ip_subd(int2)

    !     call mod05_get_BonG(Sdata,Mdata_G,Mdata_L,Ddata,et,gz,target)
    !     call makeB(Sdata,Mdata_L,et,gz,target)
    !     mat38 = matmul(Sdata%Dmat,Sdata%Bmat)
    !     Sdata%mat88 = matmul(Sdata%BonGT,mat38)*Sdata%detJ

    !     !またぐ場合のガウス積分
    !     Sdata%mat88 = Sdata%mat88*Sdata%wip_subd(int1)*Sdata%wip_subd(int2)
    !     call mod05_addKetoK(Sdata,Mdata_L,Mdata_G,Ddata,et,gz,target)
    ! end subroutine mod05_makeKe_multi_subd

    subroutine mod05_makeKe_multi_subd(Sdata,Mdata_G,Mdata_L,Ddata,int1,target)!et,gz,targetを入れると、対応する積分点のBmatonG,Dmat,Bmat,detJの積を出力する
        implicit none
        type(Smatdata) :: Sdata
        type(meshdata) :: Mdata_G,Mdata_L
        type(divisiondata) :: Ddata
        integer(4),intent(in) :: target,int1

        real(8) :: et,gz
        real(8) :: et_g,gz_g,x,mat38(3,8),mat83(8,3),mat88(8,8)

        et = Sdata%co_ip_subd(1,int1)
        gz = Sdata%co_ip_subd(2,int1)
        ! write(*,*)et,gz

        call mod05_get_BonG(Sdata,Mdata_G,Mdata_L,Ddata,et,gz,target)
        call makeB(Sdata,Mdata_L,et,gz,target)
        mat38 = matmul(Sdata%Dmat,Sdata%Bmat)
        Sdata%mat88 = matmul(Sdata%BonGT,mat38)*Sdata%detJ

        !またぐ場合のガウス積分
        Sdata%mat88 = Sdata%mat88*Sdata%wip_subd
        call mod05_addKetoK(Sdata,Mdata_L,Mdata_G,Ddata,et,gz,target)
    end subroutine mod05_makeKe_multi_subd

    ! subroutine mod05_makeKe_multi(Sdata,Mdata_G,Mdata_L,Ddata,int1,int2,target)!et,gz,targetを入れると、対応する積分点のBmatonG,Dmat,Bmat,detJの積を出力する
    !     implicit none
    !     type(Smatdata) :: Sdata
    !     type(meshdata) :: Mdata_G,Mdata_L
    !     type(divisiondata) :: Ddata
    !     integer(4),intent(in) :: target,int1,int2

    !     real(8) :: et,gz
    !     real(8) :: et_g,gz_g,x,mat38(3,8),mat83(8,3),mat88(8,8)

    !     et = Sdata%co_ip(int1)
    !     gz = Sdata%co_ip(int2)


    !     call mod05_get_BonG(Sdata,Mdata_G,Mdata_L,Ddata,et,gz,target)
    !     call makeB(Sdata,Mdata_L,et,gz,target)
    !     mat38 = matmul(Sdata%Dmat,Sdata%Bmat)
    !     Sdata%mat88 = matmul(Sdata%BonGT,mat38)*Sdata%detJ

    !     !またがない場合のガウス積分
    !     Sdata%mat88 = Sdata%mat88*Sdata%wip(int1)*Sdata%wip(int2)
    !     call mod05_addKetoK(Sdata,Mdata_L,Mdata_G,Ddata,et,gz,target)
    ! end subroutine mod05_makeKe_multi

    subroutine mod05_makeKe_multi(Sdata,Mdata_G,Mdata_L,Ddata,i,target)!et,gz,targetを入れると、対応する積分点のBmatonG,Dmat,Bmat,detJの積を出力する
        implicit none
        type(Smatdata) :: Sdata
        type(meshdata) :: Mdata_G,Mdata_L
        type(divisiondata) :: Ddata
        integer(4),intent(in) :: target,i

        real(8) :: et,gz
        real(8) :: et_g,gz_g,x,mat38(3,8)

        et = Sdata%co_ip(1,i)
        gz = Sdata%co_ip(2,i)

        call mod05_get_BonG(Sdata,Mdata_G,Mdata_L,Ddata,et,gz,target)
        call makeB(Sdata,Mdata_L,et,gz,target)
        mat38 = matmul(Sdata%Dmat,Sdata%Bmat)
        Sdata%mat88 = matmul(Sdata%BonGT,mat38)*Sdata%detJ

        !またがない場合のガウス積分
        Sdata%mat88 = Sdata%mat88*Sdata%wip
        call mod05_addKetoK(Sdata,Mdata_L,Mdata_G,Ddata,et,gz,target)
    end subroutine mod05_makeKe_multi




    ! subroutine mod05_makeK_multi(Sdata,Mdata_G,Mdata_L,Ddata)!連成項の剛性行列を生成する
    !     implicit none
    !     type(Smatdata) :: Sdata
    !     type(meshdata) :: Mdata_G,Mdata_L
    !     type(divisiondata) :: Ddata
    !     integer(4):: target

    !     real(8) :: x,x_co,y_co
    !     integer(4) :: i,j,Gelemid,Lnodeid(4),Gnodeid(4)!各領域の節点番号を記録する

    !     !ガウス積分による積分計算(L要素はすべてまたがない判定される)
    !     do target = 1,Mdata_L%Nelem
    !         if(Mdata_L%complex(target))then!G要素が境界をまたぐ場合のガウス積分
    !             ! write(*,*)target
    !             do i = 1, Sdata%Nip_subd
    !                 do j = 1, Sdata%Nip_subd
    !                     call mod05_makeKe_multi_subd(Sdata,Mdata_G,Mdata_L,Ddata,i,j,target)
    !                 enddo
    !             enddo
    !         else!またがない場合のガウス積分
    !             do i = 1, Sdata%Nip
    !                 do j = 1, Sdata%Nip
    !                     call mod05_makeKe_multi(Sdata,Mdata_G,Mdata_L,Ddata,i,j,target)      
    !                 enddo
    !             enddo
    !         endif
    !     enddo

    ! end subroutine mod05_makeK_multi


    subroutine mod05_makeK_multi(Sdata,Mdata_G,Mdata_L,Ddata)!連成項の剛性行列を生成する
        implicit none
        type(Smatdata) :: Sdata
        type(meshdata) :: Mdata_G,Mdata_L
        type(divisiondata) :: Ddata
        integer(4):: target

        real(8) :: x,x_co,y_co
        integer(4) :: i,j,Gelemid,Lnodeid(4),Gnodeid(4)!各領域の節点番号を記録する

        if(.not. dbg_KGL)then



        !ガウス積分による積分計算
        do target = 1,Mdata_L%Nelem
            if(Mdata_L%complex(target))then!G要素が境界をまたぐ場合のガウス積分
                ! write(*,*)target
                do i = 1, Sdata%Nip_subd
                        call mod05_makeKe_multi_subd(Sdata,Mdata_G,Mdata_L,Ddata,i,target)
                        ! write(*,*)Sdata%co_ip_subd(1,i),Sdata%co_ip_subd(2,i)
                enddo
                ! write(*,*)Sdata%co_ip_subd(1,i),Sdata%co_ip_subd(2,i)
            else!またがない場合のガウス積分
                do i = 1, Sdata%Nip
                        call mod05_makeKe_multi(Sdata,Mdata_G,Mdata_L,Ddata,i,target)      
                enddo
            endif
        enddo



        endif

    end subroutine mod05_makeK_multi



    subroutine mod05_combineK(Mdata_G,Mdata_L,Sdata,CGdata)!作成した行列を組み合わせて全体剛性方程式のKマトリックスを生成
        implicit none
        type(meshdata) :: Mdata_G,Mdata_L
        type(Smatdata) :: Sdata
        type(CGmethoddata) :: CGdata

        integer(4) :: i,j

        ! CGdata%Kmat = 0.0d0
        !Gメッシュ部分
        do i = 1, CGdata%SoM_G
            do j = 1, CGdata%SoM_G
                CGdata%Kmat(i,j) = Mdata_G%Kmat(i,j)
            enddo
        enddo

        !Lメッシュ部分
        do i =  1, CGdata%SoM_L
            do j = 1, CGdata%SoM_L
                CGdata%Kmat(CGdata%SoM_G + i,CGdata%SoM_G + j) = Mdata_L%Kmat(i,j)
            enddo
        enddo

        !GL,LG部分
        do i = 1, CGdata%SoM_G
            do j =  1, CGdata%SoM_L
                CGdata%Kmat(i,CGdata%SoM_G +j) = Sdata%KGL(i,j)
                CGdata%Kmat(CGdata%SoM_G +j,i) = Sdata%KGL(i,j)
            enddo
        enddo

    end subroutine mod05_combineK



    subroutine mod05_completeK(Sdata,Mdata_G,Mdata_L,Ddata,CGdata)!上記のサブルーチンをまとめて実行することで全体剛性行列生成
        implicit none
        type(meshdata) :: Mdata_G,Mdata_L
        type(Smatdata) :: Sdata
        type(divisiondata) :: Ddata
        type(CGmethoddata) :: CGdata

        integer(4) :: i,j

        if(debug_option)then
            write(*,*)"Subroutine completeK start"
        endif

        call get_subdivision()!細分化するメッシュを決めている
        call get_integration_point(Sdata%Nip_subd,Sdata%co_ip_subd,Sdata%wip_subd)!積分領域の細分化に用いる変数を整理
        call makeK_mono(Sdata,Mdata_G)!KG作成
        call makeK_mono(Sdata,Mdata_L)!KL作成
        call mod05_makeK_multi(Sdata,Mdata_G,Mdata_L,Ddata)!KGL作成
        call mod05_combineK(Mdata_G,Mdata_L,Sdata,CGdata)!K作成

        if(debug_option)then
            write(*,*)"Finish"
        endif
    end subroutine mod05_completeK

    subroutine makeJ(Sdata,Mdata,et,gz,target)!Jmatのみを求める.mod08で使用
        implicit none
        type(Smatdata) :: Sdata
        type(meshdata) :: Mdata

        real(8),intent(in) :: et,gz
        integer(4),intent(in) :: target

        real(8) :: x,x_co,y_co,radius
        integer(4) :: i

        !Dmat導出
        call mod04_get_co(Mdata,target,et,gz,x_co,y_co)

        if(sqrt(x_co**2 + y_co**2) <= divi%leng_Garea*area_ratio)then!積分点が繊維領域内にあるとき

            Sdata%Dmat =0.0d0
            Sdata%Dmat(1,1) = 1/(1-poisson_1)
            Sdata%Dmat(2,2) = 1/(1-poisson_1)
            Sdata%Dmat(1,2) = poisson_1/(1-poisson_1)
            Sdata%Dmat(2,1) = poisson_1/(1-poisson_1)
            Sdata%Dmat(3,3) = 0.5
            x = young_1/(1+poisson_1)
            Sdata%Dmat = x * Sdata%Dmat

        else!積分点が繊維領域外にあるとき

            Sdata%Dmat =0.0d0
            Sdata%Dmat(1,1) = 1/(1-poisson_2)
            Sdata%Dmat(2,2) = 1/(1-poisson_2)
            Sdata%Dmat(1,2) = poisson_2/(1-poisson_2)
            Sdata%Dmat(2,1) = poisson_2/(1-poisson_2)
            Sdata%Dmat(3,3) = 0.5
            x = young_2/(1+poisson_2)
            Sdata%Dmat = x * Sdata%Dmat
        endif

        !Jmat導出
        Sdata%mat24 = 0.0d0
        Sdata%mat42_m = 0.0d0
        Sdata%Jmat = 0.0d0

        Sdata%mat24(1,1) = -(1-et)/4
        Sdata%mat24(1,2) = (1-et)/4
        Sdata%mat24(1,3) = (1+et)/4
        Sdata%mat24(1,4) = -(1+et)/4
        Sdata%mat24(2,1) = -(1-gz)/4
        Sdata%mat24(2,2) = -(1+gz)/4
        Sdata%mat24(2,3) = (1+gz)/4
        Sdata%mat24(2,4) = (1-gz)/4
        
        do i = 1,4!対象とする要素に対応する節点座標を記録
            Sdata%mat42_m(i,1) = Mdata%co_node(1,Mdata%elem_node_id(i,target))
            Sdata%mat42_m(i,2) = Mdata%co_node(2,Mdata%elem_node_id(i,target))
        enddo

        Sdata%Jmat = matmul(Sdata%mat24,Sdata%mat42_m)

        !detJ
        Sdata%detJ = Sdata%Jmat(1,1)*Sdata%Jmat(2,2) - Sdata%Jmat(1,2)*Sdata%Jmat(2,1)
    end subroutine makeJ


        

end module makeK