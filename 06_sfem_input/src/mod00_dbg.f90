module dbg_controller
    implicit none

    integer(4) :: n_rec_disig,i_Gmesh,j_Gmesh,i_Lmesh,j_Lmesh

    logical :: dbg_AdduG,dbg_division,dbg_subdivision,dbg_KGL,dbg_preCG,dbg_Gmesh,dbg_Lmesh
    logical :: debug_option = .false.!プログラムがどこまで実行されたか表示
    contains
    
    subroutine dbg()

    !メッシュ定義
        !trueでGmesh.datを作成
        dbg_Gmesh = .true.
        if(dbg_Gmesh)then
            i_Gmesh = 1!G領域一辺長さ
            j_Gmesh = 30!G領域一辺分割数
            open(unit = 101, file = 'Gmesh.dat', status = 'replace')
            write(101,'(i0,a,i0)')i_Gmesh,' ',j_Gmesh
            close(101)
        endif

        !tureでLmesh.datを作成
        dbg_Lmesh = .true.
        if(dbg_Gmesh)then
            i_Lmesh = 8!L領域四角形部分の一辺分割数
            j_Lmesh = 8!領域円形部分のr方向分割数
            open(unit = 102, file = 'Lmesh.dat', status = 'replace')
            write(102,'(i0,a,i0)')i_Lmesh,' ',j_Lmesh
            close(102)
        endif

    !Kマトリックス
        !trueでKGL=KLG=0
        dbg_KGL = .false.

    !数値積分
        !trueで細分化なし
        dbg_division = .false.

        !trueでn_recを指定できる(通常10)
        dbg_subdivision = .false.
        n_rec_disig = 20

    !CG法
        !trueで対角スケーリングなし
        dbg_preCG = .false.

    !足しこみ
        !trueでu_Gとu_L足しこみ無し
        dbg_AdduG = .false.

    end subroutine dbg



end module dbg_controller