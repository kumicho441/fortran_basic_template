module eval_L2
    use dbg_controller
    use variables
    use makeK
    implicit none

    contains

    ! subroutine makeL2_subset(Sdata,Mdata_L,CGdata,L2data,et,gz,target)!各要素と全体のL2誤差を求める(1/2)
    !     implicit none
    !     type(Smatdata) :: Sdata
    !     type(meshdata) :: Mdata_L
    !     type(CGmethoddata) :: CGdata
    !     type(evaluationdata) :: L2data

    !     real(8),intent(in) :: et,gz
    !     integer(4),intent(in) :: target

    !     real(8) :: mat24(2,4),N(4),ipx_co,ipy_co!ipは積分点を表す
    !     real(8) :: ux_th,uy_th,ux_cal,uy_cal,x,y
    !     real(8) :: radius,ur,theta,p,q

    !     ! real(8),allocatable :: 
    !     integer(4) :: i

    !     mat24 = 0.0d0
    !     ipx_co = 0.0d0
    !     ipy_co = 0.0d0
    !     ux_cal = 0.0d0
    !     uy_cal = 0.0d0


    !     call makeJ(Sdata,Mdata_L,et,gz,target)!Jmat,detJの算出

    !     N(1) = (1 - gz) * (1 - et) / 4
    !     N(2) = (1 + gz) * (1 - et) / 4
    !     N(3) = (1 + gz) * (1 + et) / 4
    !     N(4) = (1 - gz) * (1 + et) / 4        

    !     do i = 1,4
    !         ipx_co = ipx_co + N(i)*Mdata_L%co_node(1,Mdata_L%elem_node_id(i,target))!積分点のx座標
    !         ipy_co = ipy_co + N(i)*Mdata_L%co_node(2,Mdata_L%elem_node_id(i,target))!積分点のy座標
    !     enddo

    !     !理論解について
    !     ! ux_th = CGdata%Force*(1-poisson_1)/young_1 * ipx_co
    !     ! uy_th = CGdata%Force*(1-poisson_1)/young_1 * ipy_co
    !     radius = sqrt(ipx_co**2 + ipy_co**2)
    !     theta = atan(ipy_co/ipx_co)
    !     p = lengl
    !     q = lengg*sqrt(2.0d0)

    !     ! if(radius <= lengl)then
    !         ur = ((1-(q**2/p**2)) * alpha + (q**2/p**2)) * radius
    !     ! else
    !     !     ur = (radius - q**2/radius) * alpha + q**2 / radius!L領域では使用しない解
    !     ! endif

    !     ux_th = ur * cos(theta)
    !     uy_th = ur * sin(theta)

    !     do i = 1,4
    !         ux_cal = ux_cal + N(i) * CGdata%uvec(CGdata%SoM_G + Mdata_L%elem_node_id(i,target)*2 - 1)
    !         uy_cal = uy_cal + N(i) * CGdata%uvec(CGdata%SoM_G + Mdata_L%elem_node_id(i,target)*2    )
    !     enddo

    !     x = ((ux_cal - ux_th) ** 2 + (uy_cal - uy_th) ** 2) * Sdata%detJ
    !     y = (ux_th ** 2 + uy_th ** 2) * Sdata%detJ

    !     !積分の細分化
    !     ! if(i_ip*j_ip /= 0)then !積分点を増やす場合の処理
    !     !     x = x*wip(i_ip,j_ip)
    !     !     y = y*wip(i_ip,j_ip)
    !     ! endif

    !         ! if(Mdata_L%complex(target))then!G要素が境界をまたぐ場合のガウス積分
    !         !     do i_ip = 1, Sdata%Nip_subd
    !         !         do j_ip = 1, Sdata%Nip_subd
    !         !             x = x*Sdata%wip_subd(i_ip)*Sdata%wip_subd(j_ip)
    !         !             y = y*Sdata%wip_subd(i_ip)*Sdata%wip_subd(j_ip)
    !         !         enddo
    !         !     enddo
    !         ! else!またがない場合のガウス積分
    !             do i_ip = 1, Sdata%Nip
    !                 do j_ip = 1, Sdata%Nip
    !                     x = x*Sdata%wip(i_ip)*Sdata%wip(j_ip)
    !                     y = y*Sdata%wip(i_ip)*Sdata%wip(j_ip)
    !                 enddo
    !             enddo
    !         ! endif

    !     L2data%dif_nu = L2data%dif_nu + x
    !     L2data%dif_de = L2data%dif_de + y

    !     L2data%L2_nu = L2data%L2_nu + x
    !     L2data%L2_de = L2data%L2_de + y

    !     ! write(*,*)L2data%dif_nu,L2data%dif_de
    !     ! write(*,*)
    ! end subroutine makeL2_subset

    subroutine makeL2_subset(Sdata,Mdata_L,CGdata,L2data,int1,target)!各要素と全体のL2誤差を求める(1/2)
        implicit none
        type(Smatdata) :: Sdata
        type(meshdata) :: Mdata_L
        type(CGmethoddata) :: CGdata
        type(evaluationdata) :: L2data


        integer(4),intent(in) :: target,int1

        real(8) :: et,gz
        real(8) :: mat24(2,4),N(4),ipx_co,ipy_co!ipは積分点を表す
        real(8) :: ux_th,uy_th,ux_cal,uy_cal,x,y
        real(8) :: radius,ur,theta,p,q

        ! real(8),allocatable :: 
        integer(4) :: i
        et = Sdata%co_ip(1,int1)
        gz = Sdata%co_ip(2,int1)

        mat24 = 0.0d0
        ipx_co = 0.0d0
        ipy_co = 0.0d0
        ux_cal = 0.0d0
        uy_cal = 0.0d0


        call makeJ(Sdata,Mdata_L,et,gz,target)!Jmat,detJの算出

        N(1) = (1 - gz) * (1 - et) / 4
        N(2) = (1 + gz) * (1 - et) / 4
        N(3) = (1 + gz) * (1 + et) / 4
        N(4) = (1 - gz) * (1 + et) / 4        

        do i = 1,4
            ipx_co = ipx_co + N(i)*Mdata_L%co_node(1,Mdata_L%elem_node_id(i,target))!積分点のx座標
            ipy_co = ipy_co + N(i)*Mdata_L%co_node(2,Mdata_L%elem_node_id(i,target))!積分点のy座標
        enddo

        !理論解について
        ! ux_th = CGdata%Force*(1-poisson_1)/young_1 * ipx_co
        ! uy_th = CGdata%Force*(1-poisson_1)/young_1 * ipy_co
        radius = sqrt(ipx_co**2 + ipy_co**2)
        theta = atan(ipy_co/ipx_co)
        p = lengl
        q = lengg*sqrt(2.0d0)

        ! if(radius <= lengl)then
            ur = ((1-(q**2/p**2)) * alpha + (q**2/p**2)) * radius
        ! else
        !     ur = (radius - q**2/radius) * alpha + q**2 / radius!L領域では使用しない解
        ! endif

        ux_th = ur * cos(theta)
        uy_th = ur * sin(theta)

        do i = 1,4
            ux_cal = ux_cal + N(i) * CGdata%uvec(CGdata%SoM_G + Mdata_L%elem_node_id(i,target)*2 - 1)
            uy_cal = uy_cal + N(i) * CGdata%uvec(CGdata%SoM_G + Mdata_L%elem_node_id(i,target)*2    )
        enddo

        x = ((ux_cal - ux_th) ** 2 + (uy_cal - uy_th) ** 2) * Sdata%detJ
        y = (ux_th ** 2 + uy_th ** 2) * Sdata%detJ



        x = x*Sdata%wip!重みづけ
        y = y*Sdata%wip

        L2data%dif_nu = L2data%dif_nu + x
        L2data%dif_de = L2data%dif_de + y

        L2data%L2_nu = L2data%L2_nu + x
        L2data%L2_de = L2data%L2_de + y

        ! write(*,*)L2data%dif_nu,L2data%dif_de
        ! write(*,*)
    end subroutine makeL2_subset


    subroutine makeL2_full(Sdata,Mdata_L,Mdata_G,CGdata,L2data)!各要素と全体のL2誤差を求める(2/2)
        implicit none
        type(Smatdata) :: Sdata
        type(meshdata) :: Mdata_L,Mdata_G
        type(CGmethoddata) :: CGdata
        type(evaluationdata) :: L2data

        real(8) :: x,radius,theta,ur,p,q
        integer(4) :: target,i

        if(debug_option)then
            write(*,*)"subroutine makeL2_full start"
        endif
        x = 0.57735
        !以下3行はデバッグ
                ! write(*,*)'円周上変位 = ',(((1-(lengg**2/lengl**2)) * alpha + (lengg**2/lengl**2)) * lengl) 
                ! write(*,*)((lengl - lengg**2/lengl) * alpha + lengg**2 / lengl) 
                ! write(*,*)'右辺変位 = ',((lengg - lengg**2/lengg) * alpha + lengg**2 / lengg) 
        do target = 1, Mdata_L%Nelem

            L2data%dif_nu = 0.0d0
            L2data%dif_de = 0.0d0
            ! call makeL2_subset(Sdata,Mdata_L,CGdata,L2data,-x,-x,target)

            ! call makeL2_subset(Sdata,Mdata_L,CGdata,L2data, x,-x,target)

            ! call makeL2_subset(Sdata,Mdata_L,CGdata,L2data, x, x,target)

            ! call makeL2_subset(Sdata,Mdata_L,CGdata,L2data,-x, x,target)

            ! else!またがない場合のガウス積分
                do i = 1, Sdata%Nip
                        call makeL2_subset(Sdata,Mdata_L,CGdata,L2data,i,target)
                enddo
            ! endif

            ! write(*,*)'target = ',target
            ! write(*,*)L2data%dif_nu,L2data%dif_de
            L2data%elel2(target) = sqrt(L2data%dif_nu/L2data%dif_de)!平方根をとっている

        enddo

        p = lengl
        q = lengg*sqrt(2.0d0)
        do i = 1,Mdata_L%Nnode!理論解の算出(L領域)
            ! write(*,*)i

            radius = sqrt(Mdata_L%co_node(1,i)**2 + Mdata_L%co_node(2,i)**2)

            if(radius <= 1.0E-08)then!原点の場合の処理
                ur = 0.0d0
                theta = 0.0d0
            else

                if(Mdata_L%co_node(1,i) <= 1.0E-08)then!atanが求められない場合の処理
                    theta = pi/2
                else
                    theta = atan(Mdata_L%co_node(2,i)/Mdata_L%co_node(1,i))
                endif
                ! if(radius <= lengl)then!円孔内外判定
                    ur = ((1-(q**2/p**2)) * alpha + (q**2/p**2)) * radius
                ! else
                !     ur = (radius - q**2/radius) * alpha + q**2 / radius
                ! endif
            endif
            L2data%uvec_th(CGdata%SoM_G + i*2 - 1) = ur * cos(theta)
            L2data%uvec_th(CGdata%SoM_G + i*2    ) = ur * sin(theta)

        enddo

        do i = 1,Mdata_G%Nnode!理論解の算出(G領域)
            ! write(*,*)i

            radius = sqrt(Mdata_G%co_node(1,i)**2 + Mdata_G%co_node(2,i)**2)

            if(radius <= 1.0E-08)then!原点の場合の処理
                ur = 0.0d0
                theta = 0.0d0
            else

                if(Mdata_G%co_node(1,i) <= 1.0E-08)then!atanが求められない場合の処理
                    theta = pi/2
                else
                    theta = atan(Mdata_G%co_node(2,i)/Mdata_G%co_node(1,i))
                endif
                if(radius <= lengl)then!円孔内外判定
                    ur = ((1-(q**2/p**2)) * alpha + (q**2/p**2)) * radius
                else
                    ur = (radius - q**2/radius) * alpha + q**2 / radius
                endif

            endif

            L2data%uvec_th( i*2 - 1) = ur * cos(theta)
            L2data%uvec_th( i*2    ) = ur * sin(theta)

        enddo





        L2data%L2 = sqrt(L2data%L2_nu)/sqrt(L2data%L2_de)


        if(debug_option)then
            write(*,*)"Finish"
        endif

    end subroutine makeL2_full

end module eval_L2