module initialize
    use dbg_controller
    use variables
    implicit none
    contains


    subroutine initialize_data()!変数の初期化・allocateを行う
        implicit none
        integer(4) :: i,j,k,a, b, c
        real(8) :: x
        integer(4) :: numCond=12

        real(8),parameter :: pi = 4.0d0*atan(1.0d0)
        real(8) :: f,df,dx
        real(8), allocatable :: p0(:),p1(:),tmp(:)
        real(8), allocatable :: q0(:),q1(:),tmq(:)


        if(debug_option)then
            write(*,*)"Subroutine initialize_data start"
        endif
        

        ! young,poissonの初期化
        open(numCond, file="cond.dat", status="old")
        read(numCond, *) young_1,poisson_1
        ! write(*,*) young_1,poisson_1
        read(numCond, *) young_2,poisson_2
        ! write(*,*) young_2,poisson_2
        close(numCond)


        ! Gdataの初期化
        Gdata%Nelem = 0
        Gdata%Nnode = 0

        open(numG, file="Gmesh.dat", status="old")
        read(numG, *) x, a  !{正方形の一辺の長さ, 各辺の分割数}
        Gdata%Nelem = a * a
        Gdata%Nnode = (a + 1) * (a + 1)
        close(numG)

        allocate(Gdata%co_node(2, Gdata%Nnode), &
                 Gdata%elem_node_id(4, Gdata%Nelem), &
                 Gdata%Bmat_m(3, 8, Gdata%Nelem),&
                 Gdata%Kmat(dim*Gdata%Nnode,dim*Gdata%Nnode),&
                 Gdata%complex(Gdata%Nelem))
        Gdata%co_node = 0.0d0
        Gdata%elem_node_id = 0
        Gdata%Bmat_m = 0.0d0
        Gdata%Kmat = 0.0d0

        ! Ldataの初期化
        Ldata%Nelem = 0
        Ldata%Nnode = 0


        ! !Lmesh.dat読み取り
        open(numL, file="Lmesh.dat", status="old")
            read(numL, *) b,c! {正方形各辺の分割数,r方向分割数}
        close(numL)
        Ldata%Nelem = b * b + c * (2 * b)
        Ldata%Nnode = (b + 1) * (b + 1) + c * (2 * b + 1)

        ! !デバッグ用読み取り
        ! open(numG, file="Gmesh.dat", status="old")
        !     read(numG, *) b,c !{ローカル領域の長さ, 各辺の分割数}
        ! close(numG)
        ! Ldata%Nelem = b*b
        ! Ldata%Nnode = (b + 1) * (b + 1) 

        allocate(Ldata%co_node(2, Ldata%Nnode), &
                 Ldata%elem_node_id(4, Ldata%Nelem), &
                 Ldata%Bmat_m(3, 8, Ldata%Nelem),&
                 Ldata%Kmat(dim*Ldata%Nnode,dim*Ldata%Nnode),&
                 Ldata%complex(Ldata%Nelem))
        Ldata%co_node = 0.0d0
        Ldata%elem_node_id = 0
        Ldata%Bmat_m = 0.0d0
        Ldata%Kmat = 0.0d0

        !smdの初期化
        smd%Dmat = 0.0d0
        smd%Jmat = 0.0d0
        smd%Jinv = 0.0d0
        smd%detJ = 0.0d0
        smd%Bmat = 0.0d0
        smd%BTmat = 0.0d0
        smd%Kemat = 0.0d0
        smd%KeGL = 0.0d0
        smd%mat38 = 0.0d0
        smd%mat24 = 0.0d0
        smd%mat42_m = 0.0d0
        smd%mat42_u = 0.0d0
        smd%mat88 = 0.0d0
        smd%BonGT = 0.0d0

        if(dbg_subdivision)then
        smd%n_rec = n_rec_disig
        endif

        allocate(smd%KGL(Gdata%Nnode*2,Ldata%Nnode*2))
        smd%KGL = 0.0d0

        !CGdの初期化
        CGd%SoM_G = 2*Gdata%Nnode
        CGd%SoM_L = 2*Ldata%Nnode
        CGd%SoM = CGd%SoM_L + CGd%SoM_G
        CGd%Force = 0.0d0
        CGd%load = 0.0d0

        allocate(CGd%Kmat(CGd%SoM,CGd%SoM),CGd%Fvec(CGd%SoM),&
        CGd%uvec(CGd%SoM),CGd%rvec(CGd%SoM), CGd%r0(CGd%SoM), &
        CGd%rvec_save(CGd%SoM), CGd%pvec(CGd%SoM), CGd%alp(CGd%SoM),&
        CGd%bet(CGd%SoM), CGd%pro(CGd%SoM),CGd%Pmat(CGd%SoM,CGd%SoM),&
        CGd%PTmat(CGd%SoM,CGd%SoM),CGd%Pinv(CGd%SoM,CGd%SoM))

        CGd%Kmat = 0.0d0
        CGd%Fvec = 0.0d0
        CGd%uvec = 0.0d0

        CGd%rvec = 0.0d0
        CGd%r0 = 0.0d0
        CGd%rvec_save = 0.0d0
        CGd%pvec = 0.0d0
        CGd%alp = 0.0d0
        CGd%bet = 0.0d0
        CGd%pro = 0.0d0
        CGd%Pmat = 0.0d0
        CGd%PTmat = 0.0d0
        CGd%Pinv = 0.0d0

    
        !evdの初期化
        evd%L2 = 0.0d0
        evd%L2_nu = 0.0d0
        evd%L2_de = 0.0d0
        evd%dif_nu = 0.0d0
        evd%dif_de = 0.0d0

        allocate(evd%elel2(Ldata%Nelem),evd%uvec_th(CGd%SoM))!誤差評価はL領域に対してのみ行う
        evd%elel2 = 0.0d0
        evd%uvec_th = 0.0d0

        ! !積分点についての初期化
        ! allocate(smd%co_ip(smd%Nip),smd%wip(smd%Nip))
        ! allocate(smd%co_ip_subd(smd%Nip_subd),smd%wip_subd(smd%Nip_subd))

        !通常の積分点についての初期化
        allocate(smd%co_ip(2,smd%Nip))

        ! allocate(smd%co_ip_subd(smd%Nip_subd),smd%wip_subd(smd%Nip_subd))
        smd%co_ip(1,1) = -0.57735027
        smd%co_ip(2,1) = -0.57735027
        smd%co_ip(1,2) = -0.57735027
        smd%co_ip(2,2) =  0.57735027
        smd%co_ip(1,3) =  0.57735027
        smd%co_ip(2,3) =  0.57735027
        smd%co_ip(1,4) = -0.57735027
        smd%co_ip(2,4) =  0.57735027
        smd%wip = 1.0d0


        ! !通常の積分点の座標・重さを算出
        ! p0 = [1.d0]
        ! p1 = [1.d0, 0.d0]

        ! do i = 2,smd%Nip
        !     tmp = (2.0d0*dble(i) - 1)*[p1,0.d0] - (i - 1.0d0)*[0.d0, 0.d0, p0]
        !     tmp = tmp/dble(i)
        !     p0 = p1
        !     p1 = tmp
        ! enddo

        ! do i = 1,smd%Nip
        !     x = dcos(pi*(i - 0.25d0)/(smd%Nip+0.5d0))
        !     do j = 1,10
        !         f = p1(1)
        !         df = 0.0d0
        !         do k = 2,size(p1)
        !             df = f + x*df
        !             f = p1(k) + x*f
        !         enddo
        !         dx = f/df
        !         x = x-dx
        !         if(abs(dx) < 1.d2*epsilon(dx)) exit
        !     enddo

        !     smd%co_ip(i) = x
        !     smd%wip(i)   = 2.0d0/((1.0d0 - x*x)*df*df)
            
        ! enddo
        
        ! !細分化された積分点の座標・重さを算出
        ! q0 = [1.d0]
        ! q1 = [1.d0, 0.d0]

        ! do i = 2,smd%Nip_subd
        !     tmp = (2.0d0*dble(i) - 1)*[q1,0.d0] - (i - 1.0d0)*[0.d0, 0.d0, q0]
        !     tmp = tmp/dble(i)
        !     q0 = q1
        !     q1 = tmp
        ! enddo

        ! do i = 1,smd%Nip_subd
        !     x = dcos(pi*(i - 0.25d0)/(smd%Nip_subd+0.5d0))

        !     do j = 1,10
        !         f = q1(1)
        !         df = 0.0d0
        !         do k = 2,size(q1)
        !             df = f + x*df
        !             f = q1(k) + x*f
        !         enddo
        !         dx = f/df
        !         x = x-dx
        !         if(abs(dx)<1.d2*epsilon(dx)) exit
        !     enddo
        !     smd%co_ip_subd(i) = x
        !     smd%wip_subd(i)   = 2.0d0/((1.0d0 - x*x)*df*df)
        ! enddo

        ! ! write(*,*)smd%Nip
        ! do i = 1,smd%Nip_subd
        !     write(*,*)smd%co_ip_subd(i),smd%wip_subd(i) 
        ! enddo

        ! if(debug_option)then
        !     write(*,*)"Finish"
        ! endif
    end subroutine initialize_data

end module initialize

