module bc_and_solve
    use dbg_controller
    use variables

    implicit none
    contains

    subroutine integrate_BC(Ddata,CGdata,Mdata_G)!境界条件付与を行う
        implicit none
        type(meshdata) :: Mdata_G
        type(divisiondata) :: Ddata
        type(CGmethoddata) :: CGdata
        integer(4) :: i,j,a,b,c,d,e,n,Nload,Nbc,Ndim,Gp1
        real(8) :: x,theta,radius,p,q,ur

        if(debug_option)then
            write(*,*)"Subroutine integrate_BC start"
        endif

        !load.datファイルの作成(外力が0でない部分に対して与える)
        Gp1 = Ddata%diviG + 1
        open(unit = numload, file = 'load.dat', status = 'replace')
            write(numload,'(i0,a)')(Gp1)*2,' 2'
            
            CGdata%Force = 0.0!外力の値を入力している
            CGdata%load = CGdata%Force * Ddata%leng_Gmesh / 2

            !G領域,x方向引張

            do i = 2,Ddata%diviG
                write(numload, '(i0,a,i0,a,G0)') i*(Gp1),' ',1,' ',CGdata%load*2
            enddo
            write(numload, '(i0,a,i0,a,G0)') 1*(Gp1),' ',1,' ',CGdata%load
            write(numload, '(i0,a,i0,a,G0)') (Gp1)*(Gp1),' ',1,' ',CGdata%load

            !G領域,y方向引張
            do i = 2,Ddata%diviG
                write(numload, '(i0,a,i0,a,G0)') Ddata%diviG*(Gp1) + i,' ',2,' ',CGdata%load*2
            enddo
            write(numload, '(i0,a,i0,a,G0)') Ddata%diviG*(Gp1) + 1,' ',2,' ',CGdata%load
            write(numload, '(i0,a,i0,a,G0)') (Gp1)*(Gp1) ,' ',2,' ',CGdata%load

        close(numload)
        ! write(*,*)'load.dat closed'

        !bc.datファイルの作成
        open(unit = numbc, file = 'bc.dat', status = 'replace')
            ! write(numbc,'(i0,a)')(Ddata%diviG+1)*2+ (Ddata%diviL_c)*4 + (Ddata%diviL_q)*8 ,' 2'
            !もともと境界条件数は(Gp1)*2 + (Ddata%diviL_q*2+2)  + (Ddata%diviL_c*4) + (Ddata%diviL_q*2-1)*2

            ! write(numbc,'(i0,a)')(Ddata%diviG+1)*2+ (Ddata%diviL_c)*4 + (Ddata%diviL_q)*8 + Ddata%diviG*4,' 2'
            write(numbc,'(i0,a)')(Ddata%diviG+1)*2+ (Ddata%diviL_c)*2 + (Ddata%diviL_q)*6 + Ddata%diviG*4+2,' 2'

            !G領域固定
            do i = 1,Ddata%diviG!右下・左上の節点の固定は理論解付与部分で与えられる
                write(numbc, '(i0,a,i0,a,G0)') i,' ',2,' ',0.0d0!底辺の固定
            enddo
            do i = 1,Ddata%diviG!右下・左上の節点の固定は理論解付与部分で与えられる
            write(numbc, '(i0,a,i0,a,G0)') (i-1)*(Gp1)+1,' ',1,' ',0.0d0!左辺の固定
            enddo

            !G領域強制変位
            !理論解算出に用いる定数
            lengl = divi%leng_Garea*area_ratio 
            lengg = divi%leng_Garea
            p = lengl
            q = lengg*sqrt(2.0d0)
    
            ram1 = young_1*poisson_1 / ((1+poisson_1)*(1-2*poisson_1))
            ram2 = young_2*poisson_2 / ((1+poisson_2)*(1-2*poisson_2))
            mu1 = young_1/(2*(1+poisson_1))
            mu2 = young_2/(2*(1+poisson_2))
            alpha = ((ram1 + mu1 + mu2) * (q**2)) / ((ram2 + mu2) * (p**2) + &
            (ram1 + mu1) * (q**2 - p**2) + mu2 * (q**2))
            ! write(*,*) 'alpha = ',alpha


            !右辺に理論解を与える
            do i = 1, Gp1
                radius = sqrt(Mdata_G%co_node(1,i*(Gp1))**2 + Mdata_G%co_node(2,i*(Gp1))**2)
                if(Mdata_G%co_node(1,i*(Gp1)) == 0.0d0)then
                    theta = pi/2
                else
                    theta = atan(Mdata_G%co_node(2,i*(Gp1))/Mdata_G%co_node(1,i*(Gp1)))

                endif
                ur = (radius - q**2/radius)*alpha + q**2/radius

                write(numbc, '(i0,a,i0,a,G0)') i*(Gp1),' ',1,' ',ur*cos(theta)
                ! write(numbc, '(i0,a,i0,a,G0)') i*(Gp1),' ',2,' ',ur*sin(theta)
            enddo

            do i = 1, Gp1
                radius = sqrt(Mdata_G%co_node(1,i*(Gp1))**2 + Mdata_G%co_node(2,i*(Gp1))**2)
                if(Mdata_G%co_node(1,i*(Gp1)) == 0.0d0)then
                    theta = pi/2
                else
                    theta = atan(Mdata_G%co_node(2,i*(Gp1))/Mdata_G%co_node(1,i*(Gp1)))

                endif
                ur = (radius - q**2/radius)*alpha + q**2/radius

                ! write(numbc, '(i0,a,i0,a,G0)') i*(Gp1),' ',1,' ',ur*cos(theta)
                write(numbc, '(i0,a,i0,a,G0)') i*(Gp1),' ',2,' ',ur*sin(theta)
            enddo

            ! radius = 5.0d0
            ! theta = 0.0d0
            ! ur = (radius - q**2/radius)*alpha + q**2/radius
            ! ur = ((1-(q**2/p**2)) * alpha + (q**2/p**2)) * radius!内側
            ! write(*,*)ur*cos(theta)
            ! write(*,*)ur*sin(theta)

            !上辺に理論解を与える
            do i = 1, Ddata%diviG

                radius = sqrt(Mdata_G%co_node(1,Ddata%diviG*(Gp1) + i)**2 + &
                Mdata_G%co_node(2,Ddata%diviG*(Gp1) + i)**2)
                if(Mdata_G%co_node(1,Ddata%diviG*(Gp1) + i) == 0.0d0)then
                    theta = pi/2
                else
                    theta = atan(Mdata_G%co_node(2,Ddata%diviG*(Gp1) + i)/&
                    Mdata_G%co_node(1,Ddata%diviG*(Gp1) + i))

                endif
                ur = (radius - q**2/radius)*alpha + q**2/radius
                write(numbc, '(i0,a,i0,a,G0)') Ddata%diviG*(Gp1) + i,' ',1,' ',ur*cos(theta)
                ! write(numbc, '(i0,a,i0,a,G0)') Ddata%diviG*(Gp1) + i,' ',2,' ',ur*sin(theta)

            enddo
            !上辺に理論解を与える
            do i = 1, Ddata%diviG

                radius = sqrt(Mdata_G%co_node(1,Ddata%diviG*(Gp1) + i)**2 + &
                Mdata_G%co_node(2,Ddata%diviG*(Gp1) + i)**2)
                if(Mdata_G%co_node(1,Ddata%diviG*(Gp1) + i) == 0.0d0)then
                    theta = pi/2
                else
                    theta = atan(Mdata_G%co_node(2,Ddata%diviG*(Gp1) + i)/&
                    Mdata_G%co_node(1,Ddata%diviG*(Gp1) + i))

                endif
                ur = (radius - q**2/radius)*alpha + q**2/radius
                ! write(numbc, '(i0,a,i0,a,G0)') Ddata%diviG*(Gp1) + i,' ',1,' ',ur*cos(theta)
                write(numbc, '(i0,a,i0,a,G0)') Ddata%diviG*(Gp1) + i,' ',2,' ',ur*sin(theta)

            enddo

            ! !L領域固定(節点番号の付け方に注意)
            ! c = Mdata_G%Nnode
            ! do i = 1,Ddata%diviL_q +1
            !     ! write(numbc, '(i0,a,i0,a,G0)') c+i,' ',1,' ',0.0d0
            !     ! write(numbc, '(i0,a,i0,a,G0)') c+i,' ',2,' ',0.0d0

            !     ! write(numbc, '(i0,a,i0,a,G0)') c+i,' ',1,' ',0.0d0
            !     write(numbc, '(i0,a,i0,a,G0)') c+i,' ',2,' ',0.0d0
            ! enddo
            ! do i = 2,Ddata%diviL_q + 1
            !     ! write(numbc, '(i0,a,i0,a,G0)') c+(i-1)*(Ddata%diviL_q + 1)+1,' ',1,' ',0.0d0
            !     ! write(numbc, '(i0,a,i0,a,G0)') c+(i-1)*(Ddata%diviL_q + 1)+1,' ',2,' ',0.0d0

            !     write(numbc, '(i0,a,i0,a,G0)') c+(i-1)*(Ddata%diviL_q + 1)+1,' ',1,' ',0.0d0
            !     ! write(numbc, '(i0,a,i0,a,G0)') c+(i-1)*(Ddata%diviL_q + 1)+1,' ',2,' ',0.0d0
            ! enddo

            ! d = (Ddata%diviL_q + 1)*(Ddata%diviL_q + 1)

            ! do i = 1 , Ddata%diviL_c
            !     ! write(numbc, '(i0,a,i0,a,G0)') c+d+i,' ',1,' ',0.0d0
            !     ! write(numbc, '(i0,a,i0,a,G0)') c+d+i,' ',2,' ',0.0d0

            !     ! write(numbc, '(i0,a,i0,a,G0)') c+d+i,' ',1,' ',0.0d0
            !     write(numbc, '(i0,a,i0,a,G0)') c+d+i,' ',2,' ',0.0d0
            ! enddo
            ! do i = Ddata%diviL_c*(Ddata%diviL_q)*2 +1,&
            !     Ddata%diviL_c*(Ddata%diviL_q)*2 + Ddata%diviL_c
            !     ! write(numbc, '(i0,a,i0,a,G0)') c+d+i,' ',1,' ',0.0d0
            !     ! write(numbc, '(i0,a,i0,a,G0)') c+d+i,' ',2,' ',0.0d0

            !     write(numbc, '(i0,a,i0,a,G0)') c+d+i,' ',1,' ',0.0d0
            !     ! write(numbc, '(i0,a,i0,a,G0)') c+d+i,' ',2,' ',0.0d0
            ! enddo

            ! do i = 2, Ddata%diviL_q*2
            !     write(numbc, '(i0,a,i0,a,G0)') c+d+ i * Ddata%diviL_c,' ',1,' ',0.0d0
            !     write(numbc, '(i0,a,i0,a,G0)') c+d+ i * Ddata%diviL_c,' ',2,' ',0.0d0
            ! enddo

            !L領域固定(節点番号の付け方に注意)
            c = Mdata_G%Nnode
            !正方形部分固定
            do i = 1,Ddata%diviL_q +1
                write(numbc, '(i0,a,i0,a,G0)') c+i,' ',2,' ',0.0d0
            enddo
            do i = 1,Ddata%diviL_q + 1
                write(numbc, '(i0,a,i0,a,G0)') c+(i-1)*(Ddata%diviL_q + 1)+1,' ',1,' ',0.0d0
            enddo

            d = (Ddata%diviL_q + 1)*(Ddata%diviL_q + 1)

            !繊維部分(円孔除く)固定
            do i = 1 , Ddata%diviL_c
                write(numbc, '(i0,a,i0,a,G0)') c+d+i,' ',2,' ',0.0d0
            enddo
            e = Ddata%diviL_c*(Ddata%diviL_q*2 + 1)
            do i = 1,Ddata%diviL_c
                write(numbc, '(i0,a,i0,a,G0)') c+d+e+1 - i,' ',1,' ',0.0d0            
            enddo

            !繊維部分(境界上)固定
            do i = 2, Ddata%diviL_q*2
                write(numbc, '(i0,a,i0,a,G0)') c+d+ i * Ddata%diviL_c,' ',1,' ',0.0d0
                write(numbc, '(i0,a,i0,a,G0)') c+d+ i * Ddata%diviL_c,' ',2,' ',0.0d0
            enddo
            write(numbc, '(i0,a,i0,a,G0)') c+d+ Ddata%diviL_c,' ',1,' ',0.0d0
            write(numbc, '(i0,a,i0,a,G0)') c+d+e,' ',2,' ',0.0d0


        close(numbc)

        !ファイル読み取り&境界条件付与
        !外力の境界条件
        ! open(numload,file="load.dat",status="old")
        !     read(numload,*)Nload,Ndim

        !     do i = 1,Nload
        !         read(numload,*) a,b,x
        !         CGdata%Fvec((a-1)*Ndim+b) = x
        !     enddo
        ! close(numload)

        !変位の境界条件
        open(numbc,file="bc.dat",status="old")
            read(numbc,*) Nbc,Ndim
            ! write(*,*)"Nbc = ",Nbc

            do i = 1,Nbc
                read(numbc,*) a,b,x
                n = (a-1)*Ndim + b

                do j = 1, CGdata%SoM
                    CGdata%Fvec(j) = CGdata%Fvec(j) - CGdata%Kmat(j,n)*x !>右辺を適切に引き算
                enddo

                do j = 1,CGdata%SoM
                    CGdata%Kmat(n,j) = 0.0d0
                    CGdata%Kmat(j,n) = 0.0d0
                enddo

                CGdata%Kmat(n,n) = 1.0d0
                CGdata%uvec(n) = x
                CGdata%Fvec(n) = x

            enddo
        close(numbc)

        !対角スケーリング的前処理(前処理時とそれ以外で結果が変わる)
        if(.not. dbg_preCG)then
        do i = 1, CGd%SoM
            CGdata%Pmat(i,i) = sqrt(abs(CGdata%Kmat(i,i)))
            CGdata%PTmat(i,i) = CGdata%Pmat(i,i)
            CGdata%Pinv(i,i) = 1/sqrt(abs(CGdata%Kmat(i,i)))
        enddo
        CGdata%Kmat = matmul(CGdata%Kmat,CGdata%Pinv)
        CGdata%Kmat = matmul(CGdata%Pinv,CGdata%Kmat)
        CGdata%uvec = matmul(CGdata%PTmat,CGdata%uvec)
        CGdata%Fvec = matmul(CGdata%Pinv,CGdata%Fvec)
        endif
        

        if(debug_option)then
            write(*,*)"Finish"
        endif
    end subroutine integrate_BC


    subroutine solver(CGdata)!線形ソルバ
        implicit none
        type(CGmethoddata) :: CGdata
        integer(4) :: i
        real(8) :: x,y,z

        if(debug_option)then
            write(*,*)"subroutine solver start"
        endif
        CGdata%alp = 0.0d0
        CGdata%pro = 0.0d0
        CGdata%rvec = 0.0d0

        CGdata%pro = matmul(CGdata%Kmat,CGdata%uvec)
        CGdata%rvec = CGdata%Fvec - CGdata%pro!r0の定義
        CGdata%pvec = CGdata%rvec!p0の定義
        CGdata%r0 = CGdata%rvec!r0の値は保存しておく

        do i = 1,CGdata%repu
            CGdata%pro = matmul(CGdata%Kmat,CGdata%pvec)!Kmatとp_(i-1)の積を求める

            ! x = dot_product(CGdata%rvec,CGdata%rvec)!αの分子
            ! y = dot_product(CGdata%pvec,CGdata%pro)!αの分母
            ! CGdata%alp = x/y!α(i)の計算
            CGdata%alp = dot_product(CGdata%rvec,CGdata%rvec)/dot_product(CGdata%pvec,CGdata%pro)!α(i)の計算

            CGdata%uvec = CGdata%uvec + CGdata%alp * CGdata%pvec!u_(i)の計算
            CGdata%rvec_save = CGdata%rvec!r_(i-1)を保存
            CGdata%rvec = CGdata%rvec - CGdata%alp * CGdata%pro

            x = sqrt(dot_product(CGdata%rvec,CGdata%rvec))
            y = sqrt(dot_product(CGdata%r0,CGdata%r0))
            z = dot_product(CGdata%rvec_save,CGdata%rvec_save)

            if((x/y) <= 1.0E-8)then !>繰り返し判定
                if(.not. dbg_preCG)then
                CGdata%uvec = matmul(CGdata%Pinv,CGdata%uvec)!対角スケーリングを利用する場合の処理
                endif
                iii = i
                exit
            endif

            CGdata%bet = x*x/z
            CGdata%pvec = CGdata%rvec + CGdata%bet*CGdata%pvec
        enddo

        ! write(*,*)i

        if(i == CGdata%repu)then
            write(*,*)"CG method error"
            stop
        endif

        if(debug_option)then
            write(*,*)"Finish"
        endif
    end subroutine solver

end module bc_and_solve