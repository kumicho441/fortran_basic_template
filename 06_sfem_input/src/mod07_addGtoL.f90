module addinguLuG
    use dbg_controller
    use variables
    use connectLG

    implicit none

    contains

    subroutine makeu_cal(Mdata_G,Mdata_L,Ddata,CGdata)!uLとuGを足し合わせる
        implicit none
        type(meshdata) :: Mdata_G,Mdata_L
        type(divisiondata) :: Ddata
        type(CGmethoddata) :: CGdata

        integer(4) :: target
        integer(4) :: i,c,Gelemid
        real(8) :: x_co,y_co,N(4),et_g,gz_g
        real(8) :: addvec(CGdata%SoM_L)
        if(.not. dbg_AdduG)then
        if(debug_option)then
            write(*,*)"subroutine makeu_cal start"
        endif


        !L領域の節点数分繰り返す
        ! do target =  1, Mdata_L%Nnode

        !     !L節点の座標を求める
        !     x_co = Mdata_L%co_node(1,target)
        !     y_co = Mdata_L%co_node(2,target)

        !     !節点が属するG要素を取得
        !     call mod04_get_Gmeshid(x_co,y_co,Ddata,Gelemid)
        !     ! write(*,*)target,Gelemid,et_g,gz_g

        !     !節点を表すet_G,gz_Gの値を求める
        !     call mod04_get_etgz(Mdata_G,Ddata,x_co,y_co,Gelemid,et_g,gz_g)!et,gzはG領域のものである点に注意

        !     ! write(*,*)target,Gelemid,et_g,gz_g

        !     !形状関数の値を算出する
        !     N(1) = (1 - gz_g) * (1 - et_g) / 4
        !     N(2) = (1 + gz_g) * (1 - et_g) / 4
        !     N(3) = (1 + gz_g) * (1 + et_g) / 4
        !     N(4) = (1 - gz_g) * (1 + et_g) / 4

        !     ! write(*,*)Gelemid
        !     !形状関数を用いてG節点の値を足しこむ

        !     c = CGdata%SoM_G !G領域の節点数
        !     do i = 1, 4
        !         !x座標の足しこみ
        !         CGdata%uvec(c + target*2 - 1) = &
        !         CGdata%uvec(c + target*2 - 1) + &
        !         N(i)*CGdata%uvec(Mdata_G%elem_node_id(i,Gelemid)*2-1)

        !         !y座標の足しこみ
        !         CGdata%uvec(c + target*2    ) = &
        !         CGdata%uvec(c + target*2    ) + &
        !         N(i)*CGdata%uvec(Mdata_G%elem_node_id(i,Gelemid)*2  )
        !     enddo

        !     ! do i = 1,Mdata_L%Nnode*2!デバッグ用
        !     !     CGdata%uvec(Mdata_G%Nnode*2 + i) = 0.0d0
        !     ! enddo

        !     ! do i = 1, 4
        !         ! CGdata%uvec(c + target*2 - 1) = &
        !         ! CGdata%uvec(c + target*2 - 1) + &
        !         ! N(1)*CGdata%uvec(Mdata_G%elem_node_id(1,Gelemid)*2 - 1) + N(2)*CGdata%uvec(Mdata_G%elem_node_id(2,Gelemid)*2 - 1) + &
        !         ! N(3)*CGdata%uvec(Mdata_G%elem_node_id(3,Gelemid)*2 - 1) + N(4)*CGdata%uvec(Mdata_G%elem_node_id(4,Gelemid)*2 - 1) 

        !         ! CGdata%uvec(c + target*2 ) = &
        !         ! CGdata%uvec(c + target*2 ) + &
        !         ! N(1)*CGdata%uvec(Mdata_G%elem_node_id(1,Gelemid)*2 ) + N(2)*CGdata%uvec(Mdata_G%elem_node_id(2,Gelemid)*2 ) + &
        !         ! N(3)*CGdata%uvec(Mdata_G%elem_node_id(3,Gelemid)*2 ) + N(4)*CGdata%uvec(Mdata_G%elem_node_id(4,Gelemid)*2 ) 
        !     !  enddo
        ! enddo

        
        !L領域の節点数分繰り返す
        addvec = 0.0d0
        do target =  1, Mdata_L%Nnode

            !L節点の座標を求める
            x_co = Mdata_L%co_node(1,target)
            y_co = Mdata_L%co_node(2,target)

            !節点が属するG要素を取得
            call mod04_get_Gmeshid(x_co,y_co,Ddata,Gelemid)
            ! write(*,*)target,Gelemid,et_g,gz_g

            !節点を表すet_G,gz_Gの値を求める
            call mod04_get_etgz(Mdata_G,Ddata,x_co,y_co,Gelemid,et_g,gz_g)!et,gzはG領域のものである点に注意

            !デバッグ
            ! if(abs(et_g)>1.0 .or. abs(gz_g)>1.0)then
            !     write(*,*)et_g,gz_g
            ! endif

            ! write(*,*)target,Gelemid,et_g,gz_g

            !形状関数の値を算出する
            N(1) = (1 - gz_g) * (1 - et_g) / 4
            N(2) = (1 + gz_g) * (1 - et_g) / 4
            N(3) = (1 + gz_g) * (1 + et_g) / 4
            N(4) = (1 - gz_g) * (1 + et_g) / 4

            ! write(*,*)Gelemid
            !形状関数を用いてG節点の値を足しこむ


            do i = 1, 4
                !x座標の足しこみ
                addvec(target*2-1) = addvec(target*2-1) + &
                N(i)*CGdata%uvec(Mdata_G%elem_node_id(i,Gelemid)*2-1)

                !y座標の足しこみ
                addvec(target*2) = addvec(target*2) + &
                N(i)*CGdata%uvec(Mdata_G%elem_node_id(i,Gelemid)*2  )
            enddo


        enddo

        c = CGdata%SoM_G !G領域の節点数
        do i = 1,CGdata%SoM_L
            CGdata%uvec(c+i) = CGdata%uvec(c+i) + addvec(i)
        enddo



        !デバッグ
        ! x_co = Mdata_L%co_node(1,226)
        ! y_co = Mdata_L%co_node(2,226)
        ! call mod04_get_Gmeshid(x_co,y_co,Ddata,Gelemid)
        ! ! write(*,*)target,Gelemid,et_g,gz_g
        ! !節点を表すet_G,gz_Gの値を求める
        ! call mod04_get_etgz(Mdata_G,Ddata,x_co,y_co,Gelemid,et_g,gz_g)!et,gzはG領域のものである点に注意
        ! write(*,*)Gelemid,et_g,gz_g


        if(debug_option)then
            write(*,*)"Finish"
        endif

        endif
    end subroutine makeu_cal
end module addinguLuG