module connectLG
    use dbg_controller
    use variables
    use mesh_and_vis
    implicit none

    contains
    

    ! subroutine get_co(Mdata,elemid,et,gz,x_co,y_co)!要素番号,et,gzから座標を出力
    ! implicit none
    ! type(meshdata) :: Mdata
    ! integer(4),intent(in) :: elemid
    ! real(8),intent(in) :: et,gz

    ! integer(4) :: i,nodeid(4)
    ! real(8) :: x_co,y_co,N(4)

    ! !要素に対応する節点番号
    ! do i = 1,4
    ! nodeid(i) = Mdata%elem_node_id(i,elemid)
    ! enddo

    ! !要素の形状関数
    ! N(1) = (1-gz)*(1-et)/4
    ! N(2) = (1+gz)*(1-et)/4
    ! N(3) = (1+gz)*(1+et)/4
    ! N(4) = (1-gz)*(1+et)/4

    ! x_co = 0.0d0
    ! y_co = 0.0d0

    ! !求めるx,y座標の算出
    ! do i = 1,4
    !     x_co = x_co + N(i)*Mdata%co_node(1,nodeid(i))
    !     y_co = y_co + N(i)*Mdata%co_node(2,nodeid(i))
    ! enddo

    ! end subroutine get_co

    ! subroutine get_Gmeshid(x_co,y_co,Ddata,Gelemid)!座標に対応するGメッシュ番号を出力
    !     implicit none
    !     type(divisiondata),intent(in) ::Ddata
    !     real(8),intent(in) :: x_co,y_co
    !     integer(4) :: Gelemid,xid,yid
    !     ! real(8) x_coc,y_coc
    !     xid = int(x_co / Ddata%leng_Gmesh) 
    !     yid = int(y_co / Ddata%leng_Gmesh) 

    !     Gelemid = (yid*Ddata%diviG) + xid + 1
    !     ! !デバッグ用
    !     ! if(Gelemid > 100)then
    !     !     Gelemid = 100
    !     ! endif

    ! end subroutine get_Gmeshid

    ! subroutine get_etgz(Mdata_G,Ddata,x_co,y_co,Gelemid,et_g,gz_g)!座標とG要素番号から、形状関数のet,gzを逆算
    !             implicit none
    !     type(meshdata) :: Mdata_G!Gdata
    !     type(divisiondata) :: Ddata
    !     real(8),intent(in) :: x_co,y_co
    !     integer(4),intent(in) :: Gelemid

    !     real(8) :: x0,y0,et_g,gz_g

    !     x0 = Mdata_G%co_node(1,Mdata_G%elem_node_id(1,Gelemid))
    !     gz_g = 2*(x_co - x0)/Ddata%leng_Gmesh - 1

    !     y0 = Mdata_G%co_node(2,Mdata_G%elem_node_id(1,Gelemid))
    !     et_g = 2*(y_co - y0)/Ddata%leng_Gmesh - 1

    ! end subroutine get_etgz

    subroutine mod04_get_co(Mdata,elemid,et,gz,x_co,y_co)!要素番号,et,gzから座標を出力
    implicit none
    type(meshdata) :: Mdata
    integer(4),intent(in) :: elemid
    real(8),intent(in) :: et,gz

    integer(4) :: i,nodeid(4)
    real(8) :: N(4)
    real(8),intent(out) :: x_co,y_co

    !要素に対応する節点番号
    do i = 1,4
    nodeid(i) = Mdata%elem_node_id(i,elemid)
    enddo

    !要素の形状関数
    N(1) = (1-gz)*(1-et)/4
    N(2) = (1+gz)*(1-et)/4
    N(3) = (1+gz)*(1+et)/4
    N(4) = (1-gz)*(1+et)/4

    !求めるx,y座標の算出
    x_co=0.0d0; y_co = 0.0d0

    do i = 1,4
    x_co = x_co + N(i) * Mdata%co_node(1,nodeid(i))
    y_co = y_co + N(i) * Mdata%co_node(2,nodeid(i))
    enddo
    end subroutine mod04_get_co

    subroutine mod04_get_Gmeshid(x_co,y_co,Ddata,Gelemid)!座標に対応するGメッシュ番号を出力
        implicit none

        type(divisiondata),intent(in) ::Ddata
        real(8),intent(in) :: x_co,y_co
        integer(4) :: xid,yid
        integer(4),intent(out) :: Gelemid

        xid = int(x_co/Ddata%leng_Gmesh)
        yid = int(y_co/Ddata%leng_Gmesh)

        Gelemid = yid*Ddata%diviG + xid + 1
    end subroutine mod04_get_Gmeshid

    subroutine mod04_get_etgz(Mdata_G,Ddata,x_co,y_co,Gelemid,et_g,gz_g)!座標とG要素番号から、形状関数のet,gzを逆算
        implicit none
        type(meshdata) :: Mdata_G!Gdata
        type(divisiondata) :: Ddata
        real(8),intent(in) :: x_co,y_co
        integer(4),intent(in) :: Gelemid

        real(8) :: x0,y0
        real(8),intent(out) :: et_g,gz_g

        x0 = Mdata_G%co_node(1,Mdata_G%elem_node_id(1,Gelemid))
        y0 = Mdata_G%co_node(2,Mdata_G%elem_node_id(1,Gelemid))

        gz_g = 2*(x_co - x0)/Ddata%leng_Gmesh - 1
        et_g = 2*(y_co - y0)/Ddata%leng_Gmesh - 1


    end subroutine mod04_get_etgz

    !細分化するG,Lメッシュを決める
    subroutine get_subdivision()
        implicit none 
        integer(4) :: i,j,Gid(4)
        real(8) :: radius,x_co1,x_co2,y_co1,y_co2,r1,r2
        ! if(debug_option)then
        !     write(*,*)"Subroutine get_subdivision start"
        ! endif

        if(.not. dbg_division)then

        !G要素が境界をまたいでいるか判定
        do i = 1, Gdata%Nelem
            radius = divi%leng_Garea*area_ratio
            x_co1 = Gdata%co_node(1,Gdata%elem_node_id(1,i))
            y_co1 = Gdata%co_node(2,Gdata%elem_node_id(1,i))
            r1 = sqrt(x_co1**2 + y_co1**2)

            x_co2 = Gdata%co_node(1,Gdata%elem_node_id(3,i))
            y_co2 = Gdata%co_node(2,Gdata%elem_node_id(3,i))
            r2 = sqrt(x_co2**2 + y_co2**2)

            ! if(r1 < radius .and. r2 >= radius)then
            if(r1 <= radius .and. r2 >= radius)then
                Gdata%complex(i) = .true.
            else
                Gdata%complex(i) = .false.
            endif
        enddo

        ! !デバッグ
        ! Gdata%complex = .false.

        !L要素がG要素をまたいでいるか判定
        do i = 1, Ldata%Nelem
            do j = 1,4
                call mod04_get_Gmeshid(Ldata%co_node(1,Ldata%elem_node_id(j,i)),&
                Ldata%co_node(2,Ldata%elem_node_id(j,i)),divi,Gid(j))
            enddo

            if(Gid(1) == Gid(2) .and. Gid(2) == Gid(3) .and. Gid(3) == Gid(4))then
                Ldata%complex(i) = .false.
            else
                Ldata%complex(i) = .true.
            endif

            ! if(Ldata%complex(i))then
            !     write(*,*)i,Gid(1),Gid(2),Gid(3),Gid(4)
            ! endif
        enddo

        !デバッグ
        ! Ldata%complex = .false.

        ! if(debug_option)then
        !     write(*,*)"Finish"
        ! endif
        ! do i = 1,Gdata%Nelem
        !     if(Gdata%complex(i))then
        !         write(*,*)"Gid =",i
        !     endif
        ! do i = 1,Ldata%Nelem
        !     if(Ldata%complex(i))then
        !         write(*,*)"Gid =",Ldata%nnode
        !     endif
        ! enddo
        else
            Ldata%complex = .false.
            Gdata%complex = .false.
        endif
    end subroutine get_subdivision

        !積分領域の細分化について
    subroutine get_integration_point(Npoint,co_ip_subd,wip_subd)
        ! call get_integration_point(Sdata%Nip_subd,Sdata%subd,Sdata%wip_subd)!積分領域の細分化に用いる変数を整理
        implicit none
        integer(4) :: Npoint,i,j,k,in
        real(8) :: len1,len2,len3,len4,r(2)
        real(8),allocatable :: co_ip_subd(:,:)
        real(8),allocatable :: wip_subd

        if(allocated(co_ip_subd)) deallocate(co_ip_subd)
        ! if(allocated(wip_subd)) deallocate(wip_subd)

        Npoint = smd%n_rec*smd%n_rec*4!積分点数の定義
        allocate(co_ip_subd(2,Npoint), source = 0.0d0)!積分点座標の初期化


        in = 0!inの初期化
        r(1) = -0.57735027
        r(2) =  0.57735027
        len1 = 1.0d0/dble(smd%n_rec)
        len2 = r(1)/dble(smd%n_rec)
        len3 = r(2)/dble(smd%n_rec)

        do i = 1, smd%n_rec!何列目の要素に注目しているか表す
            do j = 1, smd%n_rec!何行目の要素に注目しているか表す
                do k = 1, 4!細分化された要素内で何番目の積分点に注目しているか表す
                    in = in + 1!要素内での積分点の番号を表す

                    ! co_ip_subd(1,in) = (dble(i) - 1)*2.0d0*len1 + len2 - 1.0d0 + len1
                    ! co_ip_subd(2,in) = (dble(j) - 1)*2.0d0*len1 + len3 - 1.0d0 + len1

                    if(k == 1)then
                        co_ip_subd(1,in) = (dble(i) - 1)*2.0d0*len1 + len1 + len2 - 1.0d0
                        co_ip_subd(2,in) = (dble(j) - 1)*2.0d0*len1 + len1 + len2 - 1.0d0
                    endif
                    if(k == 2)then
                        co_ip_subd(1,in) = (dble(i) - 1)*2.0d0*len1 + len1 + len2 - 1.0d0
                        co_ip_subd(2,in) = (dble(j) - 1)*2.0d0*len1 + len1 + len3 - 1.0d0
                    endif
                    if(k == 3)then
                        co_ip_subd(1,in) = (dble(i) - 1)*2.0d0*len1 + len1 + len3 - 1.0d0
                        co_ip_subd(2,in) = (dble(j) - 1)*2.0d0*len1 + len1 + len3 - 1.0d0
                    endif
                    if(k == 4)then
                        co_ip_subd(1,in) = (dble(i) - 1)*2.0d0*len1 + len1 + len3 - 1.0d0
                        co_ip_subd(2,in) = (dble(j) - 1)*2.0d0*len1 + len1 + len2 - 1.0d0
                    endif

                enddo
            enddo
        enddo

        wip_subd = 1.0d0/(smd%n_rec*smd%n_rec)

        ! do i = 1, Npoint
        !     write(*,*)co_ip_subd(1,i),co_ip_subd(2,i),wip_subd
        ! enddo

    end subroutine get_integration_point
end module