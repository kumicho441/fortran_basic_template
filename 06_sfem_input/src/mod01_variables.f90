module variables!変数の定義を行う
    implicit none
    
    ! real(8) :: a,b!定義した変数は他のプログラムで使える
    integer(4) :: dim = 2,numG=10, numL=11, numbc=12,numload = 13, numVTK = 999,iii=1
    real(8) :: young_1,young_2,poisson_1,poisson_2,area_ratio = 0.25,leng,pi = 3.1415926535897932
    real(8) :: lengg,lengl,ram1,ram2,mu1,mu2,alpha!理論解の算出に利用する変数
    ! real(8) :: ip(3),wip(3,3)
    ! integer(4) :: i_ip,j_ip



    type :: Smatdata!要素剛性行列生成に用いる変数の定義
        real(8) :: Dmat(3,3),Jmat(2,2),Jinv(2,2),detJ,Bmat(3,8),BTmat(8,3),Kemat(8,8),KeGL(8,8)
        real(8) :: mat38(3,8),mat24(2,4),mat42_m(4,2),mat42_u(4,2),mat88(8,8),BonGT(8,3),mat24_m(2,4),mat24_u(2,4)
        real(8),allocatable :: KGL(:,:)
        !通常の要素の積分点数
        integer(4) :: Nip=4!4以外には未対応
        
        real(8),allocatable :: co_ip(:,:)
        real(8) :: wip = 1.0d0
        !細分化(subdivision)する際の積分点についての変数
        ! integer(4) :: Nip_subd= 10
        
        ! real(8),allocatable :: co_ip_subd(:),wip_subd(:)

        !細分化する際の一辺の分割数
        integer(4) :: n_rec = 10
        integer(4) :: Nip_subd
        real(8),allocatable :: co_ip_subd(:,:),wip_subd
    end type Smatdata


    type :: meshdata!各メッシュで用いる変数を定義

        integer(4) :: Nelem,Nnode
        integer(4),allocatable :: elem_node_id(:,:)!numbersの代わり
        real(8),allocatable :: co_node(:,:)!行数、列数の順序に注意
        real(8),allocatable :: Kmat(:,:),Bmat_m(:,:,:)!Bmat_m{et,gz,対応する要素番号}
        logical,allocatable :: complex(:)!内外判定を行う
    end type meshdata

    type :: divisiondata

    !G領域
    real(8) :: leng_Garea,leng_Gmesh
    integer(4) :: diviG

    !L領域
    integer(4) :: diviL_q,diviL_c!それぞれ四角形,円孔,θ方向分割数
    real(8) :: leng_Larea,leng_Lmesh!デバッグ用に設定
    integer(4) :: diviL

    end type divisiondata

    type CGmethoddata

    integer(4) :: SoM,SoM_G,SoM_L,repu = 1000000!反復回数
    real(8),allocatable :: Kmat(:,:),Fvec(:),uvec(:),Pmat(:,:),PTmat(:,:),Pinv(:,:)
    real(8),allocatable :: rvec(:),r0(:),rvec_save(:),pvec(:),alp(:),bet(:),pro(:)
    real(8) :: load,Force!引張応力
    end type CGmethoddata

    type evaluationdata

    real(8) :: L2,L2_nu,L2_de,dif_nu,dif_de!nuは分子,deは分母を意味する
    real(8),allocatable :: elel2(:),uvec_th(:)
    end type evaluationdata



    type(Smatdata) :: smd
    type(meshdata) :: Gdata,Ldata
    type(divisiondata) :: divi
    type(CGmethoddata) :: CGd
    type(evaluationdata) :: evd

    contains

    ! subroutine debugger()
    !     ! a = 1.0
    !     ! b = 2.0
    !     debug_option= .true.
    ! end subroutine debugger

end module variables