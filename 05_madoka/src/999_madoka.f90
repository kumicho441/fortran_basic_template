subroutine help_makeKe(poisson,young,et,gz,time,numbers,Noelem,Nonode,co_node,Kemat) 
    !>help_makeK(ポアソン比,ヤング率,η,ξ,何個目の要素か,変数numbers,SoM,要素数,節点数,節点座標,(出力)Kmat)

    ! implicit none
    ! integer,intent(in) :: SoM,time,Noelem,numbers(Noelem,4)
    ! real(8),intent(in) :: poisson,young,et,gz!> [修正済み]行列の大きさが固定されていることに注意
    ! real(8) :: Kemat(8,8),Dmat(3,3),Jmat(2,2),detJ,Jinv(2,2),Bmat(3,8),TraB(8,3),mat38(3,8)
    ! integer :: i,j,k,l
    ! real(8) :: x

    implicit none
    ! Integer variables with intent(in)
    integer, intent(in) :: time, Noelem
    integer, intent(in) :: numbers(Noelem, 4)

    ! Real(8) variables with intent(in)
    real(8), intent(in) :: poisson, young, et, gz,co_node(Nonode,3)

    ! Real(8) variables for matrices and other computations
    real(8) :: Kemat(8, 8), Dmat(3, 3), Jmat(2, 2), Jinv(2, 2),detJ

    real(8) :: Bmat(3, 8), TraB(8, 3), mat38(3, 8)
    real(8) :: x

    ! Allocatable real(8) array for node coordinates
    ! real(8), allocatable :: co_node1(:,:)

    real(8) :: mat24_1(2, 4), copiedco(4, 2)
    real(8) :: mat42_1(4,2),mat42_2(4,2) !>それぞれNをx,yやη,ξで微分したものを表す

    ! Integer variables for loop indices and node counts
    integer :: i, j, k,Nonode, numnode


    Kemat = 0.0
    Dmat = 0; Jmat = 0; Jinv = 0; Bmat = 0; TraB = 0; mat38 = 0;detJ = 0 !>初期化
 !> Dマトリックス導出
    do i =1,2
        Dmat(i,i) = 1/(1-poisson)
        Dmat(i,3) = 0
        Dmat(3,i) = 0
    enddo

    Dmat(1,2) = poisson/(1-poisson)
    Dmat(2,1) = poisson/(1-poisson)
    Dmat(3,3) = 0.5

    x = young/(1+poisson)
    do i = 1,3
        do j = 1,3
            Dmat(i,j) = x*Dmat(i,j)
        enddo
    enddo

 !> Jマトリックス導出
    Jmat = 0.0d0
    numnode = 11
    mat24_1 = 0
    copiedco = 0

    !> ここから行列に値を代入する
    mat24_1(1,1) = -(1-et)/4
    mat24_1(1,2) = (1-et)/4
    mat24_1(1,3) = (1+et)/4
    mat24_1(1,4) = -(1+et)/4
    mat24_1(2,1) = -(1-gz)/4
    mat24_1(2,2) = -(1+gz)/4
    mat24_1(2,3) = (1+gz)/4
    mat24_1(2,4) = (1-gz)/4

    do i = 1,4 !> 注目している要素に対応する節点座標を代入 この時点で値が対応していない
        j = numbers(time,i)
        ! write(*,*)j
        copiedco(i,1) = co_node(j,1)
        copiedco(i,2) = co_node(j,2) 
    enddo 
    ! write(*,*)"co="

    ! do i = 1,4
    !     write(*,*)copiedco(i,1),copiedco(i,2)
    ! enddo
    do k = 1,2
        do j = 1, 2
             do i = 1, 4
                Jmat(k,j) = Jmat(k,j) + mat24_1(k,i)*copiedco(i,j)
            end do
        end do
    enddo
    ! write(*,*)"Jmat =",Jmat

 !detJの計算
    detJ= Jmat(1,1)*Jmat(2,2) - Jmat(1,2)*Jmat(2,1)

 !Jマトリックスの逆行列を求める

    Jinv(1,1) = Jmat(2,2)/detJ
    Jinv(1,2) = -Jmat(1,2)/detJ
    Jinv(2,1) = -Jmat(2,1)/detJ
    Jinv(2,2) = Jmat(1,1)/detJ

 !Bマトリックス導出

    mat42_2(1,1) = -(1-et)/4
    mat42_2(1,2) = -(1-gz)/4
    mat42_2(2,1) = (1-et)/4
    mat42_2(2,2) = -(1+gz)/4
    mat42_2(3,1) = (1+et)/4
    mat42_2(3,2) = (1+gz)/4
    mat42_2(4,1) = -(1+et)/4
    mat42_2(4,2) = (1-gz)/4

    i = 0
    !> ここからNのx,y微分を求める
    do i = 1, 4 !>繰り返し回数は節点数と関係ないことに注意
        mat42_1(i,1) =  Jinv(1,1)*mat42_2(i,1) + Jinv(1,2)*mat42_2(i,2)
        mat42_1(i,2) =  Jinv(2,1)*mat42_2(i,1) + Jinv(2,2)*mat42_2(i,2)
    enddo

    !> ここから具体的な値を計算・代入する
    Bmat = 0
    do i = 1,4!>繰り返し数が固定されていることに注意
        !>write(*,*)i
        Bmat(1,(2*i-1)) = mat42_1(i,1)
        Bmat(3,2*i) = mat42_1(i,1)
        Bmat(2,2*i) = mat42_1(i,2)
        Bmat(3,(2*i-1)) = mat42_1(i,2)
    enddo
 !Bマトリックスの逆行列を求める
    do i=1,3
        do j=1,8
            traB(j,i) = Bmat(i,j)
        enddo
    enddo

 !Kマトリックスの具体的な値を求める
    do j = 1,8

    do i = 1,3 !>繰り返しの回数が固定されていることに注意
            mat38(1,j) = mat38(1,j) + Dmat(1,i)*Bmat(i,j)
            mat38(2,j) = mat38(2,j) + Dmat(2,i)*Bmat(i,j)
            mat38(3,j) = mat38(3,j) + Dmat(3,i)*Bmat(i,j)
    enddo
    enddo

    do k = 1,8
    do j = 1,8
        do i = 1,3 !>繰り返しの回数が固定されていることに注意
            Kemat(k,j) = Kemat(k,j) + TraB(k,i)*mat38(i,j)         
        enddo
    enddo
    enddo

    do i =1,8
    do j = 1,8
        Kemat(i,j) = Kemat(i,j)*detJ
    enddo
    enddo

    ! write(*,*)Kemat
    ! write(*,*)"detJ=",detJ

    !>求めた要素剛性マトリックスを剛性マトリックスに対応付ける。3次元未対応
    ! do i = 1,4
    !     do j =1,4
    !         k = numbers(time,i); l = numbers(time,j)
    !         Kmat(2*k-1,2*l-1) = Kmat(2*k-1,2*l-1) + Kemat(2*i-1,2*j-1)
    !         Kmat(2*k-1,2*l) = Kmat(2*k-1,2*l) + Kemat(2*i-1,2*j)
    !         Kmat(2*k,2*l-1) = Kmat(2*k,2*l-1) + Kemat(2*i,2*j-1)
    !         Kmat(2*k,2*l) = Kmat(2*k,2*l) + Kemat(2*i,2*j) 
    !     enddo
    ! enddo

end subroutine help_makeKe


subroutine makeK(poisson,young,time,numbers,SoM,Noelem,Nonode,co_node,Kmat) 
        !>makeK(ポアソン比,ヤング率,何個目の要素か,変数numbers,SoM,要素数,節点数,節点座標,Kマトリックス(出力))
    implicit none
    real(8),intent(in) :: poisson,young,co_node(Nonode,3)
    integer,intent(in) :: SoM,time,Noelem,numbers(Noelem,4),Nonode
    integer :: i,j,k,l
    real(8) :: Kemat(8,8),Kmat(SoM,SoM),mat1(8,8),x !>[修正済み]行列の大きさが固定されていることに注意


    x = 0.57735

    mat1 = 0.0
    Kemat = 0.0
    call help_makeKe(poisson,young,-x,-x,time,numbers,Noelem,Nonode,co_node,mat1) !>ガウス積分を計算するために値を代入していく

    do i = 1,8
        do j = 1,8
        Kemat(i,j) = Kemat(i,j) + mat1(i,j)
        enddo
    enddo

    mat1 = 0.0
    call help_makeKe(poisson,young,x,-x,time,numbers,Noelem,Nonode,co_node,mat1)
    do i = 1,8
        do j = 1,8
            Kemat(i,j) = Kemat(i,j) + mat1(i,j)
        enddo
    enddo

    mat1 = 0.0
    call help_makeKe(poisson,young,x,x,time,numbers,Noelem,Nonode,co_node,mat1)
        do i = 1,8
            do j = 1,8
                Kemat(i,j) = Kemat(i,j) + mat1(i,j)
            enddo
    enddo

    mat1 = 0.0
    call help_makeKe(poisson,young,-x,x,time,numbers,Noelem,Nonode,co_node,mat1)
    do i = 1,8
        do j = 1,8
            Kemat(i,j) = Kemat(i,j) + mat1(i,j)
        enddo
    enddo

    ! do i = 1,8
    ! write(*,*)"mat1(",i",8)=",mat1(i,8)
    ! enddo
    ! write(*,*)"Kemat="
    ! write(*,*)Kemat

    do i = 1,4
        do j =1,4
            k = numbers(time,i); l = numbers(time,j) !>k,lは注目している要素の節点番号を表す
            ! write(*,*)"k,l=",k,l
            Kmat(2*k-1,2*l-1) = Kmat(2*k-1,2*l-1) + Kemat(2*i-1,2*j-1)
            Kmat(2*k-1,2*l) = Kmat(2*k-1,2*l) + Kemat(2*i-1,2*j)
            Kmat(2*k,2*l-1) = Kmat(2*k,2*l-1) + Kemat(2*i,2*j-1)
            Kmat(2*k,2*l) = Kmat(2*k,2*l) + Kemat(2*i,2*j) 

        enddo
    enddo
    ! write(*,*)"Kmat=",Kmat

end subroutine makeK

subroutine product(mat,vec,SoM,outcome) !>product(mat,vec,SoM,outcome)
    implicit none
    
        integer :: i,j
    
        integer,intent(in) :: SoM
        real(8),intent(in) :: mat(SoM,SoM),vec(SoM)
    
        real(8) :: outcome(SoM)
    
        do i = 1, SoM
            outcome(i) = 0.0 ! ベクトルyの要素を初期化
            do j = 1, SoM
                outcome(i) = outcome(i) + mat(i,j) * vec(j)
            end do
        end do
end subroutine product

subroutine help_makeL2(gz, et, time, Nonode, Noelem, numbers, co_node, SoM, d_cal, stress, radius, poisson, young, Diff1, Diff2)
    implicit none

    real(8), intent(in) :: et, gz, stress, radius, poisson, young
    integer, intent(in) :: time, Nonode, Noelem, SoM
    real(8), intent(in) :: co_node(Nonode, 3), d_cal(SoM)
    integer, intent(in) :: numbers(Noelem, 4)
    real(8), intent(out) :: Diff1, Diff2

    integer :: i, j, k
    real(8) :: mat24(2, 4), copiedco(4, 2), Jmat(2, 2), detJ
    real(8) :: ux_cal, uy_cal, ux_th, uy_th
    real(8) :: x, y, rrr, costh, cos3th, sinth, sin3th
    real(8) :: shapeF(4), co_integral(2)

    ! 初期化
    mat24 = 0.0d0
    copiedco = 0.0d0
    Jmat = 0.0d0
    ux_cal = 0.0d0; uy_cal = 0.0d0; ux_th = 0.0d0; uy_th = 0.0d0; detJ = 0.0d0
    Diff1 = 0.0d0; Diff2 = 0.0d0

    ! ここから行列に値を代入する
    mat24(1, 1) = -(1 - et) / 4
    mat24(1, 2) = (1 - et) / 4
    mat24(1, 3) = (1 + et) / 4
    mat24(1, 4) = -(1 + et) / 4
    mat24(2, 1) = -(1 - gz) / 4
    mat24(2, 2) = -(1 + gz) / 4
    mat24(2, 3) = (1 + gz) / 4
    mat24(2, 4) = (1 - gz) / 4

    do i = 1, 4
        j = numbers(time, i)
        copiedco(i, 1) = co_node(j, 1)
        copiedco(i, 2) = co_node(j, 2)
    end do

    ! Jmatの計算
    do k = 1, 2
        do j = 1, 2
            do i = 1, 4
                Jmat(k, j) = Jmat(k, j) + mat24(k, i) * copiedco(i, j)
            end do
        end do
    end do
    detJ = Jmat(1, 1) * Jmat(2, 2) - Jmat(1, 2) * Jmat(2, 1)

    ! shapeFの計算
    shapeF(1) = (1 - gz) * (1 - et) / 4
    shapeF(2) = (1 + gz) * (1 - et) / 4
    shapeF(3) = (1 + gz) * (1 + et) / 4
    shapeF(4) = (1 - gz) * (1 + et) / 4

    ! co_integralの計算
    co_integral = 0.0d0
    do i = 1, 2
        do j = 1, 4
            co_integral(i) = co_integral(i) + copiedco(j, i) * shapeF(j)
        end do
    end do

    ! 理論解の計算
    x = atan(co_integral(2) / co_integral(1))
    costh = cos(x)
    cos3th = cos(3 * x)
    sinth = sin(x)
    sin3th = sin(3 * x)

    x = (3 - poisson) / (1 + poisson) ! κを記録
    y = young / (2 * (1 + poisson))   ! μを記録
    rrr = sqrt(co_integral(1) ** 2 + co_integral(2) ** 2)

    ux_th = (stress * radius / (8 * y)) * (rrr / radius * (x + 1) * costh + &
               2 * radius / rrr * ((1 + x) * costh + cos3th) - &
               2 * radius ** 3 / (rrr ** 3) * cos3th)

    uy_th = (stress * radius / (8 * y)) * (rrr / radius * (x - 3) * sinth + &
               2 * radius / rrr * ((1 - x) * sinth + sin3th) - &
               2 * radius ** 3 / (rrr ** 3) * sin3th)

    ! 変位の計算
            do i = 1, 4
                ux_cal = ux_cal + shapeF(i) * (d_cal(numbers(time,i) * 2 - 1))
                uy_cal = uy_cal + shapeF(i) * (d_cal(numbers(time,i) * 2))
            enddo

    ! Diff1とDiff2の計算
    Diff1 = ((ux_cal - ux_th) ** 2 + (uy_cal - uy_th) ** 2) * detJ
    Diff2 = (ux_th ** 2 + uy_th ** 2) * detJ

end subroutine help_makeL2

subroutine makeL2(time,Nonode,Noelem,numbers,co_node,stress,radius,poisson,young,SoM,d_cal,elel2,L2_nu,L2_de)
    !>ある要素のL2誤差評価を算出する
    implicit none


    integer,intent(in) :: time,Nonode,Noelem,SoM
    real(8), intent(in) :: co_node(Nonode,3),d_cal(SoM),stress,radius,poisson,young
    integer, intent(in) :: numbers(Noelem, 4)

    real(8) :: elel2(time),x,y,z,Diff1,Diff2,L2_nu,L2_de

    elel2(time) =0;z = 0;y = 0

    x = 0.57735
    call help_makeL2(-x, -x, time, Nonode, Noelem, numbers, co_node, SoM, d_cal, stress, radius, poisson, young, Diff1, Diff2)
    z = z + Diff1!>z,yは要素ごとのL2誤差評価に使う
    y = y + Diff2
    L2_nu = L2_nu + Diff1!>Diff1,Diff2は円孔平板全体のL2誤差評価に使う
    L2_de = L2_de + Diff2

    call help_makeL2( x, -x, time, Nonode, Noelem, numbers, co_node, SoM, d_cal, stress, radius, poisson, young, Diff1, Diff2)
    z = z + Diff1
    y = y + Diff2
    L2_nu = L2_nu + Diff1
    L2_de = L2_de + Diff2

    call help_makeL2( x,  x, time, Nonode, Noelem, numbers, co_node, SoM, d_cal, stress, radius, poisson, young, Diff1, Diff2)
    z = z + Diff1
    y = y + Diff2
    L2_nu = L2_nu + Diff1
    L2_de = L2_de + Diff2

    call help_makeL2(-x,  x, time, Nonode, Noelem, numbers, co_node, SoM, d_cal, stress, radius, poisson, young, Diff1, Diff2)
    z = z + Diff1
    y = y + Diff2
    L2_nu = L2_nu + Diff1
    L2_de = L2_de + Diff2

    ! elel2(time) = sqrt(z/y)
    elel2(time) = ABS(z/y)
    ! write(*,*)elel2(time)

end subroutine makeL2

program main
    implicit none
 !変数の定義
    ! 使い捨て変数
    integer :: a, b, i, j, k, n, time 
    real(8) :: x, y, z

    ! メッシュ定義に必要な変数
    integer ::  Npelem,divir,divith
    real(8) :: lengx, lengy,radius,theta,deltar
    real(8),allocatable :: meshfac(:),rrr(:)

    ! 支配方程式導出に必要な変数
    integer :: SoM
    real(8), allocatable :: d_cal(:), d_th(:), Force(:)

    ! 線形ソルバに使う変数
    real(8), allocatable :: bvec(:), Amat(:,:), xvec(:), rvec(:), r0(:), rsavevec(:), pvec(:), alpha(:), beta(:), pro(:) 

    ! ファイル出力に必要な変数
    integer :: numVTK_cal, Nonode, Noelem, numVTK_th
    integer, allocatable :: numbers(:,:)
    real(8), allocatable :: co_node(:,:),theorim(:,:)

    character(256) :: filename

    ! 定数の定義に使う変数
    real(8) :: young, poisson ,stress! ヤング率、ポアソン比、行列、ベクトル
    real(8), allocatable :: Kmat(:,:)

    ! 誤差評価に利用する変数
    real(8) :: L2,L2_nu,L2_de!>nuは分子,deは分母を表す
    real(8),allocatable :: elel2(:)

    !> 計算時間測定に使う変数
    real(8) :: start_time,file_time,makeK_time,makeeq_time,calc_time,end_time


    ! その他の変数
    integer :: numinput, numnode, numelem, numbc, numload, Nodim, Nobc, Noload
    real(8) :: pi = 3.14159265358979323846
    
    !> ファイル番号
    numinput = 10; numnode = 11; numelem = 12; numbc = 13; numload = 14; numVTK_cal = 100; numVTK_th = 101

    call CPU_TIME(start_time)
 !メッシュ生成
    !>メッシュのx,y方向長さとx,y方向分割数を入力する
    write(*,*)"辺の長さ 円孔半径 r方向分割数 Θ方向分割数(偶数) を入力"
    read *,lengx,radius,divir,divith
    lengy = lengx !>正方形平板を仮定

    if(lengx <= radius .or. lengx <= radius)then !>半径が大きすぎるときのエラー
        write(*,*)"error 辺の長さに対して半径が大きすぎます"
        stop
    endif

    if (mod(divith, 2) /= 0) then
        write(*,*)"erorr Θ方向分割数は偶数を入力してください"
        stop
    endif

    Noelem =     divir*divith
    Nonode = (divir+1)*(divith+1)
    allocate(co_node(Nonode,3))


    !>ここから各節点の座標を求める（極座標系）

    !>円孔近傍が正方形になるメッシュ比調整
    allocate(meshfac(divir))
    meshfac = 1.0d0
    ! do i = 1, int(divir/3)
    !     meshfac(i) = (divir*radius*pi)/(2*divith*lengx) 
    !     meshfac(divir+1 - i) = 2-(divir*radius*pi)/(2*divith*lengx)
    ! enddo

    allocate(rrr(divir + 1))

    do j = 1, divith + 1

        theta = pi*(j-1)/2/divith

        if (theta <= 0.7853981633974482) then
            deltar = (lengx * sqrt(1 + tan(theta)**2) - radius) / divir !> 0-45度
            rrr(1) = radius
            do i = 2, divir+1
                rrr(i) = rrr(i-1) + deltar * meshfac(i-1)
            enddo
            
            ! r = radius
            co_node(1 + (j-1)*(divir+1),1) = rrr(1) * cos(theta)
            co_node(1 + (j-1)*(divir+1),2) = rrr(1) * sin(theta)
    
            do i = 2, divir + 1
                ! r = r + deltar * meshfac(i-1)
                co_node(i + (j-1)*(divir+1),1) = rrr(i) * cos(theta)
                co_node(i + (j-1)*(divir+1),2) = rrr(i) * sin(theta)
            enddo
        else
            ! deltar = (lengy * sqrt(1 + 1 / (tan(theta)**2)) - radius) / divir !> 45-90度

            do i = 1,divir+1
                co_node(i + (j-1)*(divir+1),1) = co_node(i + divith*(divir+1) - (j-1)*(divir+1),2) !>対称性を利用して座標を定義
                co_node(i + (j-1)*(divir+1),2) = co_node(i + divith*(divir+1) - (j-1)*(divir+1),1)
            enddo
        endif

    enddo

    !>各要素の節点番号を記録する
    allocate(numbers(Noelem,4))

    do j = 1, divith
        do i = 1,divir
            numbers(i+(divir)*(j-1),1) = i+(divir+1)*(j-1)
            numbers(i+(divir)*(j-1),2) = numbers(i+(divir)*(j-1),1) + 1
            numbers(i+(divir)*(j-1),3) = i+(divir+1)*(j)  + 1
            numbers(i+(divir)*(j-1),4) = numbers(i+(divir)*(j-1),3) - 1
        enddo
    enddo

    Npelem = 4 !>各要素は正方形のため要素ごとの節点は4つ

 !>ファイル読み取り
    open(numinput,file="input.dat",status="old") 
    read(numinput,*)young
    read(numinput,*)poisson
    close(numinput)

        !!>プログラミング演習用の境界条件を付与する場合
        ! if(mode == 1)then

    !>理論解の導出
    stress = 100!>引張応力の入力
            
    open(numload,file="load.dat",status="old")
    read(numload,*)Noload,Nodim
    SoM = Nonode*Nodim
    close(numload)

    allocate(d_th(SoM))
    d_th = 0.0d0
    x = (3-poisson)/(1+poisson) !>κを記録
    y = young/(2*(1+poisson)) !>μを記録

    do j = 1, divith + 1 !>1回のループで放射状に１行の理論解を求めている
        theta = pi/(2*divith)*(j-1)

        if (theta <= 0.7853981633974482) then
            deltar = (lengx * sqrt(1 + tan(theta)**2) - radius) / divir !>0-45度
        else
            deltar = (lengy * sqrt(1 + (1/(tan(theta))**2)) - radius) / divir !45-90度
        endif


        rrr(1) = radius
        do i = 2, divir+1
            rrr(i) = rrr(i-1) + deltar * meshfac(i-1)
        enddo
        
        do i = 1, divir + 1
            k = (i + (j-1)*(divir+1))!> 煩わしいので簡略化

            d_th(k*2 - 1) = (stress * radius / (8*y)) * (rrr(i)/radius * (x+1) * cos(theta) + &
                            2*radius/rrr(i) * ((1+x) * cos(theta) + cos(3*theta)) - &
                            2*radius**3 / (rrr(i)**3) * cos(3*theta))
            d_th(k*2) = (stress * radius / (8*y)) * (rrr(i)/radius * (x-3) * sin(theta) + &
                            2*radius/rrr(i) * ((1-x) * sin(theta) + sin(3*theta)) - &
                            2*radius**3 / (rrr(i)**3) * sin(3*theta))
        enddo
    enddo
        ! Nobc = 2*(divir+1) + (divith/2 + 1)!>ディリクレ境界条件の数
        Nobc = (divir + divith)*2 + 2!>ディリクレ境界条件の数
        filename = 'bc.dat' !>ファイルの名前と拡張子
        open(unit = numbc, file = filename, status = 'replace')
            write(numbc, '(i0,a)') Nobc,' 2'

            do i = 1, divir!>右下の境界条件は後で定義する
                write(numbc, '(i0,a,i0,a,G0)') i,' ',2,' ',0.0d0
            enddo

            j = (divir+1)*divith !>左端の節点番号を表すために使う
            do i = j+1, j + divir!>左上の境界条件は後で定義する
                write(numbc, '(i0,a,i0,a,G0)') i,' ',1,' ',0.0d0
            enddo



            ! x = (3-poisson)/(1+poisson) !>κを記録
            ! y = young/(2*(1+poisson)) !>μを記録
            ! k = (divir+1)
            ! theta = 0

            ! z = (stress * radius / (8*y)) * (lengx/radius * (x+1) * cos(theta) + & !>右端の理論解を計算
            ! 2*radius/lengx * ((1+x) * cos(theta) + cos(3*theta)) - &
            ! 2*radius**3 / (lengx**3) * cos(3*theta))

            ! do i = 1 ,divith/2 + 1
            !     write(numbc, '(i0,a,i0,a,G0)') i*(divir + 1),' ',1,' ',z !>右端に変位を与えている
            ! enddo


            ! do i = divith/2 + 2 , divith + 1
            !     write(numbc, '(i0,a,i0,a,G0)') i*(divir + 1),' ',1,' ',z !>上端に右方向変位を与えている
            ! enddo

            do i = 1,divith+1
                k = i*(divir+1) *2
                write(numbc, '(i0,a,i0,a,G0)')i*(divir + 1),' ',1,' ',d_th(k-1)!>右端に変位を与えている
                write(numbc, '(i0,a,i0,a,G0)')i*(divir + 1),' ',2,' ',d_th(k)!>右端に変位を与えている
            enddo


        close(numbc)



        filename = 'load.dat'

        !>load.datファイルの作成
        open(unit = numload, file = filename, status = 'replace')

            Noload = (divith/2 + 1)*2 !>ノイマン境界条件の数
            x = 0.0d0 !>右方向等分布荷重の大きさ
            y = 0.0d0 !>上方向等分布荷重の大きさ

            write(numload, '(i0,a)') Noload,' 2'
            

            !>x方向ノイマン境界条件(荷重)
            write(numload, '(i0,a,i0,a,G0)') 1*(divir + 1),' ',1,' ',x
            do i = 1 + 1,divith/2
                write(numload, '(i0,a,i0,a,G0)') i*(divir + 1),' ',1,' ',2*x
            enddo
            write(numload, '(i0,a,i0,a,G0)') (divith/2 + 1)*(divir + 1),' ',1,' ',x

            !>y方向ノイマン境界条件(荷重)
            write(numload, '(i0,a,i0,a,G0)') (divith/2 + 1)*(divir + 1),' ',2,' ',y
            do i = (divith/2 + 1)+1, divith
                write(numload, '(i0,a,i0,a,G0)') i*(divir + 1),' ',2,' ',2*y
            enddo
            write(numload, '(i0,a,i0,a,G0)') (divith+1)*(divir + 1),' ',2,' ',y


        close(numload)

    ! endif


 !>境界条件読み取り&支配方程式導出

        open(numload,file="load.dat",status="old")
            read(numload,*)Noload,Nodim
    
            ! SoM = Nonode*Nodim
            ! write(*,*)"SoM=",SoM
            allocate(d_cal(SoM)) !> 節点数×最大自由度の分だけ解の要素数を作る
            ! allocate(d_th(SoM)) !>境界条件を求めるときに確保しているため省略
            allocate(Force(SoM)) !>節点数×最大自由度の分だけ外力ベクトルの要素数を作る

            d_cal = 0.0 !> 解の初期化
            ! d_th = 0.0
            Force = 0.0 !> 外力ベクトルの初期化
    
            do i=1,Noload
                read(numload,*) a,b,x
                Force((a-1)*Nodim+b) = x
            enddo
        close(numload)
    
        open(numinput,file="input.dat",status="old") 
            read(numinput,*)young
            read(numinput,*)poisson
        close(numinput)

        call CPU_TIME(file_time)
    !>Kマトリックス導出
        allocate(Kmat(SoM,SoM))
        Kmat = 0.0
        do time = 1,Noelem !>ループによって要素剛性行列を足し合わせることで剛性行列を生成する
            call makeK(poisson,young,time,numbers,SoM,Noelem,Nonode,co_node,Kmat)
        enddo
        call CPU_TIME(makeK_time)


        write(*,*)Kmat
    
        open(numbc,file="bc.dat",status="old")
    
        read(numbc,*) Nobc,Nodim
            do i = 1,Nobc
                read(numbc,*) a,b,x
                n = (a-1)*Nodim + b

                do j = 1, SoM
                    Force(j) = Force(j) - Kmat(j,n)*x !>右辺を適切に引き算
                enddo

                do j = 1,SoM
                    Kmat(n,j) = 0
                    Kmat(j,n) = 0
                enddo
                Kmat(n,n) = 1
                
                d_cal(n) = n
                Force(n) = x !>解を間接的に変位ベクトルに代入

            enddo
    
        close(numbc)
        call CPU_TIME(makeeq_time)

 !>CG法を用いた求解
    allocate(bvec(SoM),Amat(SoM,SoM),xvec(SoM),rvec(SoM),r0(SoM))!>長すぎるため改行
    allocate(rsavevec(SoM),pvec(SoM),alpha(SoM),beta(SoM),pro(SoM))

    xvec = 0.0; rvec = 0.0; rsavevec = 0.0; pvec = 0.0 !>各ベクトルの初期化

    xvec = d_cal
    Amat = Kmat!> A,bをソルバに入力(bはノイマン境界条件)
    bvec = Force

    call product(Amat,xvec,SoM,pro)
    rvec = bvec - pro !>r0の定義
    pvec = rvec !> p0の定義
    r0 = rvec !> r0の値は繰り返し判定に使うため保存しておく

    do i = 1, 10000
        call product(Amat,pvec,SoM,pro) !>Amatとp(i-1)の積を求める

        x = dot_product(rvec,rvec) !> alphaの分子
        y = dot_product(pvec,pro) !> alphaの分母
        alpha = x/y !>alpha(i)の計算
        
        xvec = xvec + alpha * pvec !>x(i)の計算
        rsavevec = rvec !>r(i-1)を保存
        rvec = rvec - alpha*pro !>r(i)を計算
        
        x = sqrt(dot_product(rvec,rvec))
        y = sqrt(dot_product(r0,r0))
        z = (dot_product(rsavevec,rsavevec))
        if((x/y) <= 1.0E-8)then !>繰り返し判定
            exit
        endif

        beta = x*x/z
        pvec = rvec + beta*pvec
    enddo

    if(i == 10000)then
        write(*,*)"CG method error"
        stop
    endif
    d_cal = xvec
    call CPU_TIME(calc_time)


    

 !>誤差評価
    allocate(elel2(Noelem))
    elel2 = 0.0d0
    L2 = 0 
    L2_nu = 0; L2_de = 0

    do time = 1,Noelem!>各要素について誤差評価を行っている
        ! call makeL2(time,Nonode,Noelem,numbers,co_node,SoM,d_cal,d_th,elel2,L2_nu,L2_de)
        call makeL2(time,Nonode,Noelem,numbers,co_node,stress,radius,poisson,young,SoM,d_cal,elel2,L2_nu,L2_de)
        
    enddo

    L2 = sqrt(L2_nu)/sqrt(L2_de)



    !  !>理論解の導出
    ! x = (3-poisson)/(1+poisson) !>κを記録
    ! y = young/(2*(1+poisson)) !>μを記録

    ! do j = 1, divith + 1 !>1回のループで放射状に１行の理論解を求めている
    !     theta = pi/(2*divith)*(j-1)

    !     if (theta <= 0.7853981633974482) then
    !         deltar = (lengx * sqrt(1 + tan(theta)**2) - radius) / divir !>0-45度
    !     else
    !         deltar = (lengy * sqrt(1 + (1/(tan(theta))**2)) - radius) / divir !45-90度
    !     endif
        
    !     ! r = radius

    !     rrr(1) = radius
    !     do i = 2, divir+1
    !         rrr(i) = rrr(i-1) + deltar * meshfac(i-1)
    !     enddo
        
    !     do i = 1, divir + 1
    !         k = (i + (j-1)*(divir+1))!> 煩わしいので簡略化

    !         d_th(k*2 - 1) = (stress * radius / (8*y)) * (rrr(i)/radius * (x+1) * cos(theta) + &
    !                         2*radius/rrr(i) * ((1+x) * cos(theta) + cos(3*theta)) - &
    !                         2*radius**3 / (rrr(i)**3) * cos(3*theta))
    !         d_th(k*2) = (stress * radius / (8*y)) * (rrr(i)/radius * (x-3) * sin(theta) + &
    !                         2*radius/rrr(i) * ((1-x) * sin(theta) + sin(3*theta)) - &
    !                         2*radius**3 / (rrr(i)**3) * sin(3*theta))
    !     end do

    ! enddo

 !>vtkファイル出力


    filename = 'madoka_outcome.vtk' !>ここでファイルの名前と拡張子を決めている

    open(unit = numVTK_cal, file = filename, status = 'replace')
    write(numVTK_cal, '(a)') '# vtk DataFile Version 4.1'
    write(numVTK_cal, '(a)') 'Thisis a comment line'
    write(numVTK_cal, '(a)') 'ASCII'
    write(numVTK_cal, '(a)') 'DATASET UNSTRUCTURED_GRID'

  
    write(numVTK_cal, '(a,i0,a)') 'POINTS ',Nonode,' float'
    do i = 1,Nonode
        !> 各節点の座標を行ごとに代入する
        write(numVTK_cal, '(G0,a,G0,a,G0)') dble(co_node(i,1)),' ',  dble(co_node(i,2)),' ',  dble(co_node(i,3))
    enddo

    write(numVTK_cal, '(a,i0,a,i0)') 'CELLS ', Noelem,' ', (1 + Npelem)*Noelem 
    do i = 1,Noelem
        !> 各要素の構成番号を要素ごとに入力する
        write(numVTK_cal,'(a,I0,a,I0,a,I0,a,I0)') '4 ',numbers(i,1)-1,' ', numbers(i,2)-1,' ', numbers(i,3)-1,' ', numbers(i,4)-1
    enddo

    write(numVTK_cal, '(a,I0)') 'CELL_TYPES ' , Noelem
    do i = 1, Noelem
        write(numVTK_cal,'(I0)') 9
    enddo

    write(numVTK_cal, '(a,I0)') 'POINT_DATA ',Nonode
    write(numVTK_cal, '(a)')'VECTORS d_cal_Vector_Data float'
    do i = 1, Nonode
        !>線形ソルバで求めた変位を入力する,z方向は0で入力
        write(numVTK_cal,'(G0,a,G0,a)') d_cal(2*i-1),' ',d_cal(2*i),' 0.0'
    enddo

    write(numVTK_cal, '(a)')'VECTORS d_th_Vector_Data float'
    do i = 1, Nonode
        !>理論解から求めた変位を入力する,z方向は0で入力
        write(numVTK_cal,'(G0,a,G0,a)') d_th(2*i-1),' ',d_th(2*i),' 0.0'
    enddo

    write(numVTK_cal, '(a,I0)') 'CELL_DATA ',Noelem
    write(numVTK_cal, '(a)')'SCALARS L2_Data float'
    write(numVTK_cal, '(a)')'LOOKUP_TABLE default'
    do i = 1, Noelem
        !>各点におけるL2誤差
        write(numVTK_cal,'(G0)') elel2(i)
    enddo

    close(numVTK_cal)

    call CPU_TIME(end_time)
    ! write(*,*) '  L2誤差 =',L2

    write(*,*)'--GPT添削済-'

    ! write(*,*)'--測定範囲--','-|-------計算時間----------','|--------割合--------|'
    ! write(*,*)'  実行時間  ',(end_time - start_time),(end_time - start_time)/(end_time - start_time)*100,'%'
    ! write(*,*)'行列生成時間',(makeK_time - file_time),(makeK_time - file_time)/(end_time - start_time)*100,'%'
    ! write(*,*)'  求解時間  ',(calc_time - makeeq_time),(calc_time - makeeq_time)/(end_time - start_time)*100,'%'
    write(*,*)'要素数,','節点数,','L2誤差,','計算時間[s],','求解時間[s],','求解時間割合[%],'
    write(*,*) Noelem, ',', Nonode, ',', L2, ',', (end_time - start_time), ',', &
    (calc_time - makeeq_time), ',', (calc_time - makeeq_time)/(end_time - start_time)*100
    
end program
