program main
    !use
    ! use mod_monolis
    ! use dbg
    ! use variables
    ! use initialize
    ! use makeMesh
    ! use makeK
    ! use insert_BC
    ! use CGsolver
    ! use addXtoG
    ! use evaluate
    ! use paraview
    ! use finalize
    use mod_monolis
    use dbg
    use variables
    use initialize
    use makeMesh

    implicit none

    !call

    !こちらを実行
    call mod00_debug()
    call mod03_makeMesh()
    !マジックナンバー調査
    ! integer(4) :: seisuu1,seisuu2,seisuu3
    ! real(8) :: syousuu1,syousuu2,syousuu3
    ! syousuu2 = 0.0d0
    ! do seisuu1 = 10,100
    !     syousuu1 = 10.0d0 / seisuu1 !leng_mesh
    !     do seisuu2 = 1,1000
    !         syousuu2 = syousuu1 * seisuu2
    !         if (syousuu2 >= 2.5d0) then
    !             if((syousuu1/6 - syousuu1/(6*sqrt(3.0d0))) < syousuu2-2.5d0)then
    !                 write(*,*) seisuu1, syousuu2-2.5d0
    !             endif
    !             exit  ! seisuu2のループを抜ける
    !         end if
    !     end do
    ! end do


end program main