module makeMesh
    use dbg
    use variables
    use initialize
    implicit none

    contains



    subroutine Gmesher()!Gmesh定義
        implicit none
        integer(4) :: i,j
        dbg_name = 'Gmesher'
        call dbg_start_subroutineM()
     
        meshdata%leng_mesh = (meshdata%lengL/meshdata%diviL)

        !G領域座標,id設定
        do i = 1, meshdata%diviL + 1
            do j = 1, meshdata%diviL + 1
                Garea%co_node(1,(meshdata%diviL+1)*(i-1)+ j ) = (j-1)*meshdata%leng_mesh
                Garea%co_node(2,(meshdata%diviL+1)*(i-1)+ j ) = (i-1)*meshdata%leng_mesh
            enddo
        enddo

        do j = 1, meshdata%diviL
            do i = 1, meshdata%diviL
                Garea%elem_node_id(1,i+ meshdata%diviL * (j-1)) = i+(meshdata%diviL+1)*(j-1)
                Garea%elem_node_id(2,i+ meshdata%diviL * (j-1)) = Garea%elem_node_id(1,i+meshdata%diviL * (j-1)) + 1
                Garea%elem_node_id(3,i+ meshdata%diviL * (j-1)) = i+(meshdata%diviL+1)*(j)  + 1
                Garea%elem_node_id(4,i+ meshdata%diviL * (j-1)) = Garea%elem_node_id(3,i+ meshdata%diviL * (j-1)) - 1
            enddo
        enddo
    end subroutine Gmesher

    subroutine mesher_visualize()!メッシャー部分の可視化ファイル作成
        implicit none
        integer(4) :: i,numab=10,numL2=11,numVTK = 12
        character(256) :: filename = '08_xfem.vtk'
        dbg_name = 'mesehr_visualize'
        call dbg_start_subroutineM()

    !     open(unit = numVTK, file = '08_xfem.vtk', status = 'replace')
    !         write(numVTK, '(a)') '# vtk DataFile Version 4.1'
    !         write(numVTK, '(a)') 'This is a comment line'
    !         write(numVTK, '(a)') 'ASCII'
    !         write(numVTK, '(a)') 'DATASET UNSTRUCTURED_GRID'
    !         write(numVTK, '(a,i0,a)') 'POINTS ',Garea%Nnode,' float'

    !         do i = 1,Garea%Nnode!>節点の座標を行ごとに代入する
    !             write(numVTK, '(G0,a,G0,a,G0)') dble(Garea%co_node(1,i)),&
    !             ' ',  dble(Garea%co_node(2,i)),' 0.0'
    !         enddo

    !          write(numVTK, '(a,i0,a,i0)') 'CELLS ', Garea%Nelem ,' ', 5*(Garea%Nelem)

    !         do i =1,Garea%Nelem!節点id入力
    !             write(numVTK,'(a,I0,a,I0,a,I0,a,I0)') '4 ', Garea%elem_node_id(1,i)-1,' ',&
    !             Garea%elem_node_id(2,i)-1,' ',&
    !             Garea%elem_node_id(3,i)-1,' ',&
    !             Garea%elem_node_id(4,i)-1
    !         enddo

    !         write(numVTK, '(a,I0)') 'CELL_TYPES ' , Garea%Nelem
    !         do i = 1,Garea%Nelem
    !             write(numVTK,'(I0)') 9
    !         enddo
    !    close(numVTK)

        !L2ノルム誤差評価用のメッシュを作成
    !     open(unit = numL2, file = '08_xfem_eval.vtk', status = 'replace')
    !         write(numL2, '(a)') '# vtk DataFile Version 4.1'
    !         write(numL2, '(a)') 'This is a comment line'
    !         write(numL2, '(a)') 'ASCII'
    !         write(numL2, '(a)') 'DATASET UNSTRUCTURED_GRID'
    !         write(numL2, '(a,i0,a)') 'POINTS ',Earea%Nnode,' float'

    !         do i = 1,Earea%Nnode!>節点の座標を行ごとに代入する
    !             write(numL2, '(G0,a,G0,a,G0)') dble(Earea%co_node(1,i)),&
    !             ' ',  dble(Earea%co_node(2,i)),' 0.0'
    !         enddo

    !          write(numL2, '(a,i0,a,i0)') 'CELLS ', Earea%Nelem ,' ', 5*(Earea%Nelem)

    !         do i =1,Earea%Nelem!節点id入力
    !             write(numL2,'(a,I0,a,I0,a,I0,a,I0)') '4 ', Earea%elem_node_id(1,i)-1,' ',&
    !             Earea%elem_node_id(2,i)-1,' ',&
    !             Earea%elem_node_id(3,i)-1,' ',&
    !             Earea%elem_node_id(4,i)-1
    !         enddo

    !         write(numL2, '(a,I0)') 'CELL_TYPES ' , Earea%Nelem
    !         do i = 1,Earea%Nelem
    !             write(numL2,'(I0)') 9
    !         enddo
    !    close(numL2)

        ! !絶対誤差評価用のメッシュを作成
        ! open(unit = numab, file = '08_absolute_error.vtk', status = 'replace')
        !     write(numab, '(a)') '# vtk DataFile Version 4.1'
        !     write(numab, '(a)') 'This is a comment line'
        !     write(numab, '(a)') 'ASCII'
        !     write(numab, '(a)') 'DATASET UNSTRUCTURED_GRID'
        !     write(numab, '(a,i0,a)') 'POINTS ',Harea%Nnode,' float'

        !     do i = 1,Harea%Nnode!>節点の座標を行ごとに代入する
        !         write(numab, '(G0,a,G0,a,G0)') dble(Harea%co_node(1,i)),&
        !         ' ',  dble(Harea%co_node(2,i)),' 0.0'
        !     enddo

        !         write(numab, '(a,i0,a,i0)') 'CELLS ', Harea%Nelem ,' ', 5*(Harea%Nelem)

        !     do i =1,Harea%Nelem!節点id入力
        !         write(numab,'(a,I0,a,I0,a,I0,a,I0)') '4 ', Harea%elem_node_id(1,i)-1,' ',&
        !         Harea%elem_node_id(2,i)-1,' ',&
        !         Harea%elem_node_id(3,i)-1,' ',&
        !         Harea%elem_node_id(4,i)-1
        !     enddo

        !     write(numab, '(a,I0)') 'CELL_TYPES ' , Harea%Nelem
        !     do i = 1,Harea%Nelem
        !         write(numab,'(I0)') 9
        !     enddo
        ! close(numab)

    end subroutine mesher_visualize

    subroutine make_node_dat()!node.dat作成
        implicit none 
        integer(4) :: numnode=12,i

        open(unit = numnode, file = 'node.dat', status = 'replace')       
            write(numnode, '(i0)')Garea%Nnode
        do i = 1,Garea%Nnode
            write(numnode, '(G0,a,G0,a)') Garea%co_node(1,i),' ',Garea%co_node(2,i),' 0.0'
        enddo
        close(numnode)
    end subroutine make_node_dat

    subroutine make_elem_dat()!elem.dat作成
        implicit none 
        integer(4) :: numelem=10,i

        open(unit = numelem, file = 'elem.dat', status = 'replace')    
        write(numelem, '(i0,a,i0)') Garea%Nelem,' ',4
        do i = 1,Garea%Nelem
            write(numelem, '(i0,a,i0,a,i0,a,i0)')  Garea%elem_node_id(1,i),' ',Garea%elem_node_id(2,i),' ',&
            Garea%elem_node_id(3,i),' ',Garea%elem_node_id(4,i)
        enddo

    end subroutine make_elem_dat

    subroutine make_bc_dot_dat()!bc.dat作成
        implicit none
        integer(4) :: i,j,k,n,c1,c2,c4,Nbc,Ndim,Nid,dim,numbc=10,Nconstrain
        real(8) :: x,ka,mu,ra,th,xco,yco,pi,c3,uth(Garea%Nnode*2)
        logical :: enriched_any(Garea%Nelem),constrain(Garea%Nnode)

        dbg_name = 'make_bc_dot_dat'
        call dbg_start_subroutineM()

        c1 = meshdata%diviL!Gmesh一辺分割数

        !外周理論解の作成(ついでに領域全体の理論解を求めておく)
        if(dbg_disign_sig_inf)then
            sig_inf = sig_inf_disign
        endif

        pi = 4*atan(1.0d0)
        ka = (3-poisson)/(1+poisson)
        mu = young/(2*(1+poisson))
        c3 = sig_inf*meshdata%lengR/(8*mu)
        
        uth(1) = 0.0d0
        uth(2) = 0.0d0
        do i = 2,Garea%Nnode
            
            xco = Garea%co_node(1,i)
            yco = Garea%co_node(2,i)
            ra = sqrt(xco**2 + yco**2)

            !デバッグ

            if(xco <= 1.0E-08)then!atan=∞となる場合の処理
                th = pi/2
            else
                th = atan(yco/xco)
            endif

            uth(i*2 - 1) = &
            c3*(ra*(ka+1)*cos(th)/meshdata%lengR + &
            2*meshdata%lengR*((1+ka)*cos(th) + cos(3*th))/ra - &
            2*(meshdata%lengR**3)*cos(3*th)/(ra**3))

            uth(i*2    ) = &
            c3*(ra*(ka-3)*sin(th)/meshdata%lengR + &
            2*meshdata%lengR*((1-ka)*sin(th) + sin(3*th))/ra - &
            2*(meshdata%lengR**3)*sin(3*th)/(ra**3))


            ! !デバッグ?????
            if(ra < meshdata%lengR)then
                uth(i*2-1) = 0.0d0
                uth(i*2  ) = 0.0d0
            endif

            ! !デバッグ
            ! write(*,*)uth(i*2-1),uth(i*2)
        enddo

        enriched_any = .false.

        do i = 1, Garea%Nelem !> エンリッチ節点を含む要素を探す
            if(meshdata%enrich_node(Garea%elem_node_id(1,i)) .or. meshdata%enrich_node(Garea%elem_node_id(2,i))&
            .or. meshdata%enrich_node(Garea%elem_node_id(3,i)) .or. meshdata%enrich_node(Garea%elem_node_id(4,i)))then
                enriched_any(i) = .true.
            endif
        enddo

        constrain = .false.

        do i = 1,Garea%Nelem
            if(enriched_any(i))then
                do j = 1,4
                    if(.not. meshdata%enrich_node(Garea%elem_node_id(j,i)))then
                        constrain(Garea%elem_node_id(j,i)) = .true.
                        ! Nconstrain = Nconstrain +1　!ここでカウントしてはいけない
                    endif
                enddo
            endif
        enddo

        !Nconstrainの数を数える
        Nconstrain = 0
        do i = 1, Garea%Nnode
            if(constrain(i))then
                Nconstrain = Nconstrain + 1
            endif
        enddo

        ! !境界条件数チェック
        c2 = 0
        c4 = 0
        do i = 1, c1+1
            if(meshdata%enrich_node(i))then
                c2 = c2+1
            endif

            j = (i-1)*(c1+1) +1
            if(meshdata%enrich_node(j))then
                c4 = c4+1
            endif
        enddo

        !bc.datファイルの作成
        open(unit = numbc, file = 'bc.dat', status = 'replace')
        ! write(numbc,'(i0,a)')6*c1 + 3 + 2*c1**2 + 4*c1 + 2 -2*meshdata%Nenrich + c2 + c4,' 4'!{境界条件数,Ndof}
        write(numbc,'(i0,a)')6*c1 + 3 + c2 + c4 + 2*Nconstrain,' 4'!{境界条件数,Ndof}


        !G領域下端固定
        do i = 1, c1 + 1
            write(numbc, '(i0,a,i0,a,G0)')i,' ',2,' ',0.0d0
        enddo

        !G領域右端左端理論解
        do i = 1, c1 
            j = (c1+1)*(i-1) +1
            k = (c1+1)*i
            !デバッグ
            write(numbc, '(i0,a,i0,a,G0)')j,' ',1,' ',0.0d0
            write(numbc, '(i0,a,i0,a,G0)')k,' ',1,' ',uth(k*2-1)
            write(numbc, '(i0,a,i0,a,G0)')k,' ',2,' ',uth(k*2  )
        enddo
        
        !G領域上端理論解
        j =  (c1+1)*c1 + 1
        write(numbc, '(i0,a,i0,a,G0)')j,' ',1,' ',0.0d0
        write(numbc, '(i0,a,i0,a,G0)')j,' ',2,' ',uth(j*2  )
        do i = 2, c1+1
            j =  (c1+1)*c1 + i
            write(numbc, '(i0,a,i0,a,G0)')j,' ',1,' ',uth(j*2-1)
            write(numbc, '(i0,a,i0,a,G0)')j,' ',2,' ',uth(j*2  )
        enddo

        !X領域下端固定
        do i = 1, c1 + 1
            if(meshdata%enrich_node(i))then
                write(numbc, '(i0,a,i0,a,G0)')i,' ',4,' ',0.0d0
            endif

        enddo

        !X領域左端固定
        do i = 2, c1+1
            j = (c1+1)*(i-1) +1
            if(meshdata%enrich_node(i))then
                write(numbc, '(i0,a,i0,a,G0)')j,' ',3,' ',0.0d0
            endif
        enddo

        do i = 1,c1+1
            do j = 1,c1+1
            k = (c1+1)*(i-1)+j
                if(constrain(k))then
                    write(numbc, '(i0,a,i0,a,G0)')k,' ',3,' ',0.0d0
                    write(numbc, '(i0,a,i0,a,G0)')k,' ',4,' ',0.0d0
                endif
            enddo
        enddo
            
        close(numbc)
    end subroutine make_bc_dot_dat

    subroutine make_load_dat()!load.datの作成
        implicit none 
        integer(4) :: numload=11
        open(unit = numload, file = 'load.dat', status = 'replace')
        write(numload, '(i0,a)')0,' 4'
        ! write(numload, '(i0,a,G0)')1,' 1 ',x
        close(numload)  

    end subroutine make_load_dat

    subroutine make_input_dat()!input.dat作成
        implicit none 
        integer(4) :: numinput = 13
        open(unit = numinput, file = 'input.dat', status = 'replace')
        write(numinput,'(G0)')meshdata%lengL
        write(numinput,'(G0)')meshdata%leng_mesh
        write(numinput,'(i0)')meshdata%diviL
        write(numinput,'(G0)')meshdata%lengR
        close(numinput)
    end subroutine make_input_dat

    subroutine make_monolis_files()!並列計算用のファイルを作成
        implicit none
        ! integer(4) :: i,numelem=10,numload=11,numnode=12,numinput=13
        dbg_name = 'make_monolis_files'
        call dbg_start_subroutineM()

        call make_node_dat()
        call make_elem_dat()
        call make_bc_dot_dat()
        call make_load_dat()
        call make_input_dat()

    end subroutine make_monolis_files

    subroutine make_weight_dat()
        implicit none 
        integer(4) :: i,j,numw= 10
        integer(4) :: w_node(Garea%Nnode)

        dbg_name = 'make_weight_dat'
        call dbg_start_subroutineM()

        w_node = 0
        do i = 1,Garea%Nelem
            if(meshdata%subdivision(i))then
                do j = 1,4
                    w_node(Garea%elem_node_id(j,i)) = w_node(Garea%elem_node_id(j,i)) + Gauss%n_rec**2
                enddo
            else
                do j = 1,4
                w_node(Garea%elem_node_id(j,i)) = w_node(Garea%elem_node_id(j,i)) + 1
                enddo
            endif
        enddo


        open(unit = numw, file = 'node_weight.dat', status = 'replace')
        write(numw,'(a)')'# weight'
        write(numw,'(i0,a)')Garea%Nnode,' 1'

        do i = 1,Garea%Nnode
            ! if(meshdata%enrich_node(i))then
            !     write(numw,'(i0)')Gauss%n_rec**2
            ! else
            !     write(numw,'(a)')'1'
            ! endif
            write(numw,'(i0)')w_node(i)
        enddo
    end subroutine make_weight_dat

    subroutine mod03_makeMesh()
        dbg_name = 'mod03_makeMesh'
        call dbg_start_subroutineL()

        call announce_debug()
        call initialize_meshdata()
        call initialize_Garea()
        call initialize_Gauss()

        call Gmesher()
        call select_subdivision()
        call select_Xnode()
        call make_monolis_files()
        call make_weight_dat()

    end subroutine mod03_makeMesh



end module makeMesh