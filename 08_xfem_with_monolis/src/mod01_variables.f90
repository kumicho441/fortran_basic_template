module variables
    use mod_monolis
    use dbg
    implicit none


    real(8) :: young=200000,poisson=0.3
    real(8) :: sig_inf = 100



    ! CHARACTER(100) :: fname
    ! CHARACTER(*) :: top_dir_name 
    ! CHARACTER(*) :: part_dir_name 
    ! CHARACTER(*) :: file_name 
    ! CHARACTER(*), PARAMETER :: top_dir_name = 'top_dir_name'!04
    ! CHARACTER(*), PARAMETER :: part_dir_name = 'part_dir_name'!04
    ! CHARACTER(*), PARAMETER :: file_name = 'file_name'!04

    type :: matrix_struc
    !全体剛性方程式生成に関する変数
    real(8) :: Dmat(3,3),Jmat(2,2),Jinv(2,2),detJ,Bmat(3,8),BTmat(8,3),Kemat(16,16),KeGX(8,8),mat88_u(8,8)!Kematのサイズ変更
    real(8) :: mat38(3,8),mat24(2,4),mat42_m(4,2),mat42_u(4,2),mat88(8,8),BonGT(8,3),mat24_m(2,4),mat24_u(2,4)
    ! real(8),allocatable :: KG(:,:),KGX(:,:),KX(:,:)

    ! !ガウス積分に関する変数
    ! integer(4) :: Nip=4,n_rec=4, Nip_subd
    ! real(8) :: co_ip(2,4),weight,weight_subd
    ! real(8),allocatable ::co_ip_subd(:,:)
    end type matrix_struc

    type :: Gauss_struc
    !ガウス積分に関する変数
    integer(4) :: Nip=4,n_rec=4, Nip_subd
    real(8) :: co_ip(2,4),weight,weight_subd
    real(8),allocatable ::co_ip_subd(:,:)
    end type Gauss_struc

    type :: meshdata_struc
    !メッシュ生成に用いる変数
    real(8) :: lengL,lengR,leng_mesh
    integer(4) :: diviL
    integer(4) :: Nenrich,Nconstrain!追加の物理量を付与する"節点"数,追加の物理量を0で拘束する"節点"数
    logical,allocatable :: subdivision(:)!またぐ判定(細分化)判定
    logical,allocatable :: enrich_node(:)!節点がエンリッチ要素のものか判定
    end type meshdata_struc

    type :: area_struc
    integer(4) :: Nelem,Nnode
    integer(4),allocatable :: elem_node_id(:,:)!elem_node_id(節点番号,要素番号)
    real(8),allocatable :: co_node(:,:)!co_node(方向,要素番号)
    end type area_struc

    type :: solver_struc
    real(8),allocatable :: Fvec(:),uvec(:),uth(:)
    real(8) :: load,Force!引張応力
    end type solver_struc

    type eval_struc
    !L2ノルム誤差評価
    real(8) :: L2,L2_nu,L2_de,dif_nu,dif_de!nuは分子,deは分母を意味する
    real(8),allocatable :: elel2(:),ucal(:),uvec_th(:),solver_uvec(:)

    !絶対誤差評価
    real(8) :: Ea,eleEa_sub(2)!nuは分子,deは分母を意味する
    real(8),allocatable :: eleEa(:)
    end type eval_struc

    type(matrix_struc) :: matrix
    type(meshdata_struc) :: meshdata
    type(area_struc) :: Garea,Earea,Harea
    type(solver_struc) :: solver
    type(eval_struc) :: eval
    type(Gauss_struc) :: Gauss

    type(monolis_structure) :: monost!モノリス構造体
    type(monolis_com) :: com!ソルバ部分で用いる構造体
end module variables

    