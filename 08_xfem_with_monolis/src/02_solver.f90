program main
    !use
    use mod_monolis
    use dbg
    use variables
    use initialize
    ! use makeMesh!Emesher,Hmesherはこちらで動いている
    use makeK
    use insert_BC
    use CGsolver
    use addXtoG

    ! use evaluate
    ! use paraview
    ! use finalize

    implicit none
    integer(4) :: message_switch
    integer(4) :: comm!MPIコミュニケータ

    !call




    call monolis_global_initialize()

    !通信データ構造体の初期化処理
    comm = monolis_mpi_get_global_comm()
    call monolis_com_initialize_by_parted_files(com,comm,MONOLIS_DEFAULT_TOP_DIR, MONOLIS_DEFAULT_PART_DIR, "node.dat")

    ! message_switch = monolis_mpi_get_global_my_rank()
    ! if(message_switch == 0)then
    !     write(*,*)'rank',', K生成時間',', 求解時間'
    ! endif

    !こちらを実行
    call mod00_debug()

    call deallocate_variables()
    call initialize_Gauss()
    call read_files()
    call select_subdivision()
    call select_Xnode()

    call mod04_makeK_withM()
    call mod05_BC_withM()
    call mod06_CGsolver_withM()
    call mod07_uG_plus_uX()


    call monolis_global_finalize()!monolisの並列計算に関する終了処理


end program main