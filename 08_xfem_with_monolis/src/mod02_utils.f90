module initialize
    use dbg
    use variables
    implicit none
    contains



    subroutine initialize_meshdata()! meshdata_strucの変数を初期化
        implicit none
        real(8) :: ratioR
        dbg_name = 'initialize_meshdata'
        call dbg_start_subroutineM()

        meshdata%lengL = 10.0d0
        meshdata%diviL = 10
        meshdata%Nenrich = 0

        !デバッグ:メッシュ指定

            meshdata%lengL = lengL_disign
            meshdata%diviL = diviL_disign
            ratioR = ratioR_disign


        meshdata%lengR = meshdata%lengL*ratioR
    end subroutine initialize_meshdata


    subroutine initialize_Garea()! Gareaの変数を初期化
        dbg_name = 'initialize_Garea'
        call dbg_start_subroutineM()

        Garea%Nelem = meshdata%diviL**2
        Garea%Nnode = (meshdata%diviL + 1)**2
        
        allocate(Garea%elem_node_id(4, Garea%Nelem), &
                 Garea%co_node(2, Garea%Nnode), &
                 meshdata%subdivision(Garea%Nelem))
        Garea%elem_node_id = 0
        Garea%co_node = 0.0d0
        meshdata%subdivision = .false.
    end subroutine initialize_Garea

    subroutine initialize_Earea()! Eareaの変数を初期化
        implicit none
        integer(4) :: divi=100
        Earea%Nelem = divi**2
        Earea%Nnode = (divi + 1)**2

        allocate(Earea%elem_node_id(4, Earea%Nelem), &
                 Earea%co_node(2, Earea%Nnode), &
                 )
        Earea%elem_node_id = 0
        Earea%co_node = 0.0d0


    end subroutine initialize_Earea

    subroutine initialize_matrix()!matrix%の初期化
        implicit none
        integer(4) :: i,j,k,in
        real(8) :: len1,len2,len3,r(2)

        ! dbg_name = 'initialize_matrix'
        ! call dbg_start_subroutine()

        matrix%Dmat = 0.0d0
        matrix%Jmat = 0.0d0
        matrix%Jinv = 0.0d0
        matrix%detJ = 0.0d0
        matrix%Bmat = 0.0d0
        matrix%BTmat = 0.0d0
        matrix%Kemat = 0.0d0
        matrix%KeGX = 0.0d0
        matrix%mat38 = 0.0d0
        matrix%mat24 = 0.0d0
        matrix%mat42_m = 0.0d0
        matrix%mat42_u = 0.0d0
        matrix%mat88 = 0.0d0
        matrix%mat88_u = 0.0d0
        matrix%BonGT = 0.0d0
        matrix%mat24_m = 0.0d0
        matrix%mat24_u = 0.0d0
    

    end subroutine initialize_matrix

    subroutine initialize_Gauss()
        implicit none
        integer(4) :: i,j,k,in
        real(8) :: len1,len2,len3,r(2)

        Gauss%Nip = 4
        Gauss%n_rec = 4

       !デバッグ:n_rec指定
        if(dbg_disign_nrec)then
            Gauss%n_rec = n_rec_disign
        endif

        !デバッグ:細分化なし
        if(dbg_no_subdivision)then
            Gauss%n_rec = 1
        endif

        Gauss%Nip_subd = (Gauss%n_rec**2) * 4
        Gauss%co_ip = 0.0d0
        Gauss%co_ip(1,1) = -0.57735027
        Gauss%co_ip(2,1) = -0.57735027
        Gauss%co_ip(1,2) =  0.57735027
        Gauss%co_ip(2,2) = -0.57735027
        Gauss%co_ip(1,3) =  0.57735027
        Gauss%co_ip(2,3) =  0.57735027
        Gauss%co_ip(1,4) = -0.57735027
        Gauss%co_ip(2,4) =  0.57735027

        Gauss%weight = 1.0d0
        Gauss%weight_subd = 1.0d0/(Gauss%n_rec**2)
        IF (ALLOCATED(Gauss%co_ip_subd)) THEN
            DEALLOCATE(Gauss%co_ip_subd)
        endif
        allocate(Gauss%co_ip_subd(2,Gauss%Nip_subd))
        Gauss%co_ip_subd = 0.0d0

        in = 0!inの初期化
        r(1) = -0.57735027
        r(2) =  0.57735027
        len1 = 1.0d0/dble(Gauss%n_rec)
        len2 = r(1)/dble(Gauss%n_rec)
        len3 = r(2)/dble(Gauss%n_rec)

        do i = 1, Gauss%n_rec!何列目の要素に注目しているか表す
            do j = 1, Gauss%n_rec!何行目の要素に注目しているか表す
                do k = 1, 4!細分化された要素内で何番目の積分点に注目しているか表す
                    in = in + 1!要素内での積分点の番号を表す

                    ! Gauss%co_ip_subd(1,in) = (dble(i) - 1)*2.0d0*len1 + len2 - 1.0d0 + len1
                    ! Gauss%co_ip_subd(2,in) = (dble(j) - 1)*2.0d0*len1 + len3 - 1.0d0 + len1

                    if(k == 1)then
                        Gauss%co_ip_subd(1,in) = (dble(i) - 1)*2.0d0*len1 + len1 + len2 - 1.0d0
                        Gauss%co_ip_subd(2,in) = (dble(j) - 1)*2.0d0*len1 + len1 + len2 - 1.0d0
                    endif
                    if(k == 2)then
                        Gauss%co_ip_subd(1,in) = (dble(i) - 1)*2.0d0*len1 + len1 + len3 - 1.0d0
                        Gauss%co_ip_subd(2,in) = (dble(j) - 1)*2.0d0*len1 + len1 + len2 - 1.0d0
                        ! Gauss%co_ip_subd(1,in) = (dble(i) - 1)*2.0d0*len1 + len1 + len2 - 1.0d0
                        ! Gauss%co_ip_subd(2,in) = (dble(j) - 1)*2.0d0*len1 + len1 + len3 - 1.0d0
                    endif
                    if(k == 3)then
                        Gauss%co_ip_subd(1,in) = (dble(i) - 1)*2.0d0*len1 + len1 + len3 - 1.0d0
                        Gauss%co_ip_subd(2,in) = (dble(j) - 1)*2.0d0*len1 + len1 + len3 - 1.0d0
                    endif
                    if(k == 4)then
                        Gauss%co_ip_subd(1,in) = (dble(i) - 1)*2.0d0*len1 + len1 + len2 - 1.0d0
                        Gauss%co_ip_subd(2,in) = (dble(j) - 1)*2.0d0*len1 + len1 + len3 - 1.0d0
                        ! Gauss%co_ip_subd(1,in) = (dble(i) - 1)*2.0d0*len1 + len1 + len3 - 1.0d0
                        ! Gauss%co_ip_subd(2,in) = (dble(j) - 1)*2.0d0*len1 + len1 + len2 - 1.0d0
                    endif

                enddo
            enddo
        enddo

        Gauss%weight_subd = 1.0d0/(Gauss%n_rec*Gauss%n_rec)

        ! !デバッグ
        ! do i = 1,Gauss%Nip_subd
        !     write(*,*) Gauss%co_ip(1,i),Gauss%co_ip(2,i)
        !     write(*,*) Gauss%co_ip_subd(1,i),Gauss%co_ip_subd(2,i)
        ! enddo


    end subroutine initialize_Gauss

    subroutine initialize_solver()!solver%の初期化
        implicit none
        integer(4) :: n,m,Ndof=4

        n = Garea%Nnode*Ndof
        m = Garea%Nnode*2

        dbg_name = 'initialize solver'
        call dbg_start_subroutineM()

        !利用しない変数は確保しない
        allocate(solver%Fvec(n), solver%uvec(n),solver%uth(m))
        solver%Fvec = 0.0d0
        solver%uvec = 0.0d0
        solver%uth = 0.0d0
        ! solver%iter = 0
    end subroutine initialize_solver

    subroutine initialize_eval()
        implicit none
        eval%L2 = 0.0d0
        eval%L2_nu = 0.0d0
        eval%L2_de = 0.0d0
        eval%dif_nu = 0.0d0
        eval%dif_de = 0.0d0
        allocate(eval%elel2(Earea%Nelem),eval%ucal(Earea%Nnode*2),eval%uvec_th(Earea%Nnode*2),eval%eleEa(Harea%Nelem))
        allocate(eval%solver_uvec(Garea%Nnode*2))

        eval%elel2 = 0.0d0
        eval%ucal = 0.0d0
        eval%uvec_th = 0.0d0
        eval%eleEa = 0.0d0
        eval%eleEa_sub = 0.0d0
        eval%solver_uvec = 0.0d0

    end subroutine initialize_eval

    !ここからutils

    subroutine deallocate_variables()!パート1まででallocateした変数をdeallocate
        implicit none
        if(allocated(Garea%elem_node_id))then!ok
            deallocate(Garea%elem_node_id)
        endif
        if(allocated(Garea%co_node))then!ok
            deallocate(Garea%co_node)
        endif
        if(allocated(meshdata%subdivision))then
            deallocate(meshdata%subdivision)
        endif
        if(allocated(Gauss%co_ip_subd))then
            deallocate(Gauss%co_ip_subd)
        endif
        if(allocated(meshdata%enrich_node))then
            deallocate(meshdata%enrich_node)
        endif
        if(allocated(meshdata%subdivision))then
            deallocate(meshdata%subdivision)
        endif
    end subroutine deallocate_variables

    subroutine get_co(area,elemid,et,gz,xco,yco)!積分点のグローバル座標を算出
        implicit none
        type(area_struc) :: area
        integer(4),intent(in) :: elemid
        real(8),intent(in) :: et,gz
        real(8),intent(out) :: xco,yco

        integer(4) :: i,nodeid(4)
        real(8) :: N(4)

        !要素に対応する節点番号
        do i = 1,4
            nodeid(i) = area%elem_node_id(i,elemid)
        enddo

        !要素の形状関数
        N(1) = (1-gz)*(1-et)/4
        N(2) = (1+gz)*(1-et)/4
        N(3) = (1+gz)*(1+et)/4
        N(4) = (1-gz)*(1+et)/4

        !求めるx,y座標の算出
        xco = 0.0d0
        yco = 0.0d0
        do i = 1,4
            xco = xco + N(i) * area%co_node(1,nodeid(i))
            yco = yco + N(i) * area%co_node(2,nodeid(i))
        enddo
    end subroutine get_co

    subroutine select_subdivision()!細分化メッシュ決定
        implicit none
        integer(4) :: i
        real(8) :: xco1,yco1,xco2,yco2,r1,r2,xcoip1,ycoip1,et,gz
        dbg_name = 'select_subdivision'
        call dbg_start_subroutineM()
        meshdata%subdivision = .false.
        !細分化するメッシュ選定
        do i = 1, Garea%Nelem
            et = Gauss%co_ip_subd(1,1)
            gz = Gauss%co_ip_subd(2,1)
            call get_co(Garea,i,et,gz,xco1,yco1)
            r1 = sqrt(xco1**2 + yco1**2)

            et = Gauss%co_ip_subd(1,Gauss%Nip_subd-1)
            gz = Gauss%co_ip_subd(2,Gauss%Nip_subd-1)
            call get_co(Garea,i,et,gz,xco2,yco2) !任意のn_recに対応済み,積分点は4を想定
            r2 = sqrt(xco2**2 + yco2**2)
            
            if(r1 < meshdata%lengR .and. meshdata%lengR <= r2)then
                meshdata%subdivision(i) = .true.
            endif
        enddo


    end subroutine select_subdivision

    subroutine select_Xnode()!追加の物理量を付与する節点の選定
        implicit none
        integer(4) :: i,j,k,elemid,nodeid
        integer(4),allocatable :: int_array(:)
        real(8) :: xco1,yco1,time1
        dbg_name = 'select_Xnode'
        call dbg_start_subroutineM()

        allocate(meshdata%enrich_node(Garea%Nnode))
        meshdata%enrich_node = .false.

        !物理量を追加する節点の選定
        do elemid = 1,Garea%Nelem
            if(meshdata%subdivision(elemid))then
                do i = 1,4
                    meshdata%enrich_node(Garea%elem_node_id(i,elemid)) = .true.
                enddo 
            endif
        enddo

        allocate(int_array(Garea%Nnode))
        int_array = 0
        do i = 1, Garea%Nnode
            if(meshdata%enrich_node(i))then
                int_array(i) = 1
            endif
        enddo   
        call monolis_mpi_update_I(com, 1, int_array, time1)

        meshdata%enrich_node = .false.
        do i = 1, Garea%Nnode
            if(int_array(i) == 1)then
                meshdata%enrich_node(i) = .true.
            endif
        enddo 

        !物理量を追加する節点数の記録
        do nodeid = 1,Garea%Nnode
            if(meshdata%enrich_node(nodeid))then
                meshdata%Nenrich = meshdata%Nenrich + 1!Nenrichは追加の物理量を実際に付与する節点数
            endif
        enddo

        !デバッグ
        ! write(*,*)'Nenrich= ',meshdata%Nenrich,'Nnode= ',Garea%Nnode,'cost= '&
        ! ,(meshdata%Nenrich*3+Garea%Nnode)

        meshdata%Nconstrain = Garea%Nnode - meshdata%Nenrich!Nconstrainは追加の物理量を実際には付与しない(0で固定)する節点数

    end subroutine select_Xnode

    subroutine read_files()!elem.dat,node.datファイルを読み取りmeshdata%を初期化
        implicit none
        real(8) :: x,y,z,ratioR, t1, t2
        integer(4) :: i,j,numelem=10,numnode=11,numinput = 12
        character*128 :: fname

        ! t1 = monolis_get_time()

        fname = monolis_get_global_input_file_name(MONOLIS_DEFAULT_TOP_DIR, MONOLIS_DEFAULT_PART_DIR, "elem.dat")
        open(numelem,file=fname,status="old")!elem.datの読み取り
        read(numelem,*)Garea%Nelem,i
        allocate(Garea%elem_node_id(4,Garea%Nelem))
        do i = 1,Garea%Nelem
            read(numelem,*)Garea%elem_node_id(1,i),Garea%elem_node_id(2,i),Garea%elem_node_id(3,i),Garea%elem_node_id(4,i)
        enddo
        close(numelem)

        fname = monolis_get_global_input_file_name(MONOLIS_DEFAULT_TOP_DIR, MONOLIS_DEFAULT_PART_DIR, "node.dat")
        open(numnode,file=fname,status="old")!node.datファイルの読み取り
        ! Garea%Nnode = Garea%elem_node_id(3,Garea%Nelem)
        read(numnode,*)Garea%Nnode
        allocate(Garea%co_node(2,Garea%Nnode))

        do i = 1,Garea%Nnode
            read(numnode,*)Garea%co_node(1,i),Garea%co_node(2,i),x
        enddo
        close(numnode)

        open(numinput,file = 'input.dat',status="old")
        read(numinput,*)meshdata%lengL
        read(numinput,*)meshdata%leng_mesh
        read(numinput,*)meshdata%diviL
        read(numinput,*)meshdata%lengR
        close(numinput)

        ! meshdata%lengL = Garea%co_node(1,Garea%Nnode)
        ! meshdata%leng_mesh = Garea%co_node(1,2)
        ! meshdata%diviL = NINT(meshdata%lengL/meshdata%leng_mesh)
        ! meshdata%lengR = meshdata%lengL*ratioR!ratioRはグローバル変数

        meshdata%Nenrich = 0
        allocate(meshdata%subdivision(Garea%Nelem))

! t2 = monolis_get_time()
! write(*,*) t2 - t1
    end subroutine read_files

    ! subroutine initialize_monolis_before_mod04()
    !     implicit none
    !     integer(4) :: nbase_func = 4

    !     dbg_name = 'initialize monolis側の変数'
    !     call dbg_start_subroutineM()

    !     ! !mod04以降でmonolisを使用
    !     ! call monolis_global_initialize()
    !     ! call monolis_initialize(monost)!monolis構造体の初期化

    !     ! !通信データ構造体の初期化処理
    !     ! call monolis_com_initialize_by_parted_files(com,comm,top_dir_name,part_dir_name,file_name)

    !     ! fname = monolis_get_global_input_file_name(MONOLIS_DEFAULT_TOP_DIR, MONOLIS_DEFAULT_PART_DIR, "node.dat")
    !     ! call monolis_input_node(fname, Garea%nnode, Garea%co_node)

    !     ! fname = monolis_get_global_input_file_name(MONOLIS_DEFAULT_TOP_DIR, MONOLIS_DEFAULT_PART_DIR, "elem.dat")
    !     ! call monolis_input_elem(fname, Garea%Nelem, nbase_func, Garea%elem_node_id)

    ! end subroutine initialize_monolis_before_mod04




end module initialize