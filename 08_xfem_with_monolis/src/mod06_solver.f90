module CGsolver
    use dbg
    use variables
    use initialize
    use makeK
    implicit none

    contains

    subroutine CGsolver_main_withM()
        implicit none
        integer(4) :: i
        integer(4), allocatable :: n_dof_list(:)
        real(8) :: t1,t2

        call monolis_set_method(monost,1)
        call monolis_set_precond(monost,1)
        call monolis_set_maxiter(monost,10000)!最大反復回数
        call monolis_set_tolerance(monost,1.0d-8)!収束閾値(1.0d-8は10の-8乗)
        call monolis_show_timelog(monost, .false.)!時間のログ表示
        call monolis_show_iterlog(monost, .false.)!反復回数のログ表示
        call monolis_show_summary(monost, .true.)!数値解概要のログ表示

        t1 = monolis_get_time_global_sync()
        ! t1 = monolis_get_time()

        !call monolis_solve_R(monost,com,solver%Fvec,solver%uvec)![K]{u}={f}を解く

        allocate(n_dof_list(Garea%Nnode))
        !> 節点の自由度を入れる
        ! n_dof_list = 4
        n_dof_list = 2
        do i =1,Garea%Nnode
            if(meshdata%enrich_node(i))then
                n_dof_list(i) = 4
            endif
        enddo

       call monolis_solve_V_R(monost, com, solver%Fvec, solver%uvec, n_dof_list)![K]{u}={f}を解く

        t2 = monolis_get_time_global_sync()
        ! t2 = monolis_get_time()

        t_solver = t2-t1

        call monolis_finalize(monost)!monolisの終了処理

        ! write(numVTK, '(a,I0)') 'POINT_DATA ',Garea%Nnode
        ! write(numVTK, '(a)')'SCALARS Xnode_Data float'  
        ! write(numVTK, '(a)')'LOOKUP_TABLE default'
        ! do i = 1, Garea%Nnode
        !     if(meshdata%enrich_node(i))then
        !     write(numVTK,'(i0)')1
        !     else
        !     write(numVTK,'(i0)')0
        !     endif
        ! enddo

    end subroutine CGsolver_main_withM

    subroutine mod06_CGsolver_withM()
        implicit none
        integer(4) :: i,j
        dbg_name = 'mod06_CGsolver'
        call dbg_start_subroutineL()

        call CGsolver_main_withM()
        i = monolis_mpi_get_global_my_rank()
        j = monolis_mpi_get_global_comm_size()
        if(i == 0)then
            write(*,*)'並列数,','行列生成時間,','ソルバ時間'
            write(*,*)j,',',t_makeK,',',t_solver
            ! write(*,*)',剛性行列生成時間=',t_makeK,'ソルバ時間=,',t_solver
        endif
    end subroutine mod06_CGsolver_withM



end module CGsolver