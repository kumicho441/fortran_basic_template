program main
    !use
    use mod_monolis
    use dbg
    use variables
    use initialize
    use makeMesh
    use makeK
    use insert_BC
    use CGsolver
    use addXtoG
    use evaluate
    use paraview
    use finalize

    implicit none

    !call
    call monolis_global_initialize()

    ! !最終的にこちらを実行
    call mod00_debug()

    call deallocate_variables()
    call read_files()
    call Emesher()
    call Hmesher()
    call initialize_eval()

    call mod08_L2norm()!実行前にsolver%ucalを読み取る必要がある
    call mod09_vis_outcome()
    call mod10_show_outcome()
    call monolis_global_finalize()!monolisの並列計算に関する終了処理




    !mpiに関するテスト
    ! call monolis_mpi_initialize()
    ! write(*,*)monolis_mpi_get_global_comm_size()
    ! call monolis_mpi_finalize()






end program main