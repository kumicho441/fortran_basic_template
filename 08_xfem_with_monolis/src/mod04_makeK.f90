module makeK
    use dbg
    use variables
    use initialize
    use makeMesh
    implicit none

    logical :: Xmode = .false.
    real(8) :: t_makeK,t_solver
    !CHARACTER(*), PARAMETER :: top_dir_name = 'top_dir_name'!04
    !CHARACTER(*), PARAMETER :: part_dir_name = 'part_dir_name'!04
    !CHARACTER(*), PARAMETER :: file_name = 'file_name'!04
    contains



    subroutine makeD()
        implicit none
        matrix%Dmat(1,1) = 1/(1-poisson)
        matrix%Dmat(2,2) = 1/(1-poisson)
        matrix%Dmat(1,2) = poisson/(1-poisson)
        matrix%Dmat(2,1) = poisson/(1-poisson)
        matrix%Dmat(3,3) = 0.5
        matrix%Dmat = matrix%Dmat * young/(1+poisson)
    end subroutine makeD

    subroutine makeB(area,et,gz,elemid)
        implicit none
        type(area_struc) :: area
        real(8),intent(in) :: et,gz
        integer(4) :: elemid,i,j
        real(8) :: xco,yco

        !Jmat
        matrix%mat24(1,1) = -(1-et)/4
        matrix%mat24(1,2) =  (1-et)/4
        matrix%mat24(1,3) =  (1+et)/4
        matrix%mat24(1,4) = -(1+et)/4
        matrix%mat24(2,1) = -(1-gz)/4
        matrix%mat24(2,2) = -(1+gz)/4
        matrix%mat24(2,3) =  (1+gz)/4
        matrix%mat24(2,4) =  (1-gz)/4
        
        do i = 1,4
            matrix%mat42_m(i,1) = area%co_node(1,area%elem_node_id(i,elemid))
            matrix%mat42_m(i,2) = area%co_node(2,area%elem_node_id(i,elemid))
        enddo
        
        matrix%Jmat = matmul(matrix%mat24, matrix%mat42_m)
        
        !detJ
        matrix%detJ = matrix%Jmat(1,1)*matrix%Jmat(2,2) - matrix%Jmat(1,2)*matrix%Jmat(2,1)
        
        !Jinv
        matrix%Jinv(1,1) =  matrix%Jmat(2,2)/matrix%detJ
        matrix%Jinv(1,2) = -matrix%Jmat(1,2)/matrix%detJ
        matrix%Jinv(2,1) = -matrix%Jmat(2,1)/matrix%detJ
        matrix%Jinv(2,2) =  matrix%Jmat(1,1)/matrix%detJ
        
        !Bmat
        matrix%mat24_m(1,1) = -(1-et)/4
        matrix%mat24_m(2,1) = -(1-gz)/4
        matrix%mat24_m(1,2) = (1-et)/4
        matrix%mat24_m(2,2) = -(1+gz)/4
        matrix%mat24_m(1,3) = (1+et)/4
        matrix%mat24_m(2,3) = (1+gz)/4
        matrix%mat24_m(1,4) = -(1+et)/4
        matrix%mat24_m(2,4) = (1-gz)/4
        
        matrix%mat24_u = 0.0d0
        matrix%mat24_u = matmul(matrix%Jinv, matrix%mat24_m)
        
        matrix%Bmat = 0.0d0

        call get_co(area,elemid,et,gz,xco,yco)

        if(Xmode .and. sqrt(xco**2 + yco**2) <= meshdata%lengR)then
            do i = 1,4
                matrix%Bmat(1,(2*i-1)) = -matrix%mat24_u(1,i)
                matrix%Bmat(3,2*i)     = -matrix%mat24_u(1,i)
                matrix%Bmat(2,2*i)     = -matrix%mat24_u(2,i)
                matrix%Bmat(3,(2*i-1)) = -matrix%mat24_u(2,i)
            enddo
        else
            do i = 1,4
                matrix%Bmat(1,(2*i-1)) = matrix%mat24_u(1,i)
                matrix%Bmat(3,2*i)     = matrix%mat24_u(1,i)
                matrix%Bmat(2,2*i)     = matrix%mat24_u(2,i)
                matrix%Bmat(3,(2*i-1)) = matrix%mat24_u(2,i)
            enddo
        endif
        
        !TraB
        do i = 1,3
            do j = 1,8
                matrix%BTmat(j,i) = matrix%Bmat(i,j)
            enddo
        enddo

    end subroutine makeB

    subroutine makeKe_single(area, elemid) ! G領域、X領域の要素剛性行列生成
        implicit none
        type(area_struc) :: area
        integer(4), intent(in) :: elemid
        integer(4) :: ip
        real(8) :: et, gz, xco, yco
    
        matrix%mat88_u = 0.0d0
    
        do ip = 1, Gauss%Nip
    
            et = Gauss%co_ip(1, ip)
            gz = Gauss%co_ip(2, ip)
    
            call makeB(area, et, gz, elemid)
    
            matrix%mat88 = 0.0d0 ! Keを作るための情報はmat88に保存している
            matrix%mat38 = 0.0d0
    
            matrix%mat38 = matmul(matrix%Dmat, matrix%Bmat)
            matrix%mat88 = matmul(matrix%BTmat, matrix%mat38) * matrix%detJ * Gauss%weight
    
            matrix%mat88_u = matrix%mat88_u + matrix%mat88
    
        enddo
    end subroutine makeKe_single
    
    subroutine makeKe_single_subd(area, elemid) ! G領域、L領域の要素剛性行列生成
        implicit none
        type(area_struc) :: area
        integer(4), intent(in) :: elemid
        integer(4) :: ip
        real(8) :: et, gz, xco, yco
    
        matrix%mat88_u = 0.0d0
    
        do ip = 1, Gauss%Nip_subd
    
            et = Gauss%co_ip_subd(1, ip)
            gz = Gauss%co_ip_subd(2, ip)
    
            call makeB(area, et, gz, elemid)
    
            matrix%mat88 = 0.0d0
            matrix%mat38 = 0.0d0
    
            matrix%mat38 = matmul(matrix%Dmat, matrix%Bmat)
            matrix%mat88 = matmul(matrix%BTmat, matrix%mat38) * matrix%detJ * Gauss%weight_subd ! Keを作るための情報はmat88に保存している
    
            matrix%mat88_u = matrix%mat88_u + matrix%mat88
    
        enddo
    end subroutine makeKe_single_subd
    

    subroutine makeKGe(elemid)!G領域
        implicit none
        integer(4),intent(in) :: elemid
        integer(4) :: i,j,n,m,c1,c2

        Xmode = .false.

            matrix%mat88_u = 0.0d0
        
            !細分化処理
            if(meshdata%subdivision(elemid))then
                call makeKe_single_subd(Garea,elemid) !要素が境界をまたぐ場合のガウス積分
            else !またがない場合のガウス積分
                call makeKe_single(Garea,elemid)
            endif
        
            !Kemat(16,16)へ足しこみ
            do i = 1, 4
                n = Garea%elem_node_id(i, elemid)
                do j = 1, 4
                    m = Garea%elem_node_id(j, elemid)

                    c1 = 4*i
                    c2 = 4*j
        
                    matrix%Kemat(c1-3, c2-3) = matrix%Kemat(c1-3, c2-3) + matrix%mat88_u(2*i-1, 2*j-1)
                    matrix%Kemat(c1-3, c2-2) = matrix%Kemat(c1-3, c2-2) + matrix%mat88_u(2*i-1, 2*j  )
                    matrix%Kemat(c1-2, c2-3) = matrix%Kemat(c1-2, c2-3) + matrix%mat88_u(2*i  , 2*j-1)
                    matrix%Kemat(c1-2, c2-2) = matrix%Kemat(c1-2, c2-2) + matrix%mat88_u(2*i  , 2*j  )
                enddo
            enddo

    end subroutine makeKGe

    subroutine makeKXe(elemid)!G領域、X領域の剛性行列生成
        implicit none
        integer(4),intent(in) :: elemid
        integer(4) :: i,j,n,m,c1,c2
        integer(4) :: connectivity(4)!コネクティビティを表す変数

        Xmode = .true.

            matrix%mat88_u = 0.0d0
        
            !細分化処理
            if(meshdata%subdivision(elemid))then
                call makeKe_single_subd(Garea,elemid) !要素が境界をまたぐ場合のガウス積分
            else !またがない場合のガウス積分
                call makeKe_single(Garea,elemid)
            endif
        
            !Kemat(16,16)へ足しこみ
            do i = 1, 4
                n = Garea%elem_node_id(i, elemid)
                do j = 1, 4
                    m = Garea%elem_node_id(j, elemid)

                    c1 = 4*i
                    c2 = 4*j
        
                    matrix%Kemat(c1-1, c2-1) = matrix%Kemat(c1-1, c2-1) + matrix%mat88_u(2*i-1, 2*j-1)
                    matrix%Kemat(c1-1, c2  ) = matrix%Kemat(c1-1, c2  ) + matrix%mat88_u(2*i-1, 2*j  )
                    matrix%Kemat(c1  , c2-1) = matrix%Kemat(c1  , c2-1) + matrix%mat88_u(2*i  , 2*j-1)
                    matrix%Kemat(c1  , c2  ) = matrix%Kemat(c1  , c2  ) + matrix%mat88_u(2*i  , 2*j  )
                enddo
            enddo

    end subroutine makeKXe

    subroutine makeKGXe(elemid) ! elemidはXmeshのものであることに注意
        integer(4), intent(in) :: elemid
        real(8) :: et, gz, mat38(3,8), mat83(8,3)
        integer(4) :: ip
    
        matrix%mat88_u = 0.0d0
        do ip = 1, Gauss%Nip
    
            et = Gauss%co_ip(1, ip)
            gz = Gauss%co_ip(2, ip)
    
            Xmode = .false.
            call makeB(Garea, et, gz, elemid)
            mat83 = matrix%BTmat
            Xmode = .true.
            call makeB(Garea, et, gz, elemid)
            mat38 = matmul(matrix%Dmat, matrix%Bmat)
            matrix%mat88 = matmul(mat83, mat38) * matrix%detJ * Gauss%weight
            matrix%mat88_u = matrix%mat88_u + matrix%mat88
    
        enddo
    
    end subroutine makeKGXe
    
    subroutine makeKGXe_subd(elemid) ! elemidはXmeshのものであることに注意
        integer(4), intent(in) :: elemid
        real(8) :: et, gz, mat38(3,8), mat83(8,3)
        integer(4) :: ip
    
        matrix%mat88_u = 0.0d0
        do ip = 1, Gauss%Nip_subd
    
            et = Gauss%co_ip_subd(1, ip)
            gz = Gauss%co_ip_subd(2, ip)
    
            Xmode = .false.
            call makeB(Garea, et, gz, elemid)
            mat83 = matrix%BTmat
            Xmode = .true.
            call makeB(Garea, et, gz, elemid)
            mat38 = matmul(matrix%Dmat, matrix%Bmat)
            matrix%mat88 = matmul(mat83, mat38) * matrix%detJ * Gauss%weight_subd
            matrix%mat88_u = matrix%mat88_u + matrix%mat88
    
        enddo
    
    end subroutine makeKGXe_subd
    

    subroutine makeKGXe_comp(elemid)!連成項の作成
        implicit none
        integer(4),intent(in) :: elemid
        integer(4) :: i,j,n,m,c1,c2

            matrix%mat88_u = 0.0d0

            !細分化処理
            if(meshdata%subdivision(elemid))then 
                    call makeKGXe_subd(elemid)!要素が境界をまたぐ場合のガウス積分
            else
                    call makeKGXe(elemid) !またがない場合のガウス積分
            endif

            !Kemat(16,16)へ足しこみ
            do i = 1,4
                n = Garea%elem_node_id(i,elemid)
                do j = 1,4
                    m = Garea%elem_node_id(j,elemid)
                    c1 = 4*i
                    c2 = 4*j

                    matrix%Kemat(c1-3, c2-1) = matrix%Kemat(c1-3, c2-1) + matrix%mat88_u(2*i-1, 2*j-1)
                    matrix%Kemat(c1-3, c2  ) = matrix%Kemat(c1-3, c2  ) + matrix%mat88_u(2*i-1, 2*j  )
                    matrix%Kemat(c1-2, c2-1) = matrix%Kemat(c1-2, c2-1) + matrix%mat88_u(2*i  , 2*j-1)
                    matrix%Kemat(c1-2, c2  ) = matrix%Kemat(c1-2, c2  ) + matrix%mat88_u(2*i  , 2*j  )

                    !KXGeも足しこむ
                    matrix%Kemat(c2-1, c1-3) = matrix%Kemat(c2-1, c1-3) + matrix%mat88_u(2*i-1, 2*j-1)
                    matrix%Kemat(c2  , c1-3) = matrix%Kemat(c2  , c1-3) + matrix%mat88_u(2*i-1, 2*j  )
                    matrix%Kemat(c2-1, c1-2) = matrix%Kemat(c2-1, c1-2) + matrix%mat88_u(2*i  , 2*j-1)
                    matrix%Kemat(c2  , c1-2) = matrix%Kemat(c2  , c1-2) + matrix%mat88_u(2*i  , 2*j  )
                enddo
            enddo
            matrix%mat88 = 0.0d0

    end subroutine makeKGXe_comp

    ! subroutine makeJ(area,et,gz,elemid)!mod08で座標変換に使用
    !     implicit none
    !     type(sub_area_struc) :: area
    !     real(8),intent(in) :: et,gz
    !     integer(4) :: elemid,i,j
    !     real(8) :: xco,yco

    !     !Jmat
    !     matrix%mat24(1,1) = -(1-et)/4
    !     matrix%mat24(1,2) =  (1-et)/4
    !     matrix%mat24(1,3) =  (1+et)/4
    !     matrix%mat24(1,4) = -(1+et)/4
    !     matrix%mat24(2,1) = -(1-gz)/4
    !     matrix%mat24(2,2) = -(1+gz)/4
    !     matrix%mat24(2,3) =  (1+gz)/4
    !     matrix%mat24(2,4) =  (1-gz)/4
        
    !     do i = 1,4
    !         matrix%mat42_m(i,1) = area%co_node(1,area%elem_node_id(i,elemid))
    !         matrix%mat42_m(i,2) = area%co_node(2,area%elem_node_id(i,elemid))
    !     enddo
        
    !     matrix%Jmat = matmul(matrix%mat24, matrix%mat42_m)
        
    !     !detJ
    !     matrix%detJ = matrix%Jmat(1,1)*matrix%Jmat(2,2) - matrix%Jmat(1,2)*matrix%Jmat(2,1)

    ! end subroutine makeJ

    subroutine makeKe(elemid)
        implicit none

        integer(4) :: i
        integer(4),intent(in) :: elemid

        matrix%Kemat = 0.0d0

        !エンリッチ節点を含む場合
        if(meshdata%enrich_node(Garea%elem_node_id(1,elemid)) .or. &
        meshdata%enrich_node(Garea%elem_node_id(2,elemid)) .or. &
        meshdata%enrich_node(Garea%elem_node_id(3,elemid))  .or. &
        meshdata%enrich_node(Garea%elem_node_id(4,elemid)) )then
            call makeKGe(elemid)
            call makeKXe(elemid)
            call makeKGXe_comp(elemid)

            do i = 1,4!通常の節点をエンリッチ節点として扱う現象を防止する
                if( .not. meshdata%enrich_node(Garea%elem_node_id(i,elemid)))then
                    matrix%Kemat(:,4*i  ) = 0.0d0
                    matrix%Kemat(:,4*i-1) = 0.0d0
                    matrix%Kemat(4*i,:  ) = 0.0d0
                    matrix%Kemat(4*i-1,:) = 0.0d0
                endif
            enddo
        else
        !エンリッチ節点を含まない場合
            call makeKGe(elemid)
        endif

    end subroutine makeKe
        
    subroutine mod04_makeK_withM()
        implicit none

        real(8) :: tra(16),x,y,t1,t2
        integer(4) :: Nbase,i,j,nodeid,elemid,Ndof=4
        integer(4):: connectivity(4)
        ! integer(4) :: comm!MPIコミュニケータ
        logical :: is_find = .true.

        dbg_name = 'mode04_makeK_withM'
        call dbg_start_subroutineL()

        !補完に用いる基底(base)関数の数
        Nbase = 4

        !mod04以降でmonolisを使用
        call monolis_initialize(monost)!monolis構造体の初期化

        ! !通信データ構造体の初期化処理
        ! comm = monolis_mpi_get_global_comm()
        ! call monolis_com_initialize_by_parted_files(com,comm,MONOLIS_DEFAULT_TOP_DIR, MONOLIS_DEFAULT_PART_DIR, "node.dat")

        !非零構造の確保
        call monolis_get_nonzero_pattern_by_simple_mesh_R(&
        monost,Garea%Nnode,Nbase,Ndof,Garea%Nelem,Garea%elem_node_id)!Garea%elem_node_idがelemの役割を果たす
        call monolis_clear_mat_value_R(monost)

        !K作成
        dbg_name = 'K作成'
        call dbg_start_subroutineM()

        !K作成開始時点の時刻
        t1 = monolis_get_time_global_sync()

        call makeD()
        do elemid = 1, Garea%Nelem

            do j = 1,4!コネクティビティ作成
                connectivity(j) = Garea%elem_node_id(j,elemid)
            enddo

            call makeKe(elemid)!足しこむKeを作成
            call monolis_add_matrix_to_sparse_matrix_R(monost,Nbase,connectivity,matrix%Kemat)

        enddo
        !K作成終了時点の時刻
        t2 = monolis_get_time_global_sync()

        t_makeK = t2-t1

    end subroutine mod04_makeK_withM

end module makeK