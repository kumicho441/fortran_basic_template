module addXtoG
    use dbg
    use variables
    use initialize
    implicit none

    contains

    subroutine uG_plus_uX()
        implicit none

        integer(4) :: i,Ndof=4
        if(.not.dbg_no_addition)then!足しこみなしデバッグ

            do i = 1, Garea%Nnode
                solver%uvec(i*Ndof - 3) = solver%uvec(i*Ndof - 3) + solver%uvec(i*Ndof - 1)
                solver%uvec(i*Ndof - 2) = solver%uvec(i*Ndof - 2) + solver%uvec(i*Ndof    )
            enddo
            
        endif
    end subroutine uG_plus_uX

    subroutine makeU()
        implicit none
        integer(4) :: numU=10,Ndof=4,i
        integer(4), allocatable :: global_node_id(:)
        character*128 :: fname

        if(monolis_mpi_get_global_comm_size() == 1)then
            allocate(global_node_id(Garea%Nnode))
            do i = 1, Garea%Nnode
                global_node_id(i) = i
            enddo
        else
            !> 並列計算のとき
            fname = monolis_get_global_input_file_name(MONOLIS_DEFAULT_TOP_DIR, MONOLIS_DEFAULT_PART_DIR, "node.dat.id")
            call monolis_input_global_id(fname, i, global_node_id)
        endif

        dbg_name = 'makeU'
        call dbg_start_subroutineM()

        !uvec.dat作成
        fname = monolis_get_global_output_file_name("./", "./", "uvec.dat")
        open(unit = numU, file = fname, status = 'replace')   
        write(numU,*)Garea%Nnode
        do i =1, Garea%Nnode
            write(numU,*)global_node_id(i), solver%uvec(i*Ndof-3)  ,solver%uvec(i*Ndof-2) 
        enddo
        close(numU)

    end subroutine makeU

    ! subroutine read_uvec_dbg()!uvec.datを読み取りeval%に記録
    !     implicit none

    !     integer(4) :: i,j,k,n,Nnode,numU=10,numU2=11
    !     integer(4) :: n_files
    !     CHARACTER*128 :: fname
    !     character*128 :: arg1, cnum

    !     call getarg(1, arg1)!03_evaluaterの実行コマンドに記載された並列数を読み込む
    !     read(arg1,*) n_files!読み込んだ並列数の値を整数型に変換
    !     ! write(*,*) n_files　!読み込んだ値の表示

    !     if(n_files == 1)then
    !         !> 逐次と同様にファイルを読んでeval%solve_uvecを作る
    !         open(numU,file= 'uvec.dat',status="old")!uvec.datの読み取り
    !         read(numU,*)i
    !         do i = 1,Garea%Nnode
    !             read(numU,*)j,eval%solver_uvec(2*i-1),eval%solver_uvec(2*i)
    !         enddo
    !         close(numU)
    !     else
    !         !> 並列のファイルを読んで、逐次実行時と同じ eval%solver_uvec を作る
    !         do i = 1, n_files
    !             write(cnum,"(i0)")i-1
    !             open(numU, file = "uvec.dat."//trim(cnum), status = "old")
    !             read(numU,*)n
    !             do j = 1, n
    !                 read(numU,*)k,eval%solver_uvec(2*k-1),eval%solver_uvec(2*k)
    !             enddo
    !             close(numU)
    !         enddo
    !     endif

    ! end subroutine read_uvec_dbg

    subroutine visG_sub()
        implicit none
        integer(4) :: i,numVTK=10,Ndof=4,j
        CHARACTER*128 :: fname,moji



        dbg_name = 'visG'
        call dbg_start_subroutineM()

        j = monolis_mpi_get_global_my_rank()

        WRITE(moji, '(I0)') j

        ! fname = monolis_get_global_output_file_name("./", "./", "08_xfem"//TRIM(moji)//".vtk")
        fname = "08_xfem"//TRIM(moji)//".vtk"
        open(unit = numVTK, file = fname, status = 'replace')

        ! open(unit = numVTK, file = '08_xfem.vtk', status = 'replace')
        write(numVTK, '(a)') '# vtk DataFile Version 4.1'
        write(numVTK, '(a)') 'This is a comment line'
        write(numVTK, '(a)') 'ASCII'
        write(numVTK, '(a)') 'DATASET UNSTRUCTURED_GRID'
        write(numVTK, '(a,i0,a)') 'POINTS ',Garea%Nnode,' float'

        do i = 1,Garea%Nnode!>節点の座標を行ごとに代入する
            write(numVTK, '(G0,a,G0,a,G0)') dble(Garea%co_node(1,i)),&
            ' ',  dble(Garea%co_node(2,i)),' 0.0'
        enddo

         write(numVTK, '(a,i0,a,i0)') 'CELLS ', Garea%Nelem ,' ', 5*(Garea%Nelem)

        do i =1,Garea%Nelem!節点id入力
            write(numVTK,'(a,I0,a,I0,a,I0,a,I0)') '4 ', Garea%elem_node_id(1,i)-1,' ',&
            Garea%elem_node_id(2,i)-1,' ',&
            Garea%elem_node_id(3,i)-1,' ',&
            Garea%elem_node_id(4,i)-1
        enddo

        write(numVTK, '(a,I0)') 'CELL_TYPES ' , Garea%Nelem
        do i = 1,Garea%Nelem
            write(numVTK,'(I0)') 9
        enddo

        ! !数値解の入力
        write(numVTK, '(a,I0)') 'POINT_DATA ',Garea%Nnode
        ! write(numVTK, '(a)')'VECTORS d_cal_Vector_Data float'      
        ! do i = 1, Garea%Nnode
        !     ! write(numVTK,'(G0,a,G0,a)') solver%uvec(i*Ndof-3),' ',solver%uvec(i*Ndof-2),' 0.0'
        !     write(numVTK,'(G0,a,G0,a)') eval%solver_uvec(i*2-1),' ',eval%solver_uvec(i*2),' 0.0'
        ! enddo    

        !エンリッチ判定の入力 meshdata%enrich_nodeは
        write(numVTK, '(a)')'SCALARS Xnode_Data float'  
        write(numVTK, '(a)')'LOOKUP_TABLE default'
        do i = 1, Garea%Nnode
            if(meshdata%enrich_node(i))then
            write(numVTK,'(i0)')1
            else
            write(numVTK,'(i0)')0
            endif
        enddo

        close(numVTK)
    end subroutine visG_sub

    subroutine mod07_uG_plus_uX()
        implicit none
        integer(4) :: numU

        dbg_name = 'mod07_uG_plus_uX'
        call dbg_start_subroutineL()

        call uG_plus_uX()
        call makeU()

        call visG_sub()

        ! write(*,*)'自由度=',Garea%Nnode*2 + meshdata%Nenrich*2

    end subroutine mod07_uG_plus_uX


end module addXtoG