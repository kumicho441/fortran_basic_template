module paraview
    use dbg
    use variables
    use initialize
    implicit none

    contains

    subroutine visG()
        implicit none
        integer(4) :: i, j, in, numVTK=10,Ndof=4,numw=11
        integer(4),allocatable :: w_node(:), domain_id(:)
        character*128 :: fname
        integer(kint) :: n_internal
        integer(kint), allocatable :: global_nid(:)
        integer(4) :: n_files
        character*128 :: arg1

        call getarg(1, arg1)
        read(arg1,*) n_files!読み込んだ並列数の値を整数型に変換

        dbg_name = 'visG'
        call dbg_start_subroutineM()

        open(unit = numVTK, file = '08_xfem.vtk', status = 'replace')
        write(numVTK, '(a)') '# vtk DataFile Version 4.1'
        write(numVTK, '(a)') 'This is a comment line'
        write(numVTK, '(a)') 'ASCII'
        write(numVTK, '(a)') 'DATASET UNSTRUCTURED_GRID'
        write(numVTK, '(a,i0,a)') 'POINTS ',Garea%Nnode,' float'

        do i = 1,Garea%Nnode!>節点の座標を行ごとに代入する
            write(numVTK, '(G0,a,G0,a,G0)') dble(Garea%co_node(1,i)),&
            ' ',  dble(Garea%co_node(2,i)),' 0.0'
        enddo

         write(numVTK, '(a,i0,a,i0)') 'CELLS ', Garea%Nelem ,' ', 5*(Garea%Nelem)

        do i =1,Garea%Nelem!節点id入力
            write(numVTK,'(a,I0,a,I0,a,I0,a,I0)') '4 ', Garea%elem_node_id(1,i)-1,' ',&
            Garea%elem_node_id(2,i)-1,' ',&
            Garea%elem_node_id(3,i)-1,' ',&
            Garea%elem_node_id(4,i)-1
        enddo

        write(numVTK, '(a,I0)') 'CELL_TYPES ' , Garea%Nelem
        do i = 1,Garea%Nelem
            write(numVTK,'(I0)') 9
        enddo

        !数値解の入力
        write(numVTK, '(a,I0)') 'POINT_DATA ',Garea%Nnode
        write(numVTK, '(a)')'VECTORS d_cal_Vector_Data float'      
        do i = 1, Garea%Nnode
            ! write(numVTK,'(G0,a,G0,a)') solver%uvec(i*Ndof-3),' ',solver%uvec(i*Ndof-2),' 0.0'
            write(numVTK,'(G0,a,G0,a)') eval%solver_uvec(i*2-1),' ',eval%solver_uvec(i*2),' 0.0'
        enddo    

        !エンリッチ判定の入力 meshdata%enrich_nodeは

        call initialize_Gauss()
        call select_subdivision()
        call select_Xnode()
        write(numVTK, '(a)')'SCALARS Xnode_Data int'  
        write(numVTK, '(a)')'LOOKUP_TABLE default'
        do i = 1, Garea%Nnode
            if(meshdata%enrich_node(i))then
            write(numVTK,'(i0)')1
            else
            write(numVTK,'(i0)')0
            endif
        enddo

        !ノード重みの入力
        write(numVTK, '(a)')'SCALARS node_weight_Data int'  
        write(numVTK, '(a)')'LOOKUP_TABLE default'

        open(unit = numw, file = 'node_weight.dat', status = 'old')
        allocate(w_node(Garea%Nnode))
        read(numw,*)fname!fnameはダミーインデックス
        read(numw,*)i,j
        do i = 1, Garea%Nnode
            read(numw,*)w_node(i)
        enddo
        close(numw)

        do i = 1, Garea%Nnode
            write(numVTK,'(i0)')w_node(i)
        enddo

        !> output domain id
        allocate(domain_id(Garea%Nnode))
        domain_id = 0
        !並列計算変数エラーのためコメントアウト中
        ! if(n_files > 1)then
        !   do i = 0, n_files - 1
        !     fname = monolis_get_output_file_name_by_domain_id(MONOLIS_DEFAULT_TOP_DIR, MONOLIS_DEFAULT_PART_DIR, "./node.dat.n_internal", i)
        !     call monolis_input_internal_vertex_number(fname, n_internal)
        !     fname = monolis_get_output_file_name_by_domain_id(MONOLIS_DEFAULT_TOP_DIR, MONOLIS_DEFAULT_PART_DIR, "./node.dat.id", i)
        !     call monolis_input_global_id(fname, j, global_nid)

        !     do j = 1, n_internal
        !       in = global_nid(j)
        !       domain_id(in) = i
        !     enddo

        !     deallocate(global_nid)
        !   enddo
        ! endif

        write(numVTK, '(a)')'SCALARS domain_id int'  
        write(numVTK, '(a)')'LOOKUP_TABLE default'
        do i = 1, Garea%Nnode
            write(numVTK,'(i0)')domain_id(i)
        enddo

        close(numVTK)

    end subroutine visG

    subroutine visE()
        implicit none
        integer(4) :: i,numL2=10
        dbg_name = 'visE'
        call dbg_start_subroutineM()

        !L2ノルム誤差評価用のメッシュを作成
        open(unit = numL2, file = '08_xfem_eval.vtk', status = 'replace')
        write(numL2, '(a)') '# vtk DataFile Version 4.1'
        write(numL2, '(a)') 'This is a comment line'
        write(numL2, '(a)') 'ASCII'
        write(numL2, '(a)') 'DATASET UNSTRUCTURED_GRID'
        write(numL2, '(a,i0,a)') 'POINTS ',Earea%Nnode,' float'

        do i = 1,Earea%Nnode!>節点の座標を行ごとに代入する
            write(numL2, '(G0,a,G0,a,G0)') dble(Earea%co_node(1,i)),&
            ' ',  dble(Earea%co_node(2,i)),' 0.0'
        enddo

         write(numL2, '(a,i0,a,i0)') 'CELLS ', Earea%Nelem ,' ', 5*(Earea%Nelem)

        do i =1,Earea%Nelem!節点id入力
            write(numL2,'(a,I0,a,I0,a,I0,a,I0)') '4 ', Earea%elem_node_id(1,i)-1,' ',&
            Earea%elem_node_id(2,i)-1,' ',&
            Earea%elem_node_id(3,i)-1,' ',&
            Earea%elem_node_id(4,i)-1
        enddo

        write(numL2, '(a,I0)') 'CELL_TYPES ' , Earea%Nelem
        do i = 1,Earea%Nelem
            write(numL2,'(I0)') 9
        enddo

        !数値解の入力
        write(numL2, '(a,I0)') 'POINT_DATA ',Earea%Nnode
        write(numL2, '(a)')'VECTORS d_cal_Vector_Data float'      
        do i = 1, Garea%Nnode
            write(numL2,'(G0,a,G0,a)') eval%solver_uvec(2*i-1),' ',eval%solver_uvec(2*i),' 0.0'
        enddo    
        
        ! !理論解の入力
        ! write(numL2, '(a)')'VECTORS d_th_Vector_Data float'
        ! do i = 1, Earea%Nnode
        !     write(numL2,'(G0,a,G0,a)')eval%uvec_th(2*i-1),' ',eval%uvec_th(2*i),' 0.0'
        ! enddo

        !各点におけるL2誤差
        write(numL2, '(a,I0)') 'CELL_DATA ',Earea%Nelem
        write(numL2, '(a)')'SCALARS L2_Data float'
        write(numL2, '(a)')'LOOKUP_TABLE default'
        do i = 1, Earea%Nelem !>各要素におけるL2誤差
            write(numL2,'(G0)') eval%elel2(i)
        enddo

        close(numL2)
    end subroutine visE

    subroutine visH()
        implicit none
        integer(4) :: i,numab=10

        dbg_name = 'visH'
        call dbg_start_subroutineM()

        !絶対誤差評価用のメッシュを作成
        open(unit = numab, file = '08_absolute_error.vtk', status = 'replace')
        write(numab, '(a)') '# vtk DataFile Version 4.1'
        write(numab, '(a)') 'This is a comment line'
        write(numab, '(a)') 'ASCII'
        write(numab, '(a)') 'DATASET UNSTRUCTURED_GRID'
        write(numab, '(a,i0,a)') 'POINTS ',Harea%Nnode,' float'

        do i = 1,Harea%Nnode!>節点の座標を行ごとに代入する
            write(numab, '(G0,a,G0,a,G0)') dble(Harea%co_node(1,i)),&
            ' ',  dble(Harea%co_node(2,i)),' 0.0'
        enddo

            write(numab, '(a,i0,a,i0)') 'CELLS ', Harea%Nelem ,' ', 5*(Harea%Nelem)

        do i =1,Harea%Nelem!節点id入力
            write(numab,'(a,I0,a,I0,a,I0,a,I0)') '4 ', Harea%elem_node_id(1,i)-1,' ',&
            Harea%elem_node_id(2,i)-1,' ',&
            Harea%elem_node_id(3,i)-1,' ',&
            Harea%elem_node_id(4,i)-1
        enddo

        write(numab, '(a,I0)') 'CELL_TYPES ' , Harea%Nelem
        do i = 1,Harea%Nelem
            write(numab,'(I0)') 9
        enddo
        !各点における絶対誤差
        write(numab, '(a,I0)') 'CELL_DATA ',Harea%Nelem
        write(numab, '(a)')'SCALARS L2_Data float'
        write(numab, '(a)')'LOOKUP_TABLE default'
        do i = 1, Harea%Nelem !>各要素におけるL2誤差
            write(numab,'(G0)') eval%eleEa(i)
        enddo

        close(numab)
    end subroutine visH

    subroutine mod09_vis_outcome()
        implicit none

        dbg_name = 'mod09_vis_outcome'
        call dbg_start_subroutineL()

        call visG()
        call visE()
        call visH()

    end subroutine mod09_vis_outcome

end module paraview