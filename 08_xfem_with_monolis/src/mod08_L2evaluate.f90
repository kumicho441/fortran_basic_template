module evaluate
    use dbg
    use variables
    use initialize
    implicit none

    contains

    subroutine read_uvec()!uvec.datを読み取りeval%に記録
        implicit none

        integer(4) :: i,j,k,n,Nnode,numU=10,numU2=11
        integer(4) :: n_files
        CHARACTER*128 :: fname
        character*128 :: arg1, cnum

        call getarg(1, arg1)!03_evaluaterの実行コマンドに記載された並列数を読み込む
        read(arg1,*) n_files!読み込んだ並列数の値を整数型に変換
        ! write(*,*) n_files　!読み込んだ値の表示

        if(n_files == 1)then
            !> 逐次と同様にファイルを読んでeval%solve_uvecを作る
            open(numU,file= 'uvec.dat',status="old")!uvec.datの読み取り
            read(numU,*)i
            do i = 1,Garea%Nnode
                read(numU,*)j,eval%solver_uvec(2*i-1),eval%solver_uvec(2*i)
            enddo
            close(numU)
        else
            !> 並列のファイルを読んで、逐次実行時と同じ eval%solver_uvec を作る
            do i = 1, n_files
                write(cnum,"(i0)")i-1
                open(numU, file = "uvec.dat."//trim(cnum), status = "old")
                read(numU,*)n
                do j = 1, n
                    read(numU,*)k,eval%solver_uvec(2*k-1),eval%solver_uvec(2*k)
                enddo
                close(numU)
            enddo
        endif

    end subroutine read_uvec

    subroutine Emesher()!L2ノルム誤差評価用のメッシュ
        implicit none
        integer(4) ::i,j,n,c1,c2,c3,divi
        real(8) :: th,pi,dr
        real(8),allocatable :: r(:)
        dbg_name = 'Emesher'
        call dbg_start_subroutineM()

        call initialize_Earea()!評価用メッシュの分割数は固定


        divi = 100!評価用メッシュの分割数は固定
        pi = atan(1.0d0)*4
        n = divi
        allocate(r(n+1))

        do j = 1, n+1
            th = pi*(j-1)/2/n

            !デバッグ
            ! write(*,*)th/pi * 180

            if(th <= pi/4 .or. abs(th - pi/4)<= 1.0E-08)then!分割数により稀にバグるための措置
                dr = (meshdata%lengL * sqrt(1 + tan(th)**2) - meshdata%lengR) / n
                r(1) = meshdata%lengR

                do i = 2,n+1
                    r(i) = r(i-1) + dr 
                enddo

                do i = 1, n + 1
                    Earea%co_node(1,i + (j-1)*(n + 1)) = r(i) * cos(th)
                    Earea%co_node(2,i + (j-1)*(n + 1)) = r(i) * sin(th)
                enddo

            else

                do i = 1,n + 1
                    Earea%co_node(1,i + (j-1)*(n+1)) = Earea%co_node(2,i + n*(n+1) - (j-1)*(n+1)) !>対称性を利用して座標を定義
                    Earea%co_node(2,i + (j-1)*(n+1)) = Earea%co_node(1,i + n*(n+1) - (j-1)*(n+1)) !>対称性を利用して座標を定義
                enddo

            endif
        enddo

        do j = 1,n
            do i = 1,n
                Earea%elem_node_id(1,i+n*(j-1)) = i + (n+1) * (j-1)
                Earea%elem_node_id(2,i+n*(j-1)) = Earea%elem_node_id(1,i+n*(j-1)) + 1
                Earea%elem_node_id(3,i+n*(j-1)) = i + (n+1) * j + 1
                Earea%elem_node_id(4,i+n*(j-1)) = Earea%elem_node_id(3,i+n*(j-1)) - 1
            enddo
        enddo

    end subroutine Emesher

    subroutine Hmesher()!円孔部分のメッシュ定義
        implicit none
        
        real(8) :: c1,c2,pi,dx,dy,r,c3,c4,c5
        integer(4) :: divi,i,j,n

        dbg_name = 'Hmesher'
        call dbg_start_subroutineM()

        divi = 100

        Harea%Nelem = 3 * (divi**2) 
        Harea%Nnode = 3 * divi**2 + 3 * divi + 1!(divi+1)**2 + divi* (divi*2 + 1)
        c1 = meshdata%lengR * 0.5!正方形部分一辺長さ
        c2 = meshdata%lengR!円孔長さ
        pi = 4*atan(1.0d0)

        n = divi
        allocate(Harea%elem_node_id(4,Harea%Nelem),Harea%co_node(2,Harea%Nnode))

        do i = 1,n+1
            do j = 1, n+1
                Harea%co_node(1,(n+1)*(i-1) + j) = (j-1)*c1/n
                Harea%co_node(2,(n+1)*(i-1) + j) = (i-1)*c1/n
            enddo
        enddo

        do i= 1, n+1!メッシュ座標定義
            c3 = (pi/2)/(2*n)*(i-1)

            dx = c2*cos(c3)
            dy = c2*sin(c3)

            dx = (dx - Harea%co_node(1,i*(n+1)))/n
            dy = (dy - Harea%co_node(2,i*(n+1)))/n

            do j = 1,n
                Harea%co_node(1,(n+1)**2 + (i-1)*n + j) = Harea%co_node(1,i*(n+1)) + dx*j
                Harea%co_node(2,(n+1)**2 + (i-1)*n + j) = Harea%co_node(2,i*(n+1)) + dy*j

                if(i /= n+1)then
                    Harea%co_node(1,Harea%Nnode - i*n + j) = Harea%co_node(2,(n+1)**2 + (i-1)*n + j)
                    Harea%co_node(2,Harea%Nnode - i*n + j) = Harea%co_node(1,(n+1)**2 + (i-1)*n + j)
                endif
            enddo
        enddo

        do i = 1, Harea%Nnode!メッシュ内包関係バグ予防
            if(Harea%co_node(1,i)<0.0d0)then
                Harea%co_node(1,i) = 0.0d0
            endif

            if(Harea%co_node(2,i)<0.0d0)then
                Harea%co_node(2,i) = 0.0d0
            endif
        enddo

    
        !id設定
        do j = 1, n
            do i = 1,n
                Harea%elem_node_id(1,i+n*(j-1)) = i + (n+1)*(j-1)
                Harea%elem_node_id(2,i+n*(j-1)) = Harea%elem_node_id(1,i+n*(j-1)) + 1
                Harea%elem_node_id(3,i+n*(j-1)) = i + (n+1)*j +1
                Harea%elem_node_id(4,i+n*(j-1)) = Harea%elem_node_id(3,i+n*(j-1)) -1
            enddo
        enddo

        do i = 1,n
            do j = 1,n

                if(j == 1)then
                    Harea%elem_node_id(1,n**2 + (i-1)*n+j) = (n+1)*i
                    Harea%elem_node_id(2,n**2 + (i-1)*n+j) = (n+1)**2 + n*(i-1) + j
                    Harea%elem_node_id(3,n**2 + (i-1)*n+j) = (n+1)**2 + n*i + j
                    Harea%elem_node_id(4,n**2 + (i-1)*n+j) = (n+1)*(i+1)
                else
                    Harea%elem_node_id(1,n**2 + (i-1)*n+j) = (n+1)**2 + n*(i-1) + j - 1
                    Harea%elem_node_id(2,n**2 + (i-1)*n+j) = (n+1)**2 + n*(i-1) + j    
                    Harea%elem_node_id(3,n**2 + (i-1)*n+j) = (n+1)**2 + n*i + j
                    Harea%elem_node_id(4,n**2 + (i-1)*n+j) = (n+1)**2 + n*i + j - 1
                endif

            enddo
        enddo

        do i = n+1, 2*n
            do j = 1, n

                if(j == 1)then
                    Harea%elem_node_id(1,n**2 + (i-1)*n + j) = (n+1)**2 - (i - (n+1))
                    Harea%elem_node_id(2,n**2 + (i-1)*n + j) = (n+1)**2 + n*(i-1) + j
                    Harea%elem_node_id(3,n**2 + (i-1)*n + j) = (n+1)**2 + n*i + j
                    Harea%elem_node_id(4,n**2 + (i-1)*n + j) = (n+1)**2 - (i - (n+1)) - 1
                else
                    Harea%elem_node_id(1,n**2 + (i-1)*n + j) = (n+1)**2 + n*(i-1) + j - 1
                    Harea%elem_node_id(2,n**2 + (i-1)*n + j) = (n+1)**2 + n*(i-1) + j
                    Harea%elem_node_id(3,n**2 + (i-1)*n + j) = (n+1)**2 + n*i + j
                    Harea%elem_node_id(4,n**2 + (i-1)*n + j) = (n+1)**2 + n*i + j - 1
                endif

            enddo
        enddo

    end subroutine Hmesher

    subroutine makeJ(area,et,gz,elemid)!mod08で座標変換に使用
        implicit none
        type(area_struc) :: area
        real(8),intent(in) :: et,gz
        integer(4) :: elemid,i,j
        real(8) :: xco,yco

        !Jmat
        matrix%mat24(1,1) = -(1-et)/4
        matrix%mat24(1,2) =  (1-et)/4
        matrix%mat24(1,3) =  (1+et)/4
        matrix%mat24(1,4) = -(1+et)/4
        matrix%mat24(2,1) = -(1-gz)/4
        matrix%mat24(2,2) = -(1+gz)/4
        matrix%mat24(2,3) =  (1+gz)/4
        matrix%mat24(2,4) =  (1-gz)/4
        
        do i = 1,4
            matrix%mat42_m(i,1) = area%co_node(1,area%elem_node_id(i,elemid))
            matrix%mat42_m(i,2) = area%co_node(2,area%elem_node_id(i,elemid))
        enddo
        
        matrix%Jmat = matmul(matrix%mat24, matrix%mat42_m)
        
        !detJ
        matrix%detJ = matrix%Jmat(1,1)*matrix%Jmat(2,2) - matrix%Jmat(1,2)*matrix%Jmat(2,1)

    end subroutine makeJ

    subroutine get_Gmeshid(xco,yco,Gelemid)!座標から対応するG要素を得る
        implicit none
        real(8),intent(in) :: xco,yco
        integer(4),intent(out) :: Gelemid

        integer(4) :: xid,yid

        xid = int(xco/meshdata%leng_mesh)
        yid = int(yco/meshdata%leng_mesh)

        !Gelemidエラー除去
        if(xid >= meshdata%diviL)then
            xid = meshdata%diviL-1
        endif
        if(yid >= meshdata%diviL)then
            yid = meshdata%diviL-1
        endif

        !Gelemid導出
        Gelemid = yid*meshdata%diviL + xid + 1
        ! !デバッグ
        ! write(*,*)'xid,yid=',xid,yid
    end subroutine get_Gmeshid

    subroutine get_etgz(xco,yco,Gelemid,et_g,gz_g)!座標とG要素番号からG要素におけるet,gzの値を得る
        implicit none
        real(8),intent(in) :: xco,yco
        integer(4),intent(in) :: Gelemid
        real(8),intent(out) :: et_g,gz_g

        real(8) :: x0,y0

        x0 = Garea%co_node(1,Garea%elem_node_id(1,Gelemid))
        y0 = Garea%co_node(2,Garea%elem_node_id(2,Gelemid))

        gz_g = 2*(xco - x0)/meshdata%leng_mesh - 1
        et_g = 2*(yco - y0)/meshdata%leng_mesh - 1
    end subroutine get_etgz

    ! subroutine trans_mesh()!格子状メッシュの変位を誤差評価用メッシュに移す
    !     implicit none
    !     real(8) :: xco,yco,N(4),et_g,gz_g
    !     integer(4) :: i,c1,nodeid,Gelemid,Ndof=4

    !     if(.not. dbg_not_exchange_Emesh)then


    !         dbg_name = 'trans_mesh'
    !         call dbg_start_subroutineM()

    !         !E領域全節点で繰り返す
    !         do nodeid = 1, Earea%Nnode

    !             !節点座標入手
    !             xco = Earea%co_node(1,nodeid)
    !             yco = Earea%co_node(2,nodeid)

    !             !対応するG要素のetgz入手
    !             call get_Gmeshid(xco,yco,Gelemid)

    !             ! !デバッグ
    !             ! write(*,*)'nodeid,Gelemid=',nodeid,Gelemid

    !             ! !デバッグ
    !             ! write(*,*)nodeid

    !             call get_etgz(xco,yco,Gelemid,et_g,gz_g)

    !             !デバッグ
    !             ! write(*,*)'nodeid,et_g,gz_g=',nodeid,et_g,gz_g

    !             !形状関数の値を算出する
    !             N(1) = (1 - gz_g) * (1 - et_g) / 4
    !             N(2) = (1 + gz_g) * (1 - et_g) / 4
    !             N(3) = (1 + gz_g) * (1 + et_g) / 4
    !             N(4) = (1 - gz_g) * (1 + et_g) / 4

    !             do i = 1,4
    !                 eval%ucal(nodeid*2 - 1) = eval%ucal(nodeid*2 - 1) + &
    !                 ! N(i)*solver%uvec(Garea%elem_node_id(i,Gelemid)*Ndof - 3)!従来のプログラム
    !                 N(i)*eval%solver_uvec(Garea%elem_node_id(i,Gelemid)*2 - 1)

    !                 eval%ucal(nodeid*2    ) = eval%ucal(nodeid*2    ) + &
    !                 ! N(i)*solver%uvec(Garea%elem_node_id(i,Gelemid)*Ndof - 2)
    !                 N(i)*eval%solver_uvec(Garea%elem_node_id(i,Gelemid)*2    )
    !             enddo

    !         enddo

    !     endif
    ! end subroutine trans_mesh

    ! subroutine transmesh_th()!誤差評価用メッシュでの理論解を求める
    !     implicit none
    !     integer(4) :: i,j,k,n,c1,Nbc,Ndim,Nid,dim
    !     real(8) :: x,ka,mu,ra,th,xco,yco,pi,c3

    !     dbg_name = 'transmesh_th'
    !     call dbg_start_subroutineM()


    !     c1 = meshdata%diviL!Gmesh一辺分割数

    !     pi = 4*atan(1.0d0)
    !     ka = (3-poisson)/(1+poisson)
    !     mu = young/(2*(1+poisson))
    !     c3 = sig_inf*meshdata%lengR/(8*mu)
        
    !     do i = 1,Earea%Nnode
    !         !デバッグ
    !         ! write(*,*)'Enodeid = ',i

    !         xco = Earea%co_node(1,i)
    !         yco = Earea%co_node(2,i)
    !         ra = sqrt(xco**2 + yco**2)

    !         if(xco <= 1.0E-08)then!atan=∞となる場合の処理
    !             th = pi/2
    !         else
    !             th = atan(yco/xco)
    !         endif

    !         eval%uvec_th(i*2-1) = c3*(ra*(ka+1)*cos(th)/meshdata%lengR + &
    !                             2*meshdata%lengR*((1+ka)*cos(th) + cos(3*th))/ra - &
    !                             2*(meshdata%lengR**3)*cos(3*th)/(ra**3))

    !         eval%uvec_th(i*2  ) = c3*(ra*(ka-3)*sin(th)/meshdata%lengR + &
    !                             2*meshdata%lengR*((1-ka)*sin(th) + sin(3*th))/ra - &
    !                             2*(meshdata%lengR**3)*sin(3*th)/(ra**3))

    !         if(ra < meshdata%lengR)then
    !             eval%uvec_th(i*2-1) = 0.0d0
    !             eval%uvec_th(i*2) = 0.0d0
    !         endif

    !         ! !デバッグ
    !         ! write(*,*)solver%uth(i*2-1),solver%uth(i*2)
    !     enddo        

    ! end subroutine transmesh_th

    subroutine makeL2_sub(ip,elemid)!誤差評価用メッシュでのL2ノルム誤差評価(1/2)
        implicit none
        integer(4),intent(in) :: ip,elemid

        real(8) :: et,gz,pi,ka,mu,et_g,gz_g
        real(8) :: N(4),ipxco,ipyco!ipは積分点を表す
        real(8) :: ux_th,uy_th,ux_cal,uy_cal,x,y
        real(8) :: ra,ur,th,c1,c2,c3
        integer(4) :: i,Gelemid,Ndof=4

        et = Gauss%co_ip(1,ip)
        gz = Gauss%co_ip(2,ip)
        pi = 4*atan(1.0d0)
        ka = (3-poisson)/(1+poisson)
        mu = young/(2*(1+poisson))
        c3 = sig_inf*meshdata%lengR/(8*mu)

        ipxco = 0.0d0
        ipyco = 0.0d0
        ux_cal = 0.0d0
        uy_cal = 0.0d0

        N(1) = (1 - gz) * (1 - et) / 4
        N(2) = (1 + gz) * (1 - et) / 4
        N(3) = (1 + gz) * (1 + et) / 4
        N(4) = (1 - gz) * (1 + et) / 4  

        do i = 1,4
            ipxco = ipxco + N(i)*Earea%co_node(1,Earea%elem_node_id(i,elemid))!積分点のx座標
            ipyco = ipyco + N(i)*Earea%co_node(2,Earea%elem_node_id(i,elemid))!積分点のy座標
        enddo

        ra = sqrt(ipxco**2 + ipyco**2)
        if(ipxco <= 1.0E-08)then!atan=∞となる場合の処理
            th = pi/2
        else
            th = atan(ipyco/ipxco)
        endif

        ux_th = c3*(ra*(ka+1)*cos(th)/meshdata%lengR + &
                                2*meshdata%lengR*((1+ka)*cos(th) + cos(3*th))/ra - &
                                2*(meshdata%lengR**3)*cos(3*th)/(ra**3))

        uy_th = c3*(ra*(ka-3)*sin(th)/meshdata%lengR + &
                                2*meshdata%lengR*((1-ka)*sin(th) + sin(3*th))/ra - &
                                2*(meshdata%lengR**3)*sin(3*th)/(ra**3))


        call get_Gmeshid(ipxco,ipyco,Gelemid)
        call get_etgz(ipxco,ipyco,Gelemid,et_g,gz_g)

        N(1) = (1 - gz_g) * (1 - et_g) / 4
        N(2) = (1 + gz_g) * (1 - et_g) / 4
        N(3) = (1 + gz_g) * (1 + et_g) / 4
        N(4) = (1 - gz_g) * (1 + et_g) / 4  

        do i = 1,4
            !従来
            ! ux_cal = ux_cal + N(i) * solver%uvec(Garea%elem_node_id(i,Gelemid)*Ndof - 3)
            ! uy_cal = uy_cal + N(i) * solver%uvec(Garea%elem_node_id(i,Gelemid)*Ndof - 2)
            ux_cal = ux_cal + N(i) * eval%solver_uvec(Garea%elem_node_id(i,Gelemid)*2 - 1)
            uy_cal = uy_cal + N(i) * eval%solver_uvec(Garea%elem_node_id(i,Gelemid)*2    )
        enddo
        
        call makeJ(Earea,et,gz,elemid)
        x = ((ux_cal - ux_th) ** 2 + (uy_cal - uy_th) ** 2) * matrix%detJ
        y = (ux_th ** 2 + uy_th ** 2) * matrix%detJ

        eval%dif_nu = eval%dif_nu + x !要素のL2誤差(分子)
        eval%dif_de = eval%dif_de + y !要素のL2誤差(分母)

        eval%L2_nu = eval%L2_nu + x
        eval%L2_de = eval%L2_de + y
    end subroutine makeL2_sub

    subroutine makeL2_main()!誤差評価用メッシュでのL2ノルム誤差評価(2/2)
        implicit none

        real(8) :: x,ra,th,ur,c1,c2
        integer(4) :: elemid,i

        dbg_name ='makeL2_main'
        call dbg_start_subroutineM

        eval%L2 = 0.0d0

        do elemid = 1, Earea%Nelem
            eval%dif_nu = 0.0d0
            eval%dif_de = 0.0d0
            
            do i = 1,Gauss%Nip
                call makeL2_sub(i,elemid)
            enddo

            eval%elel2(elemid) = sqrt(eval%dif_nu/eval%dif_de)
        enddo

        eval%L2 = sqrt(eval%L2_nu)/sqrt(eval%L2_de)

    end subroutine makeL2_main

    subroutine makeEa_sub(elemid)!誤差評価メッシュを用いた絶対誤差評価(1/2)
        implicit none
        integer(4),intent(in) :: elemid
        real(8) :: et,gz,et_g,gz_g,N(4),ipxco,ipyco,ux_th,uy_th,ux_cal,uy_cal,x,y
        integer(4) :: i,Gelemid,ip,Ndof=4

        eval%eleEa_sub = 0.0d0

        do ip = 1,Gauss%Nip
            et = Gauss%co_ip(1,ip)
            gz = Gauss%co_ip(2,ip)

            ipxco = 0.0d0
            ipyco = 0.0d0
            ux_cal = 0.0d0
            uy_cal = 0.0d0
            ux_th = 0.0d0
            uy_th = 0.0d0

            N(1) = (1 - gz) * (1 - et) / 4
            N(2) = (1 + gz) * (1 - et) / 4
            N(3) = (1 + gz) * (1 + et) / 4
            N(4) = (1 - gz) * (1 + et) / 4  

            do i = 1,4
                ipxco = ipxco + N(i)*Harea%co_node(1,Harea%elem_node_id(i,elemid))!積分点のx座標
                ipyco = ipyco + N(i)*Harea%co_node(2,Harea%elem_node_id(i,elemid))!積分点のy座標
            enddo

            !積分点に対応する格子メッシュと正規座標系での座標を得る
            call get_Gmeshid(ipxco,ipyco,Gelemid)
            call get_etgz(ipxco,ipyco,Gelemid,et_g,gz_g)

            N(1) = (1 - gz_g) * (1 - et_g) / 4
            N(2) = (1 + gz_g) * (1 - et_g) / 4
            N(3) = (1 + gz_g) * (1 + et_g) / 4
            N(4) = (1 - gz_g) * (1 + et_g) / 4 

            !積分点での値を補完によって得る
            do i = 1,4
                ! ux_cal = ux_cal + N(i) * solver%uvec(Garea%elem_node_id(i,Gelemid)*Ndof - 3)
                ! uy_cal = uy_cal + N(i) * solver%uvec(Garea%elem_node_id(i,Gelemid)*Ndof - 2)
                ux_cal = ux_cal + N(i) * eval%solver_uvec(Garea%elem_node_id(i,Gelemid)*2 - 1)
                uy_cal = uy_cal + N(i) * eval%solver_uvec(Garea%elem_node_id(i,Gelemid)*2    )
            enddo

            !デバッグ
            ! write(*,*)ux_cal,uy_cal

            call makeJ(Harea,et,gz,elemid)

            ! x = abs( sqrt((ux_cal - ux_th)**2 + (uy_cal - uy_th)**2) * matrix%detJ)!ux_th=uy_th=0.0d0であることに注意
            eval%eleEa_sub(1) = eval%eleEa_sub(1) + ux_cal* matrix%detJ
            eval%eleEa_sub(2) = eval%eleEa_sub(2) + uy_cal* matrix%detJ
        
        enddo

        ! eval%dif_nu = eval
    end subroutine makeEa_sub

    subroutine makeEa_main()!誤差評価用メッシュを用いた絶対誤差評価(2/2)
        implicit none

        real(8) :: x
        integer(4) :: elemid,i

        dbg_name ='makeEa_main'
        call dbg_start_subroutineM

        eval%Ea = 0.0d0
        eval%eleEa = 0.0d0

        do elemid = 1,Harea%Nelem
            eval%dif_nu = 0.0d0

            call makeEa_sub(elemid)

            ! !デバッグ
            ! write(*,*)eval%eleEa_sub(1),eval%eleEa_sub(2)
            ! eval%eleEa(elemid) = eval%dif_nu!要修正
            x = sqrt(eval%eleEa_sub(1)**2 + eval%eleEa_sub(2)**2)

            eval%eleEa(elemid) = x
            eval%Ea = eval%Ea + x

            !デバッグ
            ! write(*,*)eval%eleEa(elemid)

        enddo

    end subroutine makeEa_main

    subroutine mod08_L2norm()
        implicit none 
        dbg_name = 'mod08_L2norm'
        call dbg_start_subroutineL()




        call read_uvec()
        ! call trans_mesh()
        ! call transmesh_th()
        call makeL2_main()
        call makeEa_main()
    end subroutine mod08_L2norm

end module evaluate