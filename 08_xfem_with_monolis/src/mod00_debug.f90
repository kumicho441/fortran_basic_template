module dbg
    use mod_monolis
    implicit none

    logical :: dbg_show_processL,dbg_show_processM ,dbg_disign_nrec,dbg_no_subdivision,&
    dbg_disign_sig_inf,dbg_no_diag_scaling=.false.,dbg_no_addition,dbg_not_showL2,dbg_show_outcome,&
    dbg_not_exchange_Emesh
    character(256) :: dbg_name
    real(8) :: lengL_disign,ratioR_disign,ratioX_disign
    integer(4) :: diviL_disign,n_rec_disign,sig_inf_disign
    integer(4) :: global_iter


    contains

    subroutine controller()!領域分割数などを指定
        implicit none
        integer(4) :: i,j=10
        !メッシュ分割を指定
        !デバッグ
        ! read*,i

        ! lengL_disign = 10 !初期値(10.0d0)
        ! diviL_disign = 20!(10)
        ! ratioR_disign = 0.25d0 !(0.25d0)

        open(j,file="init.dat",status="old")!elem.datの読み取り
        read(j,*)lengL_disign!一行目は平板の一辺長さ(real(8))
        read(j,*)diviL_disign!二行目は平板の一辺あたり分割数(integer(4))
        read(j,*)ratioR_disign!三行目は一辺の長さのうち円孔平板の長さ割合(real(8))
        close(j)


    end subroutine controller

    subroutine dbg_controller()!詳細なデバッグ設定
        implicit none
        !実行中のサブルーチンを表示
        dbg_show_processL = .false. !モジュール単位の大型サブルーチン
        dbg_show_processM = .false. !モジュール内の主要なサブルーチン


        !積分領域細分化時の一辺あたり分割数指定
        dbg_disign_nrec = .true.!(4)
            n_rec_disign = 200

        !細分化なし
        dbg_no_subdivision = .false.

        !σ_∞指定
        dbg_disign_sig_inf = .false.!(100)
            sig_inf_disign = 100

        !uG+uXの足しこみなし
        dbg_no_addition = .false.

        !誤差評価用メッシュに変位を付与しない
        dbg_not_exchange_Emesh = .false.

        !解析結果の表示
        dbg_show_outcome = .true.
    
        
    end subroutine dbg_controller

    subroutine announce_debug()!実行するデバッグの可視化
        implicit none
        if(dbg_show_processL)then
            write(*,*)'CAUTION "dbg_show_processL" is True!'
        endif
        if(dbg_show_processM)then
            write(*,*)'CAUTION "dbg_show_processM" is True!'
        endif
        if(dbg_disign_nrec)then
            write(*,*)'CAUTION "dbg_disign_nrec" is True!'
        endif
        if(dbg_no_subdivision)then
            write(*,*)'CAUTION "dbg_no_subdivision" is True!'
        endif
        if(dbg_disign_sig_inf)then
            write(*,*)'CAUTION "dbg_disign_sig_inf" is True!'
        endif
        if(dbg_no_addition)then
            write(*,*)'CAUTION "dbg_no_addition" is True!'
        endif
        if(dbg_not_exchange_Emesh)then
            write(*,*)'CAUTION "dbg_not_exchange_Emesh" is True!'
        endif

        if(dbg_show_outcome)then
            write(*,*)'CAUTION "dbg_show_outcome" is True!'
        endif


        if(dbg_show_processL.or.dbg_show_processM)then
            dbg_name= 'dbg_controller'
            write(*,*)'start subroutine ',dbg_name
        endif

    end subroutine announce_debug

    subroutine dbg_start_subroutineL()
        if(dbg_show_processL)then
            write(*,*)'Finish'
            write(*,*)'Start subroutine ',dbg_name
        endif
    end subroutine dbg_start_subroutineL
    subroutine dbg_start_subroutineM()
        if(dbg_show_processM)then
            write(*,*)' Finish'
            write(*,*)' start subroutine ',dbg_name
        endif
    end subroutine dbg_start_subroutineM

    subroutine mod00_debug()
        call controller()
        call dbg_controller()
        ! call announce_debug()
    end subroutine mod00_debug


end module dbg