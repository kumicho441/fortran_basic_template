module insert_BC
    use dbg
    use variables
    use initialize
    implicit none

    contains

    subroutine insert_Neumann()!外力の境界条件
        implicit none
        real(8) :: x = 0.0d0
        integer(4) :: numload=10

        dbg_name = 'insert_Neumann'
        call dbg_start_subroutineM()

        !load.datの作成
        open(unit = numload, file = 'load.dat', status = 'replace')
        write(numload, '(i0,a)')0,' 4'
        ! write(numload, '(i0,a,G0)')1,' 1 ',x
        close(numload)  

        ! fname = monolis_get_global_input_file_name(MONOLIS_DEFAULT_TOP_DIR, MONOLIS_DEFAULT_PART_DIR, "load.dat")
        ! call monolis_input_bc_R(fname, param%ncload, ndof, param%icload, param%cload)

    end subroutine insert_Neumann

    subroutine insert_Dirichlet_withM()
        implicit none

        real(8) :: val
        integer(4) :: Nbc,Ndim,in,dof,i,numbc=10,Ndof=4
        real(8),allocatable :: r_bc(:)
        integer(4),allocatable :: i_bc(:,:)
        CHARACTER*128 :: fname

        dbg_name = 'insert_Dirichlet_withM'
        call dbg_start_subroutineM()

        !境界条件ファイルを読み込む
        fname = monolis_get_global_input_file_name(MONOLIS_DEFAULT_TOP_DIR, MONOLIS_DEFAULT_PART_DIR, "bc.dat")
        open(numbc,file=fname,status="old")
        read(numbc,*) Nbc,Ndim
        allocate(i_bc(2,Nbc),r_bc(Nbc))
        i_bc = 0
        r_bc = 0.0d0

        do i = 1,Nbc
            read(numbc,*) i_bc(1,i), i_bc(2,i), r_bc(i)
        enddo
        close(numbc)

        
        in = 0
        dof = 0
        val = 0.0d0

        do i = 1,Nbc
            in = i_bc(1,i)
            dof = i_bc(2,i)
            val = r_bc(i)

            if(4<dof) stop "***error: 4 < dof"
            call monolis_set_Dirichlet_bc_R(monost,solver%Fvec,in,dof,val)
        enddo

        !対角成分0エラーが生じるため利用しない
        ! fname = monolis_get_global_input_file_name(MONOLIS_DEFAULT_TOP_DIR, MONOLIS_DEFAULT_PART_DIR, "bc_old.dat")
        ! call monolis_input_bc_R(fname, Nbc, Ndof,i_bc,r_bc)!Ndofはグローバル変数を参照中


    end subroutine insert_Dirichlet_withM

    subroutine mod05_BC_withM()
        dbg_name = 'mod05_BC'
        call dbg_start_subroutineL()

        call initialize_solver()!mod04の後かつmod05の前で呼ぶ必要がある
        call insert_Neumann()
        call insert_Dirichlet_withM()

    end subroutine mod05_BC_withM
end module insert_BC