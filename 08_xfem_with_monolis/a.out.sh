#!/bin/bash

make
# InputFileディレクトリに移動
cd Files

# 実行ファイル
../bin/01_mesher
mpirun -np 1 ../bin/02_solver
../bin/03_evaluater 1

# 元のディレクトリに戻る
cd ..