#!/bin/bash
PARA=4 #並列数

make
# Fileディレクトリに移動
cd Files

# 実行ファイル
../bin/01_mesher

#従来手法
../submodule/monolis/bin/gedatsu_simple_mesh_partitioner -n $PARA
../submodule/monolis/bin/gedatsu_bc_partitioner_R -i bc.dat -ig node.dat -n $PARA
# #../submodule/monolis/bin/gedatsu_bc_partitioner_R -i load.dat -ig node.dat -n 2
mpirun -np $PARA ../bin/02_solver
mpirun -np $PARA ../bin/02_solver
mpirun -np $PARA ../bin/02_solver

#提案手法
../submodule/monolis/bin/gedatsu_simple_mesh_partitioner -n $PARA -inw node_weight.dat
../submodule/monolis/bin/gedatsu_bc_partitioner_R -i bc.dat -ig node.dat -n $PARA

#ソルバ
mpirun -np $PARA ../bin/02_solver
mpirun -np $PARA ../bin/02_solver
mpirun -np $PARA ../bin/02_solver

# 誤差評価は逐次計算のみ
../bin/03_evaluater $PARA


# 元のディレクトリに戻る
cd ..
