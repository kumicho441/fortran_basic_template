program main
    implicit none
!     !>pはすべての点の数、qは円の内部にある点の数を表す(いずれも初期化)
!     !>piを少数で表したいためp,qも小数点型で定義する
!     integer(8) :: p=0, q=0 
!     real(8) ::pi

!     !>iは繰り返しに用いる、jは繰り返し回数
!     integer :: i,j

!     !> 2つの要素を持つ構造体の定義
!     type point
!         real(8) :: co_x !>x座標
!         real(8) :: co_y !>y座標
!     end type point

!     !>点Aを定義する。
!     type(point) :: A 

!     write(*,*)"繰り返し回数を入力"
!     read *,j
!     !>繰り返し回数を決める
!     do i = 1,j

!     !>点Aのx,y座標を入力する
!     call random_number(A%co_x) 
!     call random_number(A%co_y)

!     !>点の数を数える処理を行う
!     p = p + 1 !>pは必ず+1
!     if (A%co_x * A%co_x + A%co_y * A%co_y <= 1) then  !>qは点が円の内部にあるなら+1
!        !> write(*,*)A%co_x * A%co_x + A%co_y * A%co_y
!         q = q + 1
!     endif

! enddo
!     !>piを計算する
!     pi = dble(q)*4/dble(p)
!     write(*,*)"π≒",pi
!     write(*,*)p,q,j
    real(8) :: x,costh,cos3th,sinth,sin3th,poisson,young,stress,radius,y,rrr,ux_th


    x = atan(0.0d0)
    costh = cos(x)
    cos3th = cos(3 * x)
    sinth = sin(x)
    sin3th = sin(3 * x)

    stress = 100
    poisson = 0.3
    young = 200000
    radius = 1
    ! rrr = 10*sqrt(2.0d0)
    rrr = 5



    x = (3 - poisson) / (1 + poisson) ! κを記録
    y = young / (2 * (1 + poisson))   ! μを記録


    ux_th = (stress * radius / (8 * y)) * (rrr / radius * (x + 1) * costh + &
               2 * radius / rrr * ((1 + x) * costh + cos3th) - &
               2 * radius ** 3 / (rrr ** 3) * cos3th)


    write(*,*)'ux_th = ',ux_th
end program main