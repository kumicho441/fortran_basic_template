module global_variables
    implicit none
    integer :: ijk,Nonode
    real(8) :: poisson,young
    real(8),allocatable :: co_node(:,:)
    contains

    subroutine fileinput(poisson,young,Nonode,co_node)
    implicit none
    integer :: Nonode,ijk
    real(8) :: poisson,young
    real(8),allocatable :: co_node(:,:)

    open(11,file="input.dat",status="old") 
        read(11,*)young
        read(11,*)poisson

    close(11)


    open(12,file="node.dat",status="old") 
    read(12,*)Nonode !>節点数を数える
    allocate(co_node(Nonode,3))  !>節点の数だけ節点座標を表す変数を作成する
    
    co_node = 0.0 !>初期化
    do ijk = 1, Nonode
        read(12,*) co_node(ijk,1),co_node(ijk,2) !>ファイルから座標を読み取り、配列に代入する
    end do

    close(12)

        end subroutine fileinput
end module
